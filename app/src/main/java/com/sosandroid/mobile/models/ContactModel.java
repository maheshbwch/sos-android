package com.sosandroid.mobile.models;

import java.io.Serializable;

public class ContactModel implements Serializable {

    private String contact_name;
    private String contact_number;
    private boolean isSelected;

    public ContactModel(String contact_name, String contact_number, boolean isSelected) {
        this.contact_name = contact_name;
        this.contact_number = contact_number;
        this.isSelected = isSelected;
    }

    public String getName() {
        return contact_name;
    }

    public void setName(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getPhoneNumber() {
        return contact_number;
    }

    public void setPhoneNumber(String contact_number) {
        this.contact_number = contact_number;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
