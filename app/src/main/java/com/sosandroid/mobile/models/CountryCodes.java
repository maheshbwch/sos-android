package com.sosandroid.mobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountryCodes extends BaseModel {

    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("countries_id")
        @Expose
        private String countriesId;
        @SerializedName("countries_name")
        @Expose
        private String countriesName;
        @SerializedName("countries_iso_code")
        @Expose
        private String countriesIsoCode;
        @SerializedName("countries_isd_code")
        @Expose
        private String countriesIsdCode;
        @SerializedName("url_path")
        @Expose
        private String urlPath;

        public String getCountriesId() {
            return countriesId;
        }

        public void setCountriesId(String countriesId) {
            this.countriesId = countriesId;
        }

        public String getCountriesName() {
            return countriesName;
        }

        public void setCountriesName(String countriesName) {
            this.countriesName = countriesName;
        }

        public String getCountriesIsoCode() {
            return countriesIsoCode;
        }

        public void setCountriesIsoCode(String countriesIsoCode) {
            this.countriesIsoCode = countriesIsoCode;
        }

        public String getCountriesIsdCode() {
            return countriesIsdCode;
        }

        public void setCountriesIsdCode(String countriesIsdCode) {
            this.countriesIsdCode = countriesIsdCode;
        }

        public String getUrlPath() {
            return urlPath;
        }

        public void setUrlPath(String urlPath) {
            this.urlPath = urlPath;
        }

    }

}
