package com.sosandroid.mobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetActivityListModel extends BaseModel {

    @SerializedName("notification")
    @Expose
    private ArrayList<Notification> notification = null;

    public ArrayList<Notification> getNotification() {
        return notification;
    }

    public void setNotification(ArrayList<Notification> notification) {
        this.notification = notification;
    }

    public class Notification {

        @SerializedName("activity_id")
        @Expose
        private String activityId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("activity_type")
        @Expose
        private String activityType;
        @SerializedName("activity_message")
        @Expose
        private String activityMessage;
        @SerializedName("activity_updated_at")
        @Expose
        private String activityUpdatedAt;

        public String getActivityId() {
            return activityId;
        }

        public void setActivityId(String activityId) {
            this.activityId = activityId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getActivityType() {
            return activityType;
        }

        public void setActivityType(String activityType) {
            this.activityType = activityType;
        }

        public String getActivityMessage() {
            return activityMessage;
        }

        public void setActivityMessage(String activityMessage) {
            this.activityMessage = activityMessage;
        }

        public String getActivityUpdatedAt() {
            return activityUpdatedAt;
        }

        public void setActivityUpdatedAt(String activityUpdatedAt) {
            this.activityUpdatedAt = activityUpdatedAt;
        }

    }
}
