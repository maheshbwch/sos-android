package com.sosandroid.mobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FaqModel extends BaseModel {
    @SerializedName("faqs")
    @Expose
    private Faqs faqs;

    public Faqs getFaqs() {
        return faqs;
    }

    public void setFaqs(Faqs faqs) {
        this.faqs = faqs;
    }

    public class Faqs {

        @SerializedName("common_faqs")
        @Expose
        private ArrayList<CommonFaq> commonFaqs = null;
        @SerializedName("location")
        @Expose
        private ArrayList<Location> location = null;
        @SerializedName("general")
        @Expose
        private ArrayList<General> general = null;
        @SerializedName("features")
        @Expose
        private ArrayList<Feature> features = null;
        @SerializedName("privacy_policy")
        @Expose
        private ArrayList<PrivacyPolicy> privacyPolicy = null;

        public ArrayList<CommonFaq> getCommonFaqs() {
            return commonFaqs;
        }

        public void setCommonFaqs(ArrayList<CommonFaq> commonFaqs) {
            this.commonFaqs = commonFaqs;
        }

        public ArrayList<Location> getLocation() {
            return location;
        }

        public void setLocation(ArrayList<Location> location) {
            this.location = location;
        }

        public ArrayList<General> getGeneral() {
            return general;
        }

        public void setGeneral(ArrayList<General> general) {
            this.general = general;
        }

        public ArrayList<Feature> getFeatures() {
            return features;
        }

        public void setFeatures(ArrayList<Feature> features) {
            this.features = features;
        }

        public ArrayList<PrivacyPolicy> getPrivacyPolicy() {
            return privacyPolicy;
        }

        public void setPrivacyPolicy(ArrayList<PrivacyPolicy> privacyPolicy) {
            this.privacyPolicy = privacyPolicy;
        }

        public class CommonFaq {

            @SerializedName("common_faq_id")
            @Expose
            private String commonFaqId;
            @SerializedName("common_faq_question")
            @Expose
            private String commonFaqQuestion;
            @SerializedName("common_faq_answer")
            @Expose
            private String commonFaqAnswer;

            public String getCommonFaqId() {
                return commonFaqId;
            }

            public void setCommonFaqId(String commonFaqId) {
                this.commonFaqId = commonFaqId;
            }

            public String getCommonFaqQuestion() {
                return commonFaqQuestion;
            }

            public void setCommonFaqQuestion(String commonFaqQuestion) {
                this.commonFaqQuestion = commonFaqQuestion;
            }

            public String getCommonFaqAnswer() {
                return commonFaqAnswer;
            }

            public void setCommonFaqAnswer(String commonFaqAnswer) {
                this.commonFaqAnswer = commonFaqAnswer;
            }

        }

        public class Location {

            @SerializedName("location_id")
            @Expose
            private String locationId;
            @SerializedName("location_content")
            @Expose
            private String locationContent;

            public String getLocationId() {
                return locationId;
            }

            public void setLocationId(String locationId) {
                this.locationId = locationId;
            }

            public String getLocationContent() {
                return locationContent;
            }

            public void setLocationContent(String locationContent) {
                this.locationContent = locationContent;
            }

        }

        public class General {

            @SerializedName("general_id")
            @Expose
            private String generalId;
            @SerializedName("general_content")
            @Expose
            private String generalContent;

            public String getGeneralId() {
                return generalId;
            }

            public void setGeneralId(String generalId) {
                this.generalId = generalId;
            }

            public String getGeneralContent() {
                return generalContent;
            }

            public void setGeneralContent(String generalContent) {
                this.generalContent = generalContent;
            }

        }

        public class Feature {

            @SerializedName("feature_id")
            @Expose
            private String featureId;
            @SerializedName("feature_content")
            @Expose
            private String featureContent;

            public String getFeatureId() {
                return featureId;
            }

            public void setFeatureId(String featureId) {
                this.featureId = featureId;
            }

            public String getFeatureContent() {
                return featureContent;
            }

            public void setFeatureContent(String featureContent) {
                this.featureContent = featureContent;
            }

        }

        public class PrivacyPolicy {

            @SerializedName("privacy_policy_id")
            @Expose
            private String privacyPolicyId;
            @SerializedName("privacy_policy_content")
            @Expose
            private String privacyPolicyContent;

            public String getPrivacyPolicyId() {
                return privacyPolicyId;
            }

            public void setPrivacyPolicyId(String privacyPolicyId) {
                this.privacyPolicyId = privacyPolicyId;
            }

            public String getPrivacyPolicyContent() {
                return privacyPolicyContent;
            }

            public void setPrivacyPolicyContent(String privacyPolicyContent) {
                this.privacyPolicyContent = privacyPolicyContent;
            }

        }

    }


}
