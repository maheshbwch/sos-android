package com.sosandroid.mobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PostModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("total_post")
    @Expose
    private Integer totalPost;
    @SerializedName("post_details")
    @Expose
    private ArrayList<PostModel.PostDetail> postModelArrayList =  new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTotalPost() {
        return totalPost;
    }

    public void setTotalPost(Integer totalPost) {
        this.totalPost = totalPost;
    }

    public ArrayList<PostModel.PostDetail> getPostDetails() {
        return postModelArrayList;
    }

    public void setPostDetails(ArrayList<PostModel.PostDetail> postDetails) {
        this.postModelArrayList = postDetails;
    }

    public class PostDetail {

        @SerializedName("post_id")
        @Expose
        private String postId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("post_category")
        @Expose
        private String postCategory;
        @SerializedName("post_content")
        @Expose
        private String postContent;
        @SerializedName("post_image")
        @Expose
        private String postImage;
        @SerializedName("posted_at")
        @Expose
        private String postedAt;

        public String getPostId() {
            return postId;
        }

        public void setPostId(String postId) {
            this.postId = postId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPostCategory() {
            return postCategory;
        }

        public void setPostCategory(String postCategory) {
            this.postCategory = postCategory;
        }

        public String getPostContent() {
            return postContent;
        }

        public void setPostContent(String postContent) {
            this.postContent = postContent;
        }

        public String getPostImage() {
            return postImage;
        }

        public void setPostImage(String postImage) {
            this.postImage = postImage;
        }

        public String getPostedAt() {
            return postedAt;
        }

        public void setPostedAt(String postedAt) {
            this.postedAt = postedAt;
        }

    }
}
