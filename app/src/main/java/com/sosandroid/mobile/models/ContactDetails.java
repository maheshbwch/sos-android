package com.sosandroid.mobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactDetails extends BaseModel {

    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("contact_id")
        @Expose
        private String contactId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("contact_name")
        @Expose
        private String contactName;
        @SerializedName("contact_number")
        @Expose
        private String contactNumber;
        @SerializedName("contact_type")
        @Expose
        private String contactType;
        @SerializedName("whatsapp_notification")
        @Expose
        private String whatsappNotification;
        @SerializedName("line_notification")
        @Expose
        private String lineNotification;
        @SerializedName("telegram_notification")
        @Expose
        private String telegramNotification;
        @SerializedName("viber_notification")
        @Expose
        private String viberNotification;
        @SerializedName("call")
        @Expose
        private String call;
        @SerializedName("date_time")
        @Expose
        private String dateTime;

        public String getContactId() {
            return contactId;
        }

        public void setContactId(String contactId) {
            this.contactId = contactId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

        public String getContactType() {
            return contactType;
        }

        public void setContactType(String contactType) {
            this.contactType = contactType;
        }

        public String getWhatsappNotification() {
            return whatsappNotification;
        }

        public void setWhatsappNotification(String whatsappNotification) {
            this.whatsappNotification = whatsappNotification;
        }

        public String getLineNotification() {
            return lineNotification;
        }

        public void setLineNotification(String lineNotification) {
            this.lineNotification = lineNotification;
        }

        public String getTelegramNotification() {
            return telegramNotification;
        }

        public void setTelegramNotification(String telegramNotification) {
            this.telegramNotification = telegramNotification;
        }

        public String getViberNotification() {
            return viberNotification;
        }

        public void setViberNotification(String viberNotification) {
            this.viberNotification = viberNotification;
        }

        public String getCall() {
            return call;
        }

        public void setCall(String call) {
            this.call = call;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

    }
}
