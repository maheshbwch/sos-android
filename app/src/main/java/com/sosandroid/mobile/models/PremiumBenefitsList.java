package com.sosandroid.mobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PremiumBenefitsList extends BaseModel {


    @SerializedName("Premium_benefits")
    @Expose
    private ArrayList<PremiumBenefit> premiumBenefits = null;

    public ArrayList<PremiumBenefit> getPremiumBenefits() {
        return premiumBenefits;
    }

    public void setPremiumBenefits(ArrayList<PremiumBenefit> premiumBenefits) {
        this.premiumBenefits = premiumBenefits;
    }


    public class PremiumBenefit {

        @SerializedName("plan_id")
        @Expose
        private String planId;
        @SerializedName("feature")
        @Expose
        private String feature;
        @SerializedName("free")
        @Expose
        private boolean free;
        @SerializedName("gold_plan")
        @Expose
        private boolean goldPlan;

        public String getPlanId() {
            return planId;
        }

        public void setPlanId(String planId) {
            this.planId = planId;
        }

        public String getFeature() {
            return feature;
        }

        public void setFeature(String feature) {
            this.feature = feature;
        }

        public boolean getFree() {
            return free;
        }

        public void setFree(boolean free) {
            this.free = free;
        }

        public boolean getGoldPlan() {
            return goldPlan;
        }

        public void setGoldPlan(boolean goldPlan) {
            this.goldPlan = goldPlan;
        }

    }
}
