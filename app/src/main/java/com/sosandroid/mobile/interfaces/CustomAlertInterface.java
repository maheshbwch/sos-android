package com.sosandroid.mobile.interfaces;

public interface CustomAlertInterface {

    void onPositiveButtonClicked();

    void onNegativeButtonClicked();

}
