package com.sosandroid.mobile.interfaces;

public interface DialogInterface {
    void onButtonClicked();
    void onDismissedClicked();
}
