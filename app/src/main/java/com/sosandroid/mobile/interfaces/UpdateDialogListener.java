package com.sosandroid.mobile.interfaces;

public interface UpdateDialogListener {
    void onUpdateButtonClicked();

    void onNotNowButtonClicked();

    void onDismissButtonClicked();

}
