package com.sosandroid.mobile.interfaces;

public interface CountDownInterface {
    void onTicking(long millisUntilFinished);
    void onFinished();

}
