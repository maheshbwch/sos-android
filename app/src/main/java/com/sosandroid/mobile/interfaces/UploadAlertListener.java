package com.sosandroid.mobile.interfaces;

public interface UploadAlertListener {
    void onChooseFileClicked();
    void onCaptureCameraClicked();
}
