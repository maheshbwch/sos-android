package com.sosandroid.mobile.interfaces;

public interface OnItemClicked {
    void onItemClicked(int position);
}
