package com.sosandroid.mobile.interfaces;

import com.sosandroid.mobile.models.ContactModel;

public interface OnContactSelected {
    void onSelected(ContactModel contactModel);

    void onDeSelected(ContactModel contactModel);
}
