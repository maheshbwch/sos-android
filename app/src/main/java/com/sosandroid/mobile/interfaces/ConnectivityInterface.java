package com.sosandroid.mobile.interfaces;

public interface ConnectivityInterface {
    void onConnected();
    void onNotConnected();
}
