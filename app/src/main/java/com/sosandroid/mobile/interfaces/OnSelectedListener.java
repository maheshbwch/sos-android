package com.sosandroid.mobile.interfaces;

import com.sosandroid.mobile.models.CountryCodes;

public interface OnSelectedListener {

    void onSelected(CountryCodes.Datum countryCodes);
    void onNothingSelected();

}