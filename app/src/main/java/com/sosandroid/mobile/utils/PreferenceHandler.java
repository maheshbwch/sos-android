package com.sosandroid.mobile.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHandler {

    public static boolean storePreference(Context context, String key, String data) {
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putString(key, data);
            editor.commit();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean storePreference(Context context, String key, int data) {
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putInt(key, data);
            editor.commit();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static String getPreferenceFromString(Context context, String key) {
        String data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getString(key, null);
            //data = mySharedPreferences.getString(key, "");
        } catch (Exception e) {
            return null;
        }
        return data;
    }

    public static int getPreferenceFromInt(Context context, String key) {
        int data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getInt(key, 0);
            //data = mySharedPreferences.getString(key, "");
        } catch (Exception e) {
            return 0;
        }
        return data;
    }

    public static boolean storeBooleanValue(Context context, String prefdialog, Boolean value) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(prefdialog, value);
        editor.commit();
        return true;
    }

    public static boolean getBooleanFromSP(Context context, String key) {
        boolean data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getBoolean(key, false);

        } catch (Exception e) {
            return false;
        }
        return data;
    }

    public static boolean storePreference(Context context, String key, Integer data) {
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putInt(key, 0);
            editor.commit();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static boolean storePreferenceOfSpinner(Context context, String key,
                                                   Integer integer) {
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putInt(key, integer);
            editor.commit();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public static int getPreferenceOfSpinner(Context context, String key) {
        int data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getInt(key, 1); // 1 is default value
            //data = mySharedPreferences.getString(key, "");
        } catch (Exception e) {
            return 0;
        }
        return data;
    }

    public static int getPreferenceOfSpinnerPreDay(Context context, String key) {
        int data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getInt(key, 5); // 1 is default value
            //data = mySharedPreferences.getString(key, "");

        } catch (Exception e) {
            return 0;
        }
        return data;
    }

    public static int getPreferenceOfSpinnerPerMonth(Context context, String key) {
        int data;
        SharedPreferences mySharedPreferences;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            data = mySharedPreferences.getInt(key, 100); // 1 is default value
            //data = mySharedPreferences.getString(key, "");

        } catch (Exception e) {
            return 0;
        }
        return data;
    }

    public static boolean clearPreferenceData(Context context, String key) {
        SharedPreferences mySharedPreferences;
        String data = null;
        try {
            mySharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            data = mySharedPreferences.getString(key, data);
            editor.clear();
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean clearPreferenceParticularData(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        if (settings.contains(key)) {
            SharedPreferences.Editor editor = settings.edit();
            editor.remove(key);
            editor.commit();
        }
        return true;
    }

    public static boolean clearPreferences(Context context){
        SharedPreferences mySharedPreferences;
        try{
            mySharedPreferences=context.getSharedPreferences(Constants.PREFS_NAME,0);
            SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.clear();
            editor.commit();
        }
        catch(Exception e){
            return false;
        }
        return true;
    }
}
