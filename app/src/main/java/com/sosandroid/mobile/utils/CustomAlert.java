package com.sosandroid.mobile.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.CustomAlertInterface;


public class CustomAlert extends AlertDialog.Builder {

    private Context context = null;

    public CustomAlert(final Context context, /*String title,*/ String message, String positiveButtonTxt, String negativeButtonTxt, final CustomAlertInterface customAlertInterface) {
        super(context);
        this.context = context;
        //this.setTitle(title);
        this.setMessage(message);
        //this.setIcon(R.drawable.logo);
        this.setPositiveButton(positiveButtonTxt, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
                customAlertInterface.onPositiveButtonClicked();

            }
        });
        if (MyUtils.checkStringValue(negativeButtonTxt)) {
            this.setNegativeButton(negativeButtonTxt, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    customAlertInterface.onNegativeButtonClicked();
                }
            });
        }
        this.setCancelable(false);


    }


    public void showDialog() {
        AlertDialog dialog = this.create();
        dialog.show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        positiveButton.setTextColor(context.getResources().getColor(R.color.blue));
        negativeButton.setTextColor(context.getResources().getColor(R.color.blue));
    }

    public AlertDialog getDialog() {
        AlertDialog dialog = this.create();
        dialog.show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        positiveButton.setTextColor(context.getResources().getColor(R.color.blue));
        negativeButton.setTextColor(context.getResources().getColor(R.color.blue));
        return dialog;
    }


}
