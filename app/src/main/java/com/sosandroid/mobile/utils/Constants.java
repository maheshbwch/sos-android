package com.sosandroid.mobile.utils;

public class Constants {

    public static final boolean DEBUG_MODE = false; //TODO SET FALSE BEFORE BUILD APK

    public static final int SPLASH_SCREEN_DELAY = 2000; //2 seconds
    public static final int READ_CONTACT_PERMISSION = 2010;
    public static final int GET_CONTACTS = 2011;

    public static final int REGISTRATION = 2012;
    public static final int REGISTRATION_DONE = 2013;
    public static final int REGISTRATION_DECLINED = 2014;

    public static final int PHONE_NUMBER_EDIT = 2015;

    public static final int ADD_POST = 2016;

    public static final int ADD_CONTACT_MANUALLY = 2017;

    public static final int TEST_CONTACT_COUNT = 2018;

    public static final int FORGOT_PASSWORD = 2019;

    public static final int PICK_IMAGE_FROM_FILES = 1001;
    public static final int CAPTURE_IMAGE_FROM_CAMERA = 1002;

    public static final int WRITE_STORAGE_FILES_PERMISSION = 2002;
    public static final int WRITE_STORAGE_CAMERA_PERMISSION = 2003;
    public static final int WRITE_FINE_LOCATION = 2004;

    public static final int CALL_PHONE = 2004;

    public static final int OTP_TIME_OUT = 120;
    public static final int ALERT_TIME_OUT = 10;

    public static final String PREFS_NAME = "pref_name";

    public static final String IS_FROM = "is_from";
    public static final String USER_LOGIN = "user_login";
    public static final String COUNTRY_CODE_ID = "country_code_id";
    public static final String COUNTRY_CODE = "country_code";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String PASSWORD = "password";
    public static final String USER_NAME = "user_name";
    public static final String VERIFICATION_ID = "verificationId";

    public static final String USER_ID = "userId";
    public static final String AGE = "age";
    public static final String RACE = "race";
    public static final String EMAIL = "email";
    public static final String PROFILE_IMG_URL = "profileFile";
    public static final String HOME_ADDRESS = "homeAddress";
    public static final String SCHOOL_ADDRESS = "schoolAddress";
    public static final String OFFICE_ADDRESS = "officeAddress";

    public static final String IS_FROM_SIGN_UP = "is_from_sign_up";
    public static final String IS_FROM_LOGIN = "is_from_login";
    public static final String IS_FROM_PHONE_NUMBER_EDIT = "is_from_phone_number_edit";
    public static final String IS_FROM_PASSWORD = "is_from_password";
    public static final String IS_FROM_FORGOT = "is_from_forgot";
    public static final String selectedContacts = "selectedContacts";
    public static final String CONTACTS_REGISTRATION = "contactRegistrationType";
    public static final String CONTACTS_SELECTION = "contactSelectionType";
    public static final String CONTACTS_OPTIONS = "contactOptionsType";

    public static final String IS_FROM_EMERGENCY_CONTACT = "is_from_emergency_contact";
    public static final String IS_FROM_TESTCONTACT = "is_from_testcontact";

    public static final String FCM_TOKEN = "fcm_token";

    public static final String CONTACT_ID = "contact_id";

    public static final String DEFAULT_STATE = "default";
    public static final String ENABLE_STATE = "enable";
    public static final String DISABLE_STATE = "disable";

    public static final String SUCCESS = "1";
    public static final String FAILURE = "0";

    public static final String DIALOG_SUCCESS = "0";
    public static final String DIALOG_FAILURE = "1";
    public static final String DIALOG_WARNING = "2";
    public static final String DIALOG_GENERAL = "3";

    public static final String WHATSAPP_NUMBER = "+60 12-357 6021"; //TODO CHANGE NUMBER
    public static final String WHATSAPP_MSG = "SOS App"; //TODO CHANGE MESSAGE
    public static final String WHATSAPP_ALERT_POLICE = "SOS - Police Emergency Message"; //TODO CHANGE MESSAGE
    public static final String WHATSAPP_ALERT_HOSPITAL = "SOS - Hospital Emergency Message"; //TODO CHANGE MESSAGE
    public static final String WHATSAPP_ALERT_FIRE = "SOS - Fire Emergency Message"; //TODO CHANGE MESSAGE
    public static final String WHATSAPP_ALERT_TEST = "Test SOS - Fire Emergency Message"; //TODO CHANGE MESSAGE

    public static final String IS_FROM_POLICE = "is_from_police";
    public static final String IS_FROM_HOSPITAL = "is_from_hospital";
    public static final String IS_FROM_FIRE = "is_from_fire";
    public static final String IS_FROM_TEST = "is_from_test";

    public static final String COMMON_FAQ = "common_faq";
    public static final String LOCATION_FAQ = "location_faq";
    public static final String GENERAL_FAQ = "general_faq";
    public static final String FEATURES_FAQ = "features_faq";
    public static final String PRIVACY_POLICY = "privacy_policy";

    public static final String TYPE = "type";
    public static final String TEST_ALERT = "test_alert";
    public static final String EMERGENCY_ALERT = "emergency_alert";

    public static final String APP_VERSION_NAME = "app_version_name";
    public static final String APP_VERSION_CODE = "app_version_code";
    public static final String APP_IS_FORCE_UPDATE = "is_force_update";
    public static final String APP_UPDATE_CONTENT = "app_update_content";

    public static final String UTILISE_API_URL_FROM_REMOTE = "utilise_remote_base_url";
    public static final String APP_BASE_API_URL = "app_base_api_url";


    public static final String ERROR_REMOTE_CONFIG = "Unable to proceed further,please try again later";


}
