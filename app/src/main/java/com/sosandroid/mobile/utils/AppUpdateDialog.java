package com.sosandroid.mobile.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.UpdateDialogListener;


public class AppUpdateDialog {

    private Activity activity;
    private String statusMessage;
    private UpdateDialogListener updateDialogListener;
    private AlertDialog dialog = null;

    private String titleMessage = null;
    private boolean isForceUpdate = false;


    public AppUpdateDialog(Activity activity, String statusMessage, UpdateDialogListener updateDialogListener) {
        this.activity = activity;
        this.statusMessage = statusMessage;
        this.updateDialogListener = updateDialogListener;
    }

    public void setTitleMessage(String titleMessage) {
        this.titleMessage = titleMessage;
    }

    public void showNotNowButton(boolean isForceUpdate) {
        this.isForceUpdate = isForceUpdate;
    }


    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void showDialog() {

        LayoutInflater inflater = activity.getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_app_update, null);

        ImageView close = alertLayout.findViewById(R.id.dau_imgClose);
        TextView statusTitleTxt = alertLayout.findViewById(R.id.dau_statusTitleTxt);
        TextView statusMessageTxt = alertLayout.findViewById(R.id.dau_statusMessageTxt);
        Button notNowButton = alertLayout.findViewById(R.id.dau_notNowButton);
        Button updateButton = alertLayout.findViewById(R.id.dau_updateButton);

        if (MyUtils.checkStringValue(titleMessage)) {
            statusTitleTxt.setVisibility(View.VISIBLE);
            statusTitleTxt.setText(titleMessage);
        } else {
            statusTitleTxt.setVisibility(View.GONE);
        }

        if (MyUtils.checkStringValue(statusMessage)) {
            statusMessageTxt.setText(statusMessage);
        }

        if (isForceUpdate) {
            notNowButton.setVisibility(View.GONE);
        } else {
            notNowButton.setVisibility(View.VISIBLE);
        }

        final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        dialog = alert.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
                updateDialogListener.onUpdateButtonClicked();
            }
        });

        notNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
                updateDialogListener.onNotNowButtonClicked();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
                updateDialogListener.onDismissButtonClicked();
            }
        });

    }


}
