package com.sosandroid.mobile.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.sosandroid.mobile.BuildConfig;
import com.sosandroid.mobile.R;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyUtils {

    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static void openOverrideAnimation(boolean isOpen, Activity activity) {
        activity.overridePendingTransition(isOpen ? R.anim.slide_from_right : R.anim.slide_from_left, isOpen ? R.anim.slide_to_left : R.anim.slide_to_right);
    }

    public static Typeface getBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Barlow-Bold.otf");
    }

    public static Typeface getRegularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Barlow-Regular.otf");
    }

    public static boolean checkStringValue(String value) {
        return value != null && !value.equalsIgnoreCase("null") && !value.isEmpty();
    }


    public static void showLongToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    public static ProgressDialog showProgressLoader(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    public static void dismissProgressLoader(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void intentToImageSelection(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.PICK_IMAGE_FROM_FILES);
    }

    public static String intentToCameraApp(Activity activity) {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile(activity);
            } catch (IOException ex) {
                //Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(pictureIntent, Constants.CAPTURE_IMAGE_FROM_CAMERA);

                return photoFile.getAbsolutePath();
            }
        }
        return null;
    }

    public static File createImageFile(Activity activity) throws IOException {
       /* File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        file.createNewFile();
        return  file;*/
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(getImageName(), ".jpg", storageDir);
    }

    public static String getImageName() {
        return "HRSB_" + getCurrentTimeStamp();
    }

    public static String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static boolean isValidEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }


    public static void openWhatsApp(Context context, String number, String message) {
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + number + "&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
            } else {
                Toast.makeText(context, "WhatsApp not installed in your device", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("ERROR WHATSAPP", e.toString());
            Toast.makeText(context, "WhatsApp not installed in your device", Toast.LENGTH_LONG).show();
        }
    }

    public static void openTelegram(Context context, String msg) {

        try {
            final String appName = "org.telegram.messenger";
            final boolean isAppInstalled = isAppAvailable(context, appName);
            if (isAppInstalled) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.setPackage(appName);
                myIntent.putExtra(Intent.EXTRA_TEXT, msg);//
                context.startActivity(Intent.createChooser(myIntent, "Share with"));
            } else {
                Toast.makeText(context, "Telegram not Installed", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Log.e("ERROR TELEGRAM", e.toString());
            Toast.makeText(context, "Telegram not installed in your device", Toast.LENGTH_LONG).show();
        }


    }

    public static void openViber(Context context, String number, String msg) {

        try {
            final String appName = "com.viber.voip";
            final boolean isAppInstalled = isAppAvailable(context, appName);
            if (isAppInstalled) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setPackage(appName);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, msg);
                context.startActivity(share);
            } else {
                Toast.makeText(context, "Viber not Installed", Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            Log.e("ERROR VIBER", e.toString());
            Toast.makeText(context, "Viber not installed in your device", Toast.LENGTH_LONG).show();
        }

    }

    public static void openLine(Context context, String number, String msg) {

        try {
            final String appName = "jp.naver.line.android";
            final boolean isAppInstalled = isAppAvailable(context, appName);
            if (isAppInstalled) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("line://msg/text/" + msg));
                context.startActivity(intent);
            } else {
                Toast.makeText(context, "Line not Installed", Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            Log.e("ERROR LINE", e.toString());
            Toast.makeText(context, "Line not installed in your device", Toast.LENGTH_LONG).show();
        }


    }

    public static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void openCallIntent(Context context, String number) {

        Log.e("number", "" + number);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            //Creating intents for making a call
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            context.startActivity(callIntent);

        } else {
            Toast.makeText(context, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }

    }

    public static boolean isInternetConnected(Context mContext) {
        boolean outcome = false;
        try {
            if (mContext != null) {
                ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
                for (NetworkInfo tempNetworkInfo : networkInfos) {
                    if (tempNetworkInfo.isConnected()) {
                        outcome = true;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outcome;
    }

    public static boolean isOreoOrAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public static boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String getCountryDialCode(Context context) {
        String contryId = null;
        String contryDialCode = null;

        TelephonyManager telephonyMngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        contryId = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrContryCode = context.getResources().getStringArray(R.array.DialingCountryCode);
        for (int i = 0; i < arrContryCode.length; i++) {
            String[] arrDial = arrContryCode[i].split(",");
            if (arrDial[1].trim().equals(contryId.trim())) {
                contryDialCode = arrDial[0];
                break;
            }
        }
        return contryDialCode;
    }

    public static int getVersionCode() {
        return BuildConfig.VERSION_CODE;
    }

    public static String getVersionCodeAndName() {
        return "" + BuildConfig.VERSION_NAME + "-" + BuildConfig.VERSION_CODE;
    }

    public static String getAppPackageName(Context context) {
        return context.getPackageName(); // package name of the app
    }

}
