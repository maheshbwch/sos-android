package com.sosandroid.mobile.activities.registration;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.ContactsAdapter;
import com.sosandroid.mobile.interfaces.OnContactSelected;
import com.sosandroid.mobile.models.ContactModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PermissionAlert;
import com.sosandroid.mobile.utils.PermissionRequest;

import java.util.ArrayList;

public class ContactsActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextView checkTxt;
    private RecyclerView recyclerView;

    ArrayList<String> alreadySelectedContactsList = new ArrayList<>();
    ArrayList<ContactModel> contactArrayList = new ArrayList<>();
    ArrayList<ContactModel> selectedContactArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        context = ContactsActivity.this;

        toolbar = findViewById(R.id.act_toolbar);
        checkTxt = findViewById(R.id.act_checkTxt);
        recyclerView = findViewById(R.id.act_recycler_view);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();

        ArrayList<ContactModel> contactArrayList = (ArrayList<ContactModel>) bundle.getSerializable(Constants.selectedContacts);
        if (contactArrayList != null) {
            alreadySelectedContactsList.clear();

            for (ContactModel contactModel : contactArrayList) {
                alreadySelectedContactsList.add(contactModel.getPhoneNumber());
            }
        }


        listeners();

    }


    private void listeners() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                MyUtils.openOverrideAnimation(false, ContactsActivity.this);
            }
        });


        checkTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedContactArrayList.size() > 0) {

                    Log.e("selected", "size:" + selectedContactArrayList.size());

                    Intent intent = getIntent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.selectedContacts, selectedContactArrayList);
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK, intent);
                    finish();


                } else {
                    if (contactArrayList.size() > 0) {
                        MyUtils.showLongToast(context, "Select the Contacts");
                    }
                }

            }
        });

        getContactPermission();
    }

    private void getContactPermission() {
        if (PermissionRequest.askForActivityPermission(Manifest.permission.READ_CONTACTS, Constants.READ_CONTACT_PERMISSION, ContactsActivity.this)) {
            getContactsList();
        }
    }


    private void getContactsList() {
        contactArrayList.clear();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            contactArrayList.add(new ContactModel(name, phoneNumber, alreadySelectedContactsList.contains(phoneNumber)));
            //Log.e("name>>", ":" + name + "  -  " + phoneNumber);
        }
        phones.close();

        adaptContactListToRecycler();
    }


    private void adaptContactListToRecycler() {
        if (contactArrayList.size() > 0) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            ContactsAdapter contactsAdapter = new ContactsAdapter(context, contactArrayList, new OnContactSelected() {
                @Override
                public void onSelected(ContactModel contactModel) {
                    if (!selectedContactArrayList.contains(contactModel)) {
                        selectedContactArrayList.add(contactModel);
                    }
                }

                @Override
                public void onDeSelected(ContactModel contactModel) {
                    if (selectedContactArrayList.contains(contactModel)) {
                        selectedContactArrayList.remove(contactModel);
                    }
                }
            }, Constants.CONTACTS_SELECTION);
            recyclerView.setAdapter(contactsAdapter);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.READ_CONTACT_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContactsList();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ContactsActivity.this, Manifest.permission.READ_CONTACTS)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "CONTACTS");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }


}
