package com.sosandroid.mobile.activities.account;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.FaqListAdapter;
import com.sosandroid.mobile.models.FagTitleTypeModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.ArrayList;

public class FaqActivity extends AppCompatActivity {


    private Context context;
    private Toolbar toolbar;
    private RecyclerView faqRecyclerView;

    ArrayList<FagTitleTypeModel> faqsArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        context = FaqActivity.this;
        init();
    }


    private void init() {

        toolbar = findViewById(R.id.faq_toolbar);
        faqRecyclerView = findViewById(R.id.faq_recycler_view);
        faqRecyclerView.setHasFixedSize(true);
        faqRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        listener();
    }

    private void listener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });
        updateUI();
    }


    private void updateUI() {

        faqsArrayList.clear();
        faqsArrayList.add(new FagTitleTypeModel("Common Faqs",Constants.COMMON_FAQ));
        faqsArrayList.add(new FagTitleTypeModel("Location",Constants.LOCATION_FAQ));
        faqsArrayList.add(new FagTitleTypeModel("General",Constants.GENERAL_FAQ));
        faqsArrayList.add(new FagTitleTypeModel("Features",Constants.FEATURES_FAQ));
        faqsArrayList.add(new FagTitleTypeModel("Privacy Policy",Constants.PRIVACY_POLICY));

        if (faqsArrayList.size() > 0) {
            FaqListAdapter getFaqAdapter = new FaqListAdapter(context, faqsArrayList);
            faqRecyclerView.setAdapter(getFaqAdapter);
        }

    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, FaqActivity.this);
    }
}
