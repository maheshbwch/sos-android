package com.sosandroid.mobile.activities.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailEdtActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextView saveTxt;
    private TextInputLayout userEmailTil;
    private TextInputEditText userEmailEdt;
    private ProgressDialog progressDialog = null;

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_edit);
        context = EmailEdtActivity.this;

        init();
    }


    private void init() {
        toolbar = findViewById(R.id.aee_toolbar);
        saveTxt = findViewById(R.id.aee_saveTxt);
        userEmailTil = findViewById(R.id.aee_userEmailTil);
        userEmailEdt = findViewById(R.id.aee_userEmailEdt);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        userEmailTil.setTypeface(MyUtils.getRegularFont(context));
        userEmailEdt.setTypeface(MyUtils.getRegularFont(context));


        listeners();
    }

    private void listeners() {
        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        Log.e("userID", ":" + userId);

        String userEmail = PreferenceHandler.getPreferenceFromString(context, Constants.EMAIL);

        if (MyUtils.checkStringValue(userEmail)) {
            userEmailEdt.setText(userEmail);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        saveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = userEmailEdt.getText().toString().trim();

                if (!MyUtils.checkStringValue(email)) {
                    MyUtils.showLongToast(context, "Email required");
                } else if (!MyUtils.isValidEmail(email)) {
                    MyUtils.showLongToast(context, "Valid Email required");
                } else {
                    updateEmail(email);
                }

            }
        });
    }


    private void updateEmail(final String email) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating Email..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.updateEmail("email", userId, email);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                PreferenceHandler.storePreference(context, Constants.EMAIL, email);

                                showAlertToUser(Constants.DIALOG_SUCCESS, "Update Email", MyUtils.checkStringValue(message) ? message : "User Email Updated Successfully");

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update Email", MyUtils.checkStringValue(message) ? message : "Unable to Update User Email");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update Email", "Unable to Update User Email");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Update Email", "Unable to Update User Email");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Email", "Unable to Update User Email");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Email", "Unable to Update User Email");
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(EmailEdtActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, EmailEdtActivity.this);
    }

}
