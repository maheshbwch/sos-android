package com.sosandroid.mobile.activities.account;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;

public class ForgotPassword extends AppCompatActivity {

    private Context context;
    private TextView txtPhone;
    private LinearLayout linearContinue;
    private String phoneNumber,countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context = ForgotPassword.this;
        init();
        listener();
    }

    private void init() {

        Intent intent = getIntent();

        txtPhone = findViewById(R.id.afp_txtPhone);
        linearContinue = findViewById(R.id.afp_bottomLinear);

         countryCode = intent.getStringExtra(Constants.COUNTRY_CODE);
         phoneNumber = intent.getStringExtra(Constants.PHONE_NUMBER);

        if (MyUtils.checkStringValue(phoneNumber) && (MyUtils.checkStringValue(countryCode))) {

            countryCode = countryCode.contains("+") ? countryCode : ("+" + countryCode);

            String phoneWithIsd = countryCode + phoneNumber;
            txtPhone.setText(phoneWithIsd);
        }
    }

    private void listener() {

        linearContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ForgotPasswordDetail.class);
                intent.putExtra(Constants.PHONE_NUMBER,phoneNumber);
                intent.putExtra(Constants.COUNTRY_CODE,countryCode);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, ForgotPassword.this);

            }
        });
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, ForgotPassword.this);
    }

}
