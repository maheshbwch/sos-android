package com.sosandroid.mobile.activities.registration;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.HomeActivity;
import com.sosandroid.mobile.adapters.ContactsAdapter;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.interfaces.OnContactSelected;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.models.ContactModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddContactActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextView skipTxt, addContactTxt, contactCountTxt;
    private RecyclerView contactRecyclerView;
    private LinearLayout bottomLinear;
    private ProgressDialog progressDialog = null;

    ArrayList<ContactModel> contactModelArrayList = new ArrayList<>();
    private ContactsAdapter contactsAdapter = null;

    String userId = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contacts);
        context = AddContactActivity.this;

        toolbar = findViewById(R.id.aac_toolbar);
        skipTxt = findViewById(R.id.aac_skipTxt);
        addContactTxt = findViewById(R.id.aac_addContactTxt);
        contactCountTxt = findViewById(R.id.aac_contactCountTxt);
        contactRecyclerView = findViewById(R.id.aac_contactRecyclerView);
        bottomLinear = findViewById(R.id.aac_bottomLinear);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getString(Constants.USER_ID);

        listeners();

    }


    private void listeners() {
        contactRecyclerView.setHasFixedSize(true);
        contactRecyclerView.setLayoutManager(new LinearLayoutManager(context));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });


        addContactTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToContactsPage();
            }
        });

        skipTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToHomePage();
            }
        });


        bottomLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomLinear.isFocusable()) {

                    try {
                        /*JSONArray jsonArray = new JSONArray();
                        for (ContactModel contactModel : contactModelArrayList) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("contact_name", "" + contactModel.getName());
                            jsonObject.put("contact_number", "" + contactModel.getPhoneNumber());
                            jsonArray.put(jsonObject);
                        }*/

                        Gson gson = new GsonBuilder().create();
                        JsonArray myCustomArray = gson.toJsonTree(contactModelArrayList).getAsJsonArray();
                        Log.e("data", "data:" + myCustomArray.toString());

                        saveContacts(myCustomArray.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void saveContacts(String contactsJson) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Adding Contacts..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.saveContacts("emergency_contacts", userId, contactsJson);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                showAlertToUser(Constants.DIALOG_SUCCESS, "Add Contacts", MyUtils.checkStringValue(message) ? message : "Contacts Added Successfully");

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", MyUtils.checkStringValue(message) ? message : "Unable to Add Contacts");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(AddContactActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                intentToHomePage();
            }

            @Override
            public void onDismissedClicked() {
                intentToHomePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    private void adaptContactListToRecycler() {
        if (contactModelArrayList.size() > 0) {
            if (contactsAdapter != null) {
                contactsAdapter.notifyDataSetChanged();
            } else {
                contactsAdapter = new ContactsAdapter(context, contactModelArrayList, new OnContactSelected() {
                    @Override
                    public void onSelected(ContactModel contactModel) {

                    }

                    @Override
                    public void onDeSelected(ContactModel contactModel) {

                    }
                }, Constants.CONTACTS_REGISTRATION);
                contactRecyclerView.setAdapter(contactsAdapter);
            }
            contactCountTxt.setText("" + (contactModelArrayList.size()) + " Contacts");
            enableBottomButton();
        }
    }

    private void enableBottomButton() {
        bottomLinear.setAlpha(1);
        bottomLinear.setFocusable(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("requestCode", "code:" + requestCode);
        Log.e("resultCode", "code:" + resultCode);

        switch (resultCode) {
            case Activity.RESULT_OK:
                Bundle b = data.getExtras();

                ArrayList<ContactModel> contactArrayList = (ArrayList<ContactModel>) b.getSerializable(Constants.selectedContacts);
                if (contactArrayList != null) {
                    contactModelArrayList.clear();
                    contactModelArrayList.addAll(contactArrayList);
                }

                adaptContactListToRecycler();

                Log.e("selected11", "size:" + contactModelArrayList.size());
                break;
            case Constants.REGISTRATION_DECLINED:
                setResult(Constants.REGISTRATION_DECLINED);
                finish();
                break;
            default:
                break;
        }
    }

    private void intentToHomePage() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivityForResult(intent, Constants.REGISTRATION);
        MyUtils.openOverrideAnimation(true, AddContactActivity.this);
    }


    private void intentToContactsPage() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.selectedContacts, contactModelArrayList);
        Intent intent = new Intent(context, ContactsActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.GET_CONTACTS);
        MyUtils.openOverrideAnimation(true, AddContactActivity.this);
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        setResult(Constants.REGISTRATION_DECLINED);
        finish();
        MyUtils.openOverrideAnimation(false, AddContactActivity.this);
    }


}
