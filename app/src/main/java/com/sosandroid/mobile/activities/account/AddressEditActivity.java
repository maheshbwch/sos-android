package com.sosandroid.mobile.activities.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressEditActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout homeAddressTil,officeAddressTil,schoolAddressTil;
    private TextInputEditText homeAddressEdt,officeAddressEdt,schoolAddressEdt;
    private LinearLayout bottomLinear = null;
    private ProgressDialog progressDialog = null;

    String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_edit);
        context = AddressEditActivity.this;

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.aae_toolbar);
        homeAddressTil = findViewById(R.id.aae_homeAddressTil);
        officeAddressTil = findViewById(R.id.aae_officeAddressTil);
        schoolAddressTil = findViewById(R.id.aae_schoolAddressTil);

        homeAddressEdt = findViewById(R.id.aae_homeAddressEdt);
        officeAddressEdt = findViewById(R.id.aae_officeAddressEdt);
        schoolAddressEdt = findViewById(R.id.aae_schoolAddressEdt);

        bottomLinear = findViewById(R.id.aae_bottomLinear);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        listeners();
    }

    private void listeners() {
        homeAddressTil.setTypeface(MyUtils.getRegularFont(context));
        officeAddressTil.setTypeface(MyUtils.getRegularFont(context));
        schoolAddressTil.setTypeface(MyUtils.getRegularFont(context));

        homeAddressEdt.setTypeface(MyUtils.getRegularFont(context));
        officeAddressEdt.setTypeface(MyUtils.getRegularFont(context));
        schoolAddressEdt.setTypeface(MyUtils.getRegularFont(context));

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        Log.e("userID", ":" + userId);
        String homeAddress = PreferenceHandler.getPreferenceFromString(context, Constants.HOME_ADDRESS);
        String officeAddress = PreferenceHandler.getPreferenceFromString(context, Constants.OFFICE_ADDRESS);
        String schoolAddress = PreferenceHandler.getPreferenceFromString(context, Constants.SCHOOL_ADDRESS);

        if (MyUtils.checkStringValue(homeAddress)){
            homeAddressEdt.setText(homeAddress);
        }

        if (MyUtils.checkStringValue(officeAddress)){
            officeAddressEdt.setText(officeAddress);
        }

        if (MyUtils.checkStringValue(schoolAddress)){
            schoolAddressEdt.setText(schoolAddress);
        }

        bottomLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String homeAddress = ""+homeAddressEdt.getText().toString().trim();
                String officeAddress = ""+officeAddressEdt.getText().toString().trim();
                String schoolAddress = ""+schoolAddressEdt.getText().toString().trim();

                if (MyUtils.checkStringValue(homeAddress) || MyUtils.checkStringValue(officeAddress) || MyUtils.checkStringValue(schoolAddress)){
                    updateAddress(homeAddress,officeAddress,schoolAddress);
                }

            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });
    }



    private void updateAddress(final String homeAddress, final String officeAddress, final String schoolAddress) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating Address..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.updateAddress("edit_address", userId, homeAddress, officeAddress,schoolAddress);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                PreferenceHandler.storePreference(context, Constants.HOME_ADDRESS, homeAddress);
                                PreferenceHandler.storePreference(context, Constants.OFFICE_ADDRESS, officeAddress);
                                PreferenceHandler.storePreference(context, Constants.SCHOOL_ADDRESS, schoolAddress);
                                showAlertToUser(Constants.DIALOG_SUCCESS, "Update Addresses", MyUtils.checkStringValue(message) ? message : "Address Updated Successfully");

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update Addresses", MyUtils.checkStringValue(message) ? message : "Unable to Update Address");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update Addresses", "Unable to Update Address");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Update Addresses", "Unable to Update Address");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Addresses", "Unable to Update Address");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Addresses", "Unable to Update Address");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(AddressEditActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                //closePage();
            }

            @Override
            public void onDismissedClicked() {
                //closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }



    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AddressEditActivity.this);
    }


}
