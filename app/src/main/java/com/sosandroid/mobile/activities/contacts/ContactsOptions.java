package com.sosandroid.mobile.activities.contacts;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.models.ContactDetails;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsOptions extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ProgressDialog progressDialog = null;
    private RadioButton radioWhatsapp, radioTelegram, radioLine, radioViber, radioCall;

    /* private CheckBox checkBoxWhatsapp, checkBoxTelegram, checkBoxLine, checkBoxViber, checkBoxCall;*/
    private TextView txtClick;

    private String userId = null;
    private String contactId = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_options);
        context = ContactsOptions.this;

        init();

    }

    private void init() {

        toolbar = findViewById(R.id.ac_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

       /* checkBoxWhatsapp = findViewById(R.id.aco_whatsAppCheckBox);
        checkBoxTelegram = findViewById(R.id.aco_telegramCheckBox);
        checkBoxLine = findViewById(R.id.aco_lineCheckBox);
        checkBoxViber = findViewById(R.id.aco_viberCheckBox);
        checkBoxCall = findViewById(R.id.aco_callCheckBox);*/

        txtClick = findViewById(R.id.ac_checkTxt);
        radioWhatsapp = findViewById(R.id.aco_whatsAppRadio);
        radioTelegram = findViewById(R.id.aco_telegramRadio);
        radioLine = findViewById(R.id.aco_lineRadio);
        radioViber = findViewById(R.id.aco_viberRadio);
        radioCall = findViewById(R.id.aco_callRadio);


        listeners();
    }

    private void listeners() {

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        Intent intent = getIntent();
        contactId = intent.getStringExtra(Constants.CONTACT_ID);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                MyUtils.openOverrideAnimation(false, ContactsOptions.this);
            }
        });

        txtClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (MyUtils.isInternetConnected(context)) {

                    String whatsAppCheckStatus, telegramCheckStatus, lineCheckStatus, viberCheckStatus, callCheckStatus;

                    if (radioWhatsapp.isChecked()) {
                        whatsAppCheckStatus = "1";
                    } else {
                        whatsAppCheckStatus = "0";
                    }

                    if (radioTelegram.isChecked()) {
                        telegramCheckStatus = "1";
                    } else {
                        telegramCheckStatus = "0";
                    }

                    if (radioLine.isChecked()) {
                        lineCheckStatus = "1";
                    } else {
                        lineCheckStatus = "0";
                    }

                    if (radioViber.isChecked()) {
                        viberCheckStatus = "1";
                    } else {
                        viberCheckStatus = "0";
                    }

                    if (radioCall.isChecked()) {
                        callCheckStatus = "1";
                    } else {
                        callCheckStatus = "0";
                    }

                    updateClickStaus(whatsAppCheckStatus, telegramCheckStatus, lineCheckStatus, viberCheckStatus, callCheckStatus);
                } else {

                    MyUtils.showLongToast(context, String.valueOf(R.string.internet_not_working));
                }

            }
        });


        getContactDetails();

    }


    private void getContactDetails() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Getting..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ContactDetails> call = apiService.getContactDetails("read_contact_details", userId, contactId);
            call.enqueue(new Callback<ContactDetails>() {
                @Override
                public void onResponse(Call<ContactDetails> call, Response<ContactDetails> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        ContactDetails contactsDetails = response.body();

                        if (contactsDetails != null) {
                            String status = contactsDetails.getStatus();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                String whatsappStatus = contactsDetails.getData().getWhatsappNotification();
                                String telegramStatus = contactsDetails.getData().getTelegramNotification();
                                String lineStatus = contactsDetails.getData().getLineNotification();
                                String viberStatus = contactsDetails.getData().getViberNotification();
                                String callStatus = contactsDetails.getData().getCall();

                                updateUI(whatsappStatus, telegramStatus, lineStatus, viberStatus, callStatus);
                            }
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<ContactDetails> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void updateUI(String whatsappStatus, String telegramStatus, String lineStatus, String viberStatus, String callStatus) {

        Log.e("whatsapStatus", ":" + whatsappStatus);
        Log.e("contactId", ":" + contactId);

        if (whatsappStatus.equalsIgnoreCase("1")) {
            radioWhatsapp.setChecked(true);
        } else if (telegramStatus.equalsIgnoreCase("1")) {
            radioTelegram.setChecked(true);
        } else if (lineStatus.equalsIgnoreCase("1")) {
            radioLine.setChecked(true);
        } else if (viberStatus.equalsIgnoreCase("1")) {
            radioViber.setChecked(true);
        } else if (callStatus.equalsIgnoreCase("1")) {
            radioCall.setChecked(true);
        }

    }


    private void updateClickStaus(String whatsAppCheckStatus, String telegramCheckStatus, String lineCheckStatus, String viberCheckStatus, String callCheckStatus) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.updateStatus("edit_contact_options", userId, contactId, whatsAppCheckStatus, telegramCheckStatus, lineCheckStatus, viberCheckStatus, callCheckStatus);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        BaseModel baseModel = response.body();
                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                //showAlertToUser(Constants.DIALOG_SUCCESS, "Update Status", MyUtils.checkStringValue(message) ? message : "Status Updated Successfully");
                                MyUtils.showLongToast(context, "Status Updated Successfully");
                                finish();
                                MyUtils.openOverrideAnimation(false, ContactsOptions.this);

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update Status", MyUtils.checkStringValue(message) ? message : "Unable to Update Status");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update Status", "Unable to Update Status");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Update Status", "Unable to Update Status");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Status", "Unable to Update Status");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Status", "Unable to Update Status");
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(ContactsOptions.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, ContactsOptions.this);
    }


}
