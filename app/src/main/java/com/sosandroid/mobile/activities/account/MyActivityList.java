package com.sosandroid.mobile.activities.account;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.MyActivityAdapter;
import com.sosandroid.mobile.models.GetActivityListModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyActivityList extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ProgressBar progressBar, bottomProgressBar;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<GetActivityListModel.Notification> notificationArrayList = new ArrayList<>();
    private MyActivityAdapter myActivityAdapter = null;
    private RecyclerView activityRecyclerView;

    private String userId = null;
    private boolean isLoading = true;
    private int pageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);
        context = MyActivityList.this;

        toolbar = findViewById(R.id.aml_toolbar);
        progressBar = findViewById(R.id.fp_progressBar);
        bottomProgressBar = findViewById(R.id.fp_bottomProgressBar);
        activityRecyclerView = findViewById(R.id.aml_recyclerView);

        linearLayoutManager = new LinearLayoutManager(context);
        activityRecyclerView.setLayoutManager(linearLayoutManager);
        activityRecyclerView.setHasFixedSize(true);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        getActivityList(0, true);
    }

    private void getActivityList(int pageNumber, final boolean isLoadingForFirstTime) {
        try {
            if (isLoadingForFirstTime) {
                notificationArrayList.clear();
                progressBar.setVisibility(View.VISIBLE);
            } else {
                bottomProgressBar.setVisibility(View.VISIBLE);
            }

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<GetActivityListModel> call = apiService.getActivityList("get_activity", userId, String.valueOf(pageNumber));
            call.enqueue(new Callback<GetActivityListModel>() {
                @Override
                public void onResponse(Call<GetActivityListModel> call, Response<GetActivityListModel> response) {
                    hideProgressBar(isLoadingForFirstTime);
                    if (response.isSuccessful()) {
                        GetActivityListModel postModel = response.body();
                        if (postModel != null && postModel.getNotification().size() > 0) {
                            ArrayList<GetActivityListModel.Notification> arrayList = new ArrayList<>();
                            arrayList.addAll(postModel.getNotification());
                            if (arrayList.size() > 0) {
                                notificationArrayList.addAll(arrayList);
                                adaptvalues(notificationArrayList);
                            }
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<GetActivityListModel> call, Throwable t) {
                    hideProgressBar(isLoadingForFirstTime);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            hideProgressBar(isLoadingForFirstTime);
            if (!isLoadingForFirstTime) {
                isLoading = isLoadingForFirstTime;
            }
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void adaptvalues(ArrayList<GetActivityListModel.Notification> notifications) {

        if (notifications.size() > 0) {
            if (myActivityAdapter != null) {
                myActivityAdapter.notifyDataSetChanged();
                isLoading = true;
            } else {
                myActivityAdapter = new MyActivityAdapter(context, notifications);
                activityRecyclerView.setAdapter(myActivityAdapter);
            }

            activityRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                isLoading = false;
                                Log.e("recyclerView", "bottom_reached");
                                pageNumber = pageNumber + 1;

                                getActivityList(pageNumber, false);

                                Log.e("page_number", "number:" + pageNumber);
                            }
                        }
                    }
                }
            });
        }
    }


    private void hideProgressBar(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }
        } else {
            hideBottomProgressBar();
        }
    }

    private void hideBottomProgressBar() {
        if (bottomProgressBar.getVisibility() == View.VISIBLE) {
            bottomProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, MyActivityList.this);
    }


}
