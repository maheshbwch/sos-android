package com.sosandroid.mobile.activities.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.OtpVerifyActivity;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.models.ValidateUser;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneEditActivity extends AppCompatActivity {


    private Context context;
    private Toolbar toolbar;
    private TextView saveTxt;
    private TextInputLayout userPhoneTil;
    private TextInputEditText userPhoneEdt;
    private ProgressDialog progressDialog = null;

    private String userId = null;
    private String countryCodeId = null;
    String countryCode = "";
    String phoneWithCode = null;
    String phoneNumber = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_edit);
        context = PhoneEditActivity.this;

        init();
    }


    private void init() {
        toolbar = findViewById(R.id.aph_toolbar);
        saveTxt = findViewById(R.id.aph_saveTxt);
        userPhoneTil = findViewById(R.id.aph_phoneNumberTil);
        userPhoneEdt = findViewById(R.id.aph_phoneNumberEdt);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        userPhoneTil.setTypeface(MyUtils.getRegularFont(context));
        userPhoneEdt.setTypeface(MyUtils.getRegularFont(context));


        listeners();
    }

    private void listeners() {
        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        Log.e("userID", ":" + userId);

        String userPhone = PreferenceHandler.getPreferenceFromString(context, Constants.PHONE_NUMBER);
        countryCodeId = PreferenceHandler.getPreferenceFromString(context, Constants.COUNTRY_CODE_ID);
        countryCode = PreferenceHandler.getPreferenceFromString(context, Constants.COUNTRY_CODE);

        countryCode = MyUtils.checkStringValue(countryCode) ? countryCode : "";

        countryCode = countryCode.contains("+") ? countryCode : ("+"+countryCode);


        if (MyUtils.checkStringValue(userPhone)) {
            userPhoneEdt.setText(countryCode + userPhone);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        saveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phoneWithCode = userPhoneEdt.getText().toString().trim();
                phoneNumber = phoneWithCode.replace(countryCode, "");
                if (!MyUtils.checkStringValue(phoneNumber)) {
                    MyUtils.showLongToast(context, "Phone Number required");
                } else {
                    validateIfUserAlreadyRegistered(countryCodeId, countryCode, phoneNumber);

                }

            }
        });


        userPhoneEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.e("textChange", "after:" + s.toString());
                if (s.length() == countryCode.length() - 1) {
                    userPhoneEdt.setText(countryCode);
                    userPhoneEdt.setSelection(countryCode.length());
                } else {
                    //checkBottomLinearStatus(s.length() != countryCode.length());
                }
            }
        });
    }


    private void validateIfUserAlreadyRegistered(final String countryCodeId, final String countryCode, final String phoneNumber) {
        try {
            Log.e("countryCodeId", "id:" + countryCodeId);
            progressDialog = MyUtils.showProgressLoader(context, "Validating Phone Number..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ValidateUser> call = apiService.validateIfUserAlreadyRegistered("existing_user_check", countryCodeId, phoneNumber);
            call.enqueue(new Callback<ValidateUser>() {
                @Override
                public void onResponse(Call<ValidateUser> call, Response<ValidateUser> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        ValidateUser validateUser = response.body();

                        if (validateUser != null) {
                            String status = validateUser.getStatus();

                            if (MyUtils.checkStringValue(status)) {
                                if (status.equalsIgnoreCase(Constants.SUCCESS)) {
                                    showPhoneNumberAlert(Constants.DIALOG_GENERAL, "Verify Phone Number " + countryCode + phoneNumber, "OTP message will be sent to given Phone Number", true, phoneNumber);
                                } else if (status.equalsIgnoreCase(Constants.FAILURE)) {
                                    //String userName = "" + validateUser.getData().getUserName();
                                    showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "This mobile number already exists, please choose another number");
                                }
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
                    }
                }

                @Override
                public void onFailure(Call<ValidateUser> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
        }
    }

    private void showPhoneNumberAlert(String statusCode, String title, String statusMessage, final boolean state, final String phoneNumber) {
        CustomDialog customDialog = new CustomDialog(PhoneEditActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (state) {
                    sendVerificationCode(countryCode.trim() + phoneNumber);
                }
            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }

    private void sendVerificationCode(String mobile) {
        progressDialog = MyUtils.showProgressLoader(context, "Sending Code...");
        PhoneAuthProvider provider = PhoneAuthProvider.getInstance();
        provider.verifyPhoneNumber(mobile, Constants.OTP_TIME_OUT, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, mCallbacks);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.e("verification", "code:" + code);
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(PhoneEditActivity.this, "code:" + code, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.e("verification", "exp:" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(PhoneEditActivity.this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            Log.e("verification", "id:" + verificationId);
            MyUtils.dismissProgressLoader(progressDialog);
            intentToOtpVerifyPage(verificationId);

        }
    };

    private void intentToOtpVerifyPage(String verificationId) {
        //String password = passwordEdt.getText().toString().trim();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.COUNTRY_CODE_ID, countryCodeId);
        bundle.putString(Constants.COUNTRY_CODE, countryCode);
        bundle.putString(Constants.PHONE_NUMBER, phoneNumber);
        // bundle.putString(Constants.PASSWORD, password);
        bundle.putString(Constants.VERIFICATION_ID, verificationId);
        bundle.putString(Constants.IS_FROM, Constants.IS_FROM_PHONE_NUMBER_EDIT);

        Intent intent = new Intent(context, OtpVerifyActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.PHONE_NUMBER_EDIT);
        MyUtils.openOverrideAnimation(true, PhoneEditActivity.this);
    }

    private void updatePhone(final String phone) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating Phone Number..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.updatePhoneNumber("mobile_number", userId, phone, countryCodeId);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                //TODO HANDLE OTP
                                //PreferenceHandler.storePreference(context, Constants.EMAIL, email);
                                showAlertToUser(Constants.DIALOG_SUCCESS, "Update Phone", MyUtils.checkStringValue(message) ? message : "User Phone Updated Successfully");
                                PreferenceHandler.storePreference(context, Constants.PHONE_NUMBER, phone);

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update Phone", MyUtils.checkStringValue(message) ? message : "Unable to Update User Phone");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update Phone", "Unable to Update User Phone");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Update Phone", "Unable to Update User Phone");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Phone", "Unable to Update User Phone");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Phone", "Unable to Update User Phone");
        }
    }


    private void showAlertToUser(final String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(PhoneEditActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (statusCode.equalsIgnoreCase(Constants.DIALOG_SUCCESS)) {
                    closePage();
                }
            }

            @Override
            public void onDismissedClicked() {
                if (statusCode.equalsIgnoreCase(Constants.DIALOG_SUCCESS)) {
                    closePage();
                }
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, PhoneEditActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Constants.PHONE_NUMBER_EDIT) {
            updatePhone(phoneNumber);
        }

    }
}
