package com.sosandroid.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.IntroPagerAdapter;
import com.sosandroid.mobile.utils.Constants;

import java.util.ArrayList;

public class WelcomeActivity extends AppCompatActivity {

    private Context context;
    private ViewPager viewPager;
    private LinearLayout pagerDotsLinear;
    private TextView sliderTxt, getStartedTxt, signInTxt;


    private static final String sliderTxt1 = "Know the location of your family and friends";
    private static final String sliderTxt2 = "24 Hours Operated for SOS \n";
    private static final String sliderTxt3 = "Notify all your loved ones during emergency";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_welcome);
        context = WelcomeActivity.this;

        viewPager = findViewById(R.id.aw_view_pager);
        pagerDotsLinear = findViewById(R.id.aw_pager_dots);
        sliderTxt = findViewById(R.id.aw_sliderTxt);
        getStartedTxt = findViewById(R.id.aw_getStartedTxt);
        signInTxt = findViewById(R.id.aw_signInTxt);

        listeners();
    }


    private void listeners() {

        final ArrayList<Integer> sliderArrayList = new ArrayList<>();
        sliderArrayList.add(R.mipmap.screen1_slider);
        sliderArrayList.add(R.mipmap.screen2_slider);
        sliderArrayList.add(R.mipmap.screen3_slider);

        IntroPagerAdapter sizeAdapter = new IntroPagerAdapter(WelcomeActivity.this, sliderArrayList);
        viewPager.setAdapter(sizeAdapter);

        addBottomDots(0, sliderArrayList.size());
        sliderTxt.setText(sliderTxt1);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position, sliderArrayList.size());

                if (position == 0) {
                    sliderTxt.setText(sliderTxt1);
                } else if (position == 1) {
                    sliderTxt.setText(sliderTxt2);
                } else if (position == 2) {
                    sliderTxt.setText(sliderTxt3);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        getStartedTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToPhoneNumberPage(/*Constants.IS_FROM_SIGN_UP*/);
            }
        });


        signInTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToPhoneNumberPage(/*Constants.IS_FROM_LOGIN*/);
            }
        });
    }

    private void addBottomDots(int currentPage, int size) {
        if (size > 0) {
            TextView[] dots = new TextView[size];

            pagerDotsLinear.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(this);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(60);
                dots[i].setTextColor(Color.parseColor("#D4D8DB"));
                pagerDotsLinear.addView(dots[i]);
            }

            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
            dots[currentPage].setTextSize(60);
        }

    }


    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    private void intentToPhoneNumberPage(/*String isFrom*/) {
        /*Bundle bundle = new Bundle();
        bundle.putString(Constants.IS_FROM, isFrom);*/
        Intent intent = new Intent(context, PhoneNumberActivity.class);
        //intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REGISTRATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constants.REGISTRATION_DECLINED:
                setResult(Constants.REGISTRATION_DECLINED);
                finish();
                break;
            default:
                break;
        }
    }


}
