package com.sosandroid.mobile.activities.account;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class AccountSettingActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ImageView userImage;
    private LinearLayout userNameLinear, editPhoneLinear, editEmailLinear, changePasswordLinear;
    private TextView userNameText;

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setting);
        context = AccountSettingActivity.this;

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.aas_toolbar);
        userImage = findViewById(R.id.aas_userImage);
        userNameLinear = findViewById(R.id.aas_userNameLinear);
        userNameText = findViewById(R.id.aas_userNameText);
        editPhoneLinear = findViewById(R.id.aas_editPhoneLinear);
        editEmailLinear = findViewById(R.id.aas_editEmailLinear);
        changePasswordLinear = findViewById(R.id.aas_changePasswordLinear);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        listeners();
    }

    private void listeners() {


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        userNameLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileEditActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountSettingActivity.this);
            }
        });

        editPhoneLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PhoneEditActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountSettingActivity.this);
            }
        });


        editEmailLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EmailEdtActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountSettingActivity.this);
            }
        });


        changePasswordLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChangePasswordActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountSettingActivity.this);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        String userName = PreferenceHandler.getPreferenceFromString(context, Constants.USER_NAME);
        String profileUrl = PreferenceHandler.getPreferenceFromString(context, Constants.PROFILE_IMG_URL);
        Log.e("profileUrl", ":" + profileUrl);
        Log.e("userName", ":" + userName);
        if (MyUtils.checkStringValue(userName)) {
            userNameText.setText(userName);
        }

        if (MyUtils.checkStringValue(profileUrl)) {
            loadImageSource(profileUrl);
        }

    }

    private void loadImageSource(String profileUrl) {
        Glide.with(context).load(profileUrl).placeholder(R.drawable.user_pic).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(userImage);
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AccountSettingActivity.this);
    }

}
