package com.sosandroid.mobile.activities.account;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.BenefitsAdapter;
import com.sosandroid.mobile.models.PremiumBenefitsList;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PremiumBenefitsActivity extends AppCompatActivity {


    private Context context;
    private Toolbar toolbar;
    private LinearLayout plansLinear;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    ArrayList<PremiumBenefitsList.PremiumBenefit> premiumBenefitsListArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_benefits);
        context = PremiumBenefitsActivity.this;

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.apb_toolbar);
        plansLinear = findViewById(R.id.apb_plansLinear);
        recyclerView = findViewById(R.id.apb_recyclerView);
        progressBar = findViewById(R.id.apb_progress);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }


        listeners();
    }

    private void listeners() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        getBenefitsList();
    }

    private void getBenefitsList() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<PremiumBenefitsList> call = apiService.getPremiumBenefitsList();
            call.enqueue(new Callback<PremiumBenefitsList>() {
                @Override
                public void onResponse(Call<PremiumBenefitsList> call, Response<PremiumBenefitsList> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {

                        PremiumBenefitsList premiumBenefitsList = response.body();

                        if (premiumBenefitsList != null) {

                            premiumBenefitsListArrayList.clear();
                            premiumBenefitsListArrayList.addAll(premiumBenefitsList.getPremiumBenefits());

                            adaptContactListToRecycler();

                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<PremiumBenefitsList> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
            progressBar.setVisibility(View.GONE);
        }
    }


    private void adaptContactListToRecycler() {
        if (premiumBenefitsListArrayList.size() > 0) {
            enablePlansLinear();
            BenefitsAdapter contactsAdapter = new BenefitsAdapter(context, premiumBenefitsListArrayList);
            recyclerView.setAdapter(contactsAdapter);
        }
    }


    private void enablePlansLinear() {
        if (plansLinear.getVisibility() == View.GONE) {
            plansLinear.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, PremiumBenefitsActivity.this);
    }


}
