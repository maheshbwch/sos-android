package com.sosandroid.mobile.activities.account;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.PhoneNumberActivity;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassWordFinal extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextView saveTxt;
    private TextInputLayout txtNewPassTil, txtConfirmPass;
    private TextInputEditText edtNewPass, edtConfirmPass;

    private ProgressDialog progressDialog = null;

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass_word_final);
        context = ForgotPassWordFinal.this;

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.afp_toolbar);
        saveTxt = findViewById(R.id.afp_saveTxt);

        txtNewPassTil = findViewById(R.id.afp_newTxtPass);
        txtConfirmPass = findViewById(R.id.afp_ConfirmTxtPass);

        edtNewPass = findViewById(R.id.afp_edtNewPass);
        edtConfirmPass = findViewById(R.id.afp_edtConfirmPass);

        setSupportActionBar(toolbar);

        txtNewPassTil.setTypeface(MyUtils.getRegularFont(context));
        txtConfirmPass.setTypeface(MyUtils.getRegularFont(context));


        edtNewPass.setTypeface(MyUtils.getRegularFont(context));
        edtConfirmPass.setTypeface(MyUtils.getRegularFont(context));


        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        listeners();
    }

    private void listeners() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        saveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MyUtils.isInternetConnected(ForgotPassWordFinal.this)) {
                    validEditText();
                } else {
                    MyUtils.showLongToast(context, String.valueOf(R.string.internet_not_working));
                }

            }
        });

    }

    private void validEditText() {

        String newPassword = edtNewPass.getText().toString();
        String confirmPassword = edtConfirmPass.getText().toString();

        if (!MyUtils.checkStringValue(newPassword)) {
            MyUtils.showLongToast(context, "New Password required");
        } else if (!MyUtils.checkStringValue(confirmPassword)) {
            MyUtils.showLongToast(context, "Confirm New Password required");
        } else {
            if (newPassword.equals(confirmPassword)) {
                forgotPass(newPassword);
            } else {
                MyUtils.showLongToast(context, "New Password and Confirm New Password incorrect");
            }

        }
    }

    private void forgotPass(String newPassword) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.forgotPassword("forgot_password", userId, newPassword);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                //showAlertToUser(Constants.DIALOG_SUCCESS, "Forgot Password", MyUtils.checkStringValue(message) ? message : "User Password Updated Successfully");
                                MyUtils.showLongToast(context, "Password Updated");
                                Intent intent = new Intent(context, PhoneNumberActivity.class);
                                startActivity(intent);
                                MyUtils.openOverrideAnimation(false, ForgotPassWordFinal.this);
                                finish();
                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Forgot Password", MyUtils.checkStringValue(message) ? message : "Unable to Update User Password");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Forgot Password", "Unable to Update User Password");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Forgot Password", "Unable to Update User Password");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Forgot Password", "Unable to Update User Password");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Forgot Password", "Unable to Update User Password");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(ForgotPassWordFinal.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, ForgotPassWordFinal.this);
    }

}
