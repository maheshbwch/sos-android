package com.sosandroid.mobile.activities.registration;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.CustomAlertInterface;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.interfaces.UploadAlertListener;
import com.sosandroid.mobile.models.Profile;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomAlert;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PermissionAlert;
import com.sosandroid.mobile.utils.PermissionRequest;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.utils.RealPathUtil;
import com.sosandroid.mobile.utils.UploadAlert;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {


    private Context context;
    private Toolbar toolbar;
    private RelativeLayout profileImageRelative;
    private ImageView profileImageView;
    private TextInputLayout userNameTil, userAgeTil, userEmailTil;
    private TextInputEditText userNameEdt, userAgeEdt, userEmailEdt;
    private MaterialBetterSpinner raceSpinner;
    private LinearLayout bottomLinear;

    private ProgressDialog progressDialog = null;

    private String userId = null;
    private String race = null;
    private String imageFilePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        context = ProfileActivity.this;

        toolbar = findViewById(R.id.apr_toolbar);
        profileImageRelative = findViewById(R.id.apr_profileImageRelative);
        profileImageView = findViewById(R.id.apr_profileImageView);
        userNameTil = findViewById(R.id.apr_userNameTil);
        userNameEdt = findViewById(R.id.apr_userNameEdt);
        userAgeTil = findViewById(R.id.apr_userAgeTil);
        userAgeEdt = findViewById(R.id.apr_userAgeEdt);
        userEmailTil = findViewById(R.id.apr_userEmailTil);
        userEmailEdt = findViewById(R.id.apr_userEmailEdt);
        raceSpinner = findViewById(R.id.apr_raceSpinner);
        bottomLinear = findViewById(R.id.apr_bottomLinear);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getString(Constants.USER_ID);

        listeners();
    }


    private void listeners() {

        userNameTil.setTypeface(MyUtils.getRegularFont(context));
        userNameEdt.setTypeface(MyUtils.getRegularFont(context));
        userAgeTil.setTypeface(MyUtils.getRegularFont(context));
        userAgeEdt.setTypeface(MyUtils.getRegularFont(context));
        userEmailTil.setTypeface(MyUtils.getRegularFont(context));
        userEmailEdt.setTypeface(MyUtils.getRegularFont(context));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        adaptSpinnerValues();

        profileImageRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUploadAlert();
            }
        });

        bottomLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = userNameEdt.getText().toString().trim();
                String age = userAgeEdt.getText().toString().trim();
                String email = userEmailEdt.getText().toString().trim();

                if (!MyUtils.checkStringValue(userName)) {
                    MyUtils.showLongToast(context, "User Name required");
                } else if ((!MyUtils.checkStringValue(age))) {
                    MyUtils.showLongToast(context, "Age required");
                } else if (!MyUtils.checkStringValue(race)) {
                    MyUtils.showLongToast(context, "Race required");
                } else if ((!MyUtils.checkStringValue(email))) {
                    MyUtils.showLongToast(context, "Email required");
                } else if (!MyUtils.isValidEmail(email)) {
                    MyUtils.showLongToast(context, "Valid Email required");
                } else if (!MyUtils.checkStringValue(imageFilePath)) {
                    showImageAttachAlert();
                } else {
                    updateUserProfileInfo(userId);
                }

            }
        });
    }


    private void updateUserProfileInfo(final String userId) {

        final String userName = userNameEdt.getText().toString().trim();
        final String age = userAgeEdt.getText().toString().trim();
        final String email = userEmailEdt.getText().toString().trim();

        Log.e("update_profile", "imageFilePath:" + imageFilePath + "\n" + "userId:" + userId + "\n" + "userName:" + userName + "\n" + "age:" + age + "\n" + "email:" + email);

        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("type", RequestBody.create(MediaType.parse("text/plain"), "edit_user_details"));
            map.put("user_id", RequestBody.create(MediaType.parse("text/plain"), userId));
            map.put("user_name", RequestBody.create(MediaType.parse("text/plain"), userName));
            map.put("age", RequestBody.create(MediaType.parse("text/plain"), age));
            map.put("race", RequestBody.create(MediaType.parse("text/plain"), race));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), email));

            MultipartBody.Part body = null;
            if (MyUtils.checkStringValue(imageFilePath)) {
                File file = new File(imageFilePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
                body = MultipartBody.Part.createFormData("profile_file", file.getName(), requestFile);
            } else {
                body = MultipartBody.Part.createFormData("profile_file", "");
            }

            Log.e("image_path", "file:" + imageFilePath);

            progressDialog = MyUtils.showProgressLoader(context, "Uploading...");

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<Profile> call = apiService.updateProfileInfo(map, body);
            call.enqueue(new Callback<Profile>() {
                @Override
                public void onResponse(Call<Profile> call, Response<Profile> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        Log.e("response", "code" + response.code());
                        Profile profile = response.body();
                        if (profile != null) {
                            String status = profile.getStatus();
                            String message = profile.getMessage();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                String imageUrl = profile.getProfileImage();

                                PreferenceHandler.storePreference(context, Constants.PROFILE_IMG_URL, imageUrl);
                                PreferenceHandler.storePreference(context, Constants.USER_NAME, userName);
                                PreferenceHandler.storePreference(context, Constants.AGE, age);
                                PreferenceHandler.storePreference(context, Constants.RACE, race);
                                PreferenceHandler.storePreference(context, Constants.EMAIL, email);


                                showAlertToUser(Constants.DIALOG_SUCCESS, "Update Profile", (MyUtils.checkStringValue(message) ? message : "Profile Image Update Successfully"));
                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", (MyUtils.checkStringValue(message) ? message : "Unable to update Profile Image"));
                            }

                        } else {
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", "Unable to update Profile");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Profile> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", "Unable to update Profile");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", "code" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", "Unable to update Profile");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(ProfileActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                intentToAddContactPage();
            }

            @Override
            public void onDismissedClicked() {
                intentToAddContactPage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    private void showImageAttachAlert() {
        CustomAlert customAlert = new CustomAlert(context, "You are missing your profile photo", "ADD PHOTO", "SKIP", new CustomAlertInterface() {
            @Override
            public void onPositiveButtonClicked() {
                showUploadAlert();
            }

            @Override
            public void onNegativeButtonClicked() {
                updateUserProfileInfo(userId);
            }
        });
        customAlert.showDialog();
    }


    private void showUploadAlert() {
        UploadAlert uploadAlert = new UploadAlert(ProfileActivity.this, new UploadAlertListener() {
            @Override
            public void onChooseFileClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, ProfileActivity.this)) {
                    intentToImageSelection();
                }
            }

            @Override
            public void onCaptureCameraClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_CAMERA_PERMISSION, ProfileActivity.this)) {
                    intentToCameraApp();
                }
            }
        });
        uploadAlert.showAlertDialog();
    }


    private void intentToImageSelection() {
        imageFilePath = null;
        MyUtils.intentToImageSelection(ProfileActivity.this);
    }

    private void intentToCameraApp() {
        imageFilePath = null;
        imageFilePath = MyUtils.intentToCameraApp(ProfileActivity.this);
    }


    private void adaptSpinnerValues() {
        final ArrayList<String> raceList = new ArrayList<>();
        raceList.add("Malay");
        raceList.add("Chinese");
        raceList.add("Indian");
        raceList.add("Others");

        ArrayAdapter<String> opuSpinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, raceList);
        raceSpinner.setAdapter(opuSpinnerAdapter);

        raceSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    race = raceList.get(i);
                    Log.e("race", ":" + race);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (requestCode == Constants.PICK_IMAGE_FROM_FILES) {

                        imageFilePath = RealPathUtil.getPath(context, data.getData());
                        Log.e("selectedFile", "path:" + imageFilePath);

                        loadImageSource(imageFilePath);

                    } else if (requestCode == Constants.CAPTURE_IMAGE_FROM_CAMERA) {

                        if (MyUtils.checkStringValue(imageFilePath)) {
                            Log.e("capturedImagePath", "path:" + imageFilePath);
                            loadImageSource(imageFilePath);
                        }
                    }
                    break;
                case Constants.REGISTRATION_DECLINED:
                    setResult(Constants.REGISTRATION_DECLINED);
                    finish();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }


    private void loadImageSource(String imageFilePath) {
        Glide.with(context).load(imageFilePath).placeholder(R.drawable.user_pic).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(profileImageView);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToImageSelection();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            case Constants.WRITE_STORAGE_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentToCameraApp();

                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(ProfileActivity.this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }


    private void intentToAddContactPage() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.USER_ID, userId);
        Intent intent = new Intent(context, AddContactActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REGISTRATION);
        MyUtils.openOverrideAnimation(true, ProfileActivity.this);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        setResult(Constants.REGISTRATION_DECLINED);
        finish();
        MyUtils.openOverrideAnimation(false, ProfileActivity.this);
    }


}
