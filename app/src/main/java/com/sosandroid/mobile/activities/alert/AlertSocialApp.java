package com.sosandroid.mobile.activities.alert;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.AlertSocailAppAdapter;
import com.sosandroid.mobile.interfaces.OnItemClicked;
import com.sosandroid.mobile.models.ContactModel;
import com.sosandroid.mobile.models.Contacts;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PermissionAlert;
import com.sosandroid.mobile.utils.PermissionRequest;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertSocialApp extends AppCompatActivity {


    ArrayList<Contacts.Datum> savedContactsList = new ArrayList<>();
    ArrayList<ContactModel> contactModelArrayList = new ArrayList<>();
    private ProgressDialog progressDialog = null;
    private TextView txtTitle, errorTxt;
    private RecyclerView emergencyContactsRecycler;
    private Context context;
    private String isFrom = "";
    private String userId = null;
    private String countryCodeString = "";
    private String number = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_social_app);
        context = AlertSocialApp.this;

        isFrom = getIntent().getStringExtra(Constants.IS_FROM);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        emergencyContactsRecycler = findViewById(R.id.fct_emergencyContactsRecycler);
        errorTxt = findViewById(R.id.al_errorTxt);
        txtTitle = findViewById(R.id.fct_titleNameTxt);
        emergencyContactsRecycler.setHasFixedSize(true);
        emergencyContactsRecycler.setLayoutManager(new LinearLayoutManager(context));

        if (isFrom.equalsIgnoreCase(Constants.IS_FROM_TEST)) {
            txtTitle.setText("Test Contacts");
        } else {

            txtTitle.setText("Emergency Contacts");
        }

        getContactsList();
    }


    private void getContactsList() {
        try {

            String type;
            if (isFrom.equalsIgnoreCase(Constants.IS_FROM_TEST)) {
                type = "test_contacts";
            } else {
                type = "emergency_contacts";
            }
            progressDialog = MyUtils.showProgressLoader(context, "Loading..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<Contacts> call = apiService.getContactsList(type, userId);
            call.enqueue(new Callback<Contacts>() {
                @Override
                public void onResponse(Call<Contacts> call, Response<Contacts> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        Contacts contacts = response.body();
                        if (contacts != null) {
                            String status = contacts.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                updateUI(contacts);
                            } else if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.FAILURE)) {
                                showError();
                            }
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<Contacts> call, Throwable t) {
                    showError();
                    MyUtils.dismissProgressLoader(progressDialog);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            showError();
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void updateUI(Contacts contacts) {

        savedContactsList.clear();
        savedContactsList.addAll(contacts.getData());

        contactModelArrayList.clear();
        for (Contacts.Datum contactModel : savedContactsList) {
            contactModelArrayList.add(new ContactModel(contactModel.getContactName(), contactModel.getContactNumber(), false));
        }
        adaptContactListToRecycler();
    }


    private void adaptContactListToRecycler() {
        if (savedContactsList.size() > 0) {
            resetViews();
            AlertSocailAppAdapter contactsAdapter = new AlertSocailAppAdapter(context, savedContactsList, new OnItemClicked() {
                @Override
                public void onItemClicked(int position) {
                    countryCodeString = "";
                    String message = "\"SOS-EMERGENCY";
                    number = savedContactsList.get(position).getContactNumber();
                    String first_two = number.substring(0, 3);
                    if (first_two.equalsIgnoreCase("+60")) {
                        number = number.replace("+60", "");
                    }
                    countryCodeString = "+60";
                    if (isFrom.equalsIgnoreCase(Constants.IS_FROM_POLICE)) {
                        message = Constants.WHATSAPP_ALERT_POLICE;
                    } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_HOSPITAL)) {
                        message = Constants.WHATSAPP_ALERT_HOSPITAL;
                    } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_FIRE)) {
                        message = Constants.WHATSAPP_ALERT_FIRE;
                    } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_TEST)) {
                        message = Constants.WHATSAPP_ALERT_TEST;
                    }


                    String whatsappStatus = savedContactsList.get(position).getWhatsappNotification();
                    String telegramStatus = savedContactsList.get(position).getTelegramNotification();
                    String lineStatus = savedContactsList.get(position).getLineNotification();
                    String viberStatus = savedContactsList.get(position).getViberNotification();
                    String callStatus = savedContactsList.get(position).getCall();

                    if (whatsappStatus.equalsIgnoreCase("1")) {
                        MyUtils.openWhatsApp(context, countryCodeString + number, message);
                    } else if (telegramStatus.equalsIgnoreCase("1")) {
                        MyUtils.openTelegram(context, message);
                    } else if (lineStatus.equalsIgnoreCase("1")) {
                        MyUtils.openLine(context, countryCodeString + number, message);
                    } else if (viberStatus.equalsIgnoreCase("1")) {
                        MyUtils.openViber(context, countryCodeString + number, message);
                    } else if (callStatus.equalsIgnoreCase("1")) {
                        if (PermissionRequest.askForActivityPermission(Manifest.permission.CALL_PHONE, Constants.CALL_PHONE, AlertSocialApp.this)) {
                            MyUtils.openCallIntent(context, countryCodeString + number);
                        }
                    }
                }
            });
            emergencyContactsRecycler.setAdapter(contactsAdapter);
        } else {
            showError();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MyUtils.openCallIntent(context, countryCodeString + number);
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AlertSocialApp.this, Manifest.permission.CALL_PHONE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
        }
    }

    private void showError() {
        if (emergencyContactsRecycler.getVisibility() == View.VISIBLE) {
            emergencyContactsRecycler.setVisibility(View.GONE);
        }

        if (errorTxt.getVisibility() == View.GONE) {
            errorTxt.setVisibility(View.VISIBLE);
        }
    }

    private void resetViews() {
        if (emergencyContactsRecycler.getVisibility() == View.GONE) {
            emergencyContactsRecycler.setVisibility(View.VISIBLE);
        }

        if (errorTxt.getVisibility() == View.VISIBLE) {
            errorTxt.setVisibility(View.GONE);
        }
    }
}
