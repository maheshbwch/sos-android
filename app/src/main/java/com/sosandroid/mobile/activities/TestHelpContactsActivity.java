package com.sosandroid.mobile.activities;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.TestContactsAdapter;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;

public class TestHelpContactsActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_help_contacts);
        init();
    }

    private void init() {

        toolbar = findViewById(R.id.aap_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        viewPager = findViewById(R.id.ah_viewPager);
        listener();
    }

    private void listener() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePages();
            }
        });


        viewPager.setAdapter(new TestContactsAdapter(getSupportFragmentManager(), Constants.IS_FROM_TESTCONTACT));


    }

    @Override
    public void onBackPressed() {
        closePages();
    }

    private void closePages() {
        setResult(Constants.TEST_CONTACT_COUNT);
        finish();
        MyUtils.openOverrideAnimation(false, TestHelpContactsActivity.this);

    }
}
