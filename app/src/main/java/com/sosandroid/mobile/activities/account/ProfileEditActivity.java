package com.sosandroid.mobile.activities.account;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.interfaces.UploadAlertListener;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.models.Profile;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PermissionAlert;
import com.sosandroid.mobile.utils.PermissionRequest;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.utils.RealPathUtil;
import com.sosandroid.mobile.utils.UploadAlert;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileEditActivity extends AppCompatActivity {


    private Context context;
    private Toolbar toolbar;
    private TextView saveTxt;
    private RelativeLayout profileImageRelative;
    private ImageView profileImageView;
    private ProgressBar imageProgress;
    private TextInputLayout userNameTil;
    private TextInputEditText userNameEdt;
    private MaterialBetterSpinner raceSpinner;
    private ProgressDialog progressDialog = null;

    private String userId = null;
    private String race = null;
    private String imageFilePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        context = ProfileEditActivity.this;

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.ape_toolbar);
        saveTxt = findViewById(R.id.ape_saveTxt);
        profileImageRelative = findViewById(R.id.ape_profileImageRelative);
        profileImageView = findViewById(R.id.ape_profileImageView);
        imageProgress = findViewById(R.id.ape_imageProgress);
        userNameTil = findViewById(R.id.ape_userNameTil);
        userNameEdt = findViewById(R.id.ape_userNameEdt);
        raceSpinner = findViewById(R.id.ape_raceSpinner);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        userNameTil.setTypeface(MyUtils.getRegularFont(context));
        userNameEdt.setTypeface(MyUtils.getRegularFont(context));

        listeners();
    }

    private void listeners() {
        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        Log.e("userID", ":" + userId);

        String profileUrl = PreferenceHandler.getPreferenceFromString(context, Constants.PROFILE_IMG_URL);
        String userName = PreferenceHandler.getPreferenceFromString(context, Constants.USER_NAME);
        race = PreferenceHandler.getPreferenceFromString(context, Constants.RACE);

        if (MyUtils.checkStringValue(userName)) {
            userNameEdt.setText(userName);
        }

        if (MyUtils.checkStringValue(race)) {
            raceSpinner.setText(race);
        }

        if (MyUtils.checkStringValue(profileUrl)) {
            loadImageSource(profileUrl);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        profileImageRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUploadAlert();
            }
        });


        saveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = userNameEdt.getText().toString().trim();

                if (!MyUtils.checkStringValue(userName)) {
                    MyUtils.showLongToast(context, "User Name required");
                } else if (!MyUtils.checkStringValue(race)) {
                    MyUtils.showLongToast(context, "Race required");
                } else {
                    updateUserName(userName, race);
                }

            }
        });


        adaptSpinnerValues();
    }

//===UPDATE USER PROFILE PIC=====================================================================

    private void showUploadAlert() {
        UploadAlert uploadAlert = new UploadAlert(ProfileEditActivity.this, new UploadAlertListener() {
            @Override
            public void onChooseFileClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, ProfileEditActivity.this)) {
                    intentToImageSelection();
                }
            }

            @Override
            public void onCaptureCameraClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_CAMERA_PERMISSION, ProfileEditActivity.this)) {
                    intentToCameraApp();
                }
            }
        });
        uploadAlert.showAlertDialog();
    }


    private void intentToImageSelection() {
        imageFilePath = null;
        MyUtils.intentToImageSelection(ProfileEditActivity.this);
    }

    private void intentToCameraApp() {
        imageFilePath = null;
        imageFilePath = MyUtils.intentToCameraApp(ProfileEditActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (requestCode == Constants.PICK_IMAGE_FROM_FILES) {

                        imageFilePath = RealPathUtil.getPath(context, data.getData());
                        Log.e("selectedFile", "path:" + imageFilePath);

                        loadImageSource(imageFilePath);

                        updateUserProfileInfo();

                    } else if (requestCode == Constants.CAPTURE_IMAGE_FROM_CAMERA) {

                        if (MyUtils.checkStringValue(imageFilePath)) {
                            Log.e("capturedImagePath", "path:" + imageFilePath);
                            loadImageSource(imageFilePath);

                            updateUserProfileInfo();
                        }
                    }
                    break;
                case Constants.REGISTRATION_DECLINED:
                    setResult(Constants.REGISTRATION_DECLINED);
                    finish();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }


    private void loadImageSource(String imageFilePath) {
        Glide.with(context).load(imageFilePath).placeholder(R.drawable.user_pic).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(profileImageView);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToImageSelection();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileEditActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            case Constants.WRITE_STORAGE_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentToCameraApp();

                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileEditActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(ProfileEditActivity.this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }


    private void updateUserProfileInfo() {

        Log.e("update_profile", "imageFilePath:" + imageFilePath + "\n" + "userId:" + userId);

        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("type", RequestBody.create(MediaType.parse("text/plain"), "profile_image"));
            map.put("user_id", RequestBody.create(MediaType.parse("text/plain"), userId));

            MultipartBody.Part body = null;
            if (MyUtils.checkStringValue(imageFilePath)) {
                File file = new File(imageFilePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
                body = MultipartBody.Part.createFormData("profile_file", file.getName(), requestFile);
            } else {
                body = MultipartBody.Part.createFormData("profile_file", "");
            }

            Log.e("image_path", "file:" + imageFilePath);

            imageProgress.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<Profile> call = apiService.updateProfileProfile(map, body);
            call.enqueue(new Callback<Profile>() {
                @Override
                public void onResponse(Call<Profile> call, Response<Profile> response) {
                    imageProgress.setVisibility(View.GONE);
                    try {
                        Log.e("response", "code" + response.code());
                        Profile profile = response.body();
                        if (profile != null) {
                            String status = profile.getStatus();
                            String message = profile.getMessage();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                String imageUrl = profile.getProfileImage();
                                PreferenceHandler.storePreference(context, Constants.PROFILE_IMG_URL, imageUrl);

                                MyUtils.showLongToast(context, "Updated Profile Picture!!");

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", (MyUtils.checkStringValue(message)?message:"Unable to update Profile Image") );
                            }

                        } else {
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", "Unable to update Profile");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Profile> call, Throwable t) {
                    imageProgress.setVisibility(View.GONE);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", "Unable to update Profile");
                }
            });
        } catch (Exception e) {
            imageProgress.setVisibility(View.GONE);
            Log.e("exp", "code" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Profile", "Unable to update Profile");
        }
    }


//=====UPDATE USERNAME AND RACE==================================================================

    private void adaptSpinnerValues() {
        final ArrayList<String> raceList = new ArrayList<>();
        raceList.add("Malay");
        raceList.add("Chinese");
        raceList.add("Indian");
        raceList.add("Others");

        ArrayAdapter<String> opuSpinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, raceList);
        raceSpinner.setAdapter(opuSpinnerAdapter);

        raceSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    race = raceList.get(i);
                    Log.e("race", ":" + race);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void updateUserName(final String userName, final String race) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating UserInfo..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.updateUserName("user_name_and_race", userId, userName, race);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                PreferenceHandler.storePreference(context, Constants.USER_NAME, userName);
                                PreferenceHandler.storePreference(context, Constants.RACE, race);

                                showAlertToUser(Constants.DIALOG_SUCCESS, "Update UserName", MyUtils.checkStringValue(message) ? message : "User Info Updated Successfully");

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update UserName", MyUtils.checkStringValue(message) ? message : "Unable to Update User Info");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update UserName", "Unable to Update User Info");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Update UserName", "Unable to Update User Info");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update UserName", "Unable to Update User Info");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update UserName", "Unable to Update User Info");
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(ProfileEditActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, ProfileEditActivity.this);
    }

}
