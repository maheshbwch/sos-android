package com.sosandroid.mobile.activities.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextView saveTxt, forgotPassword;
    private TextInputLayout txtCurrentPassTil, txtNewPassTil, txtConfirmPass;
    private TextInputEditText edtCurrentPass, edtNewPass, edtConfirmPass;

    private ProgressDialog progressDialog = null;

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        context = ChangePasswordActivity.this;

        init();
    }


    private void init() {
        toolbar = findViewById(R.id.acp_toolbar);
        saveTxt = findViewById(R.id.acp_saveTxt);

        txtCurrentPassTil = findViewById(R.id.acp_currentTxtPass);
        txtNewPassTil = findViewById(R.id.acp_newTxtPass);
        txtConfirmPass = findViewById(R.id.acp_ConfirmTxtPass);

        edtCurrentPass = findViewById(R.id.acp_edtCurrentPass);
        edtNewPass = findViewById(R.id.acp_edtNewPass);
        edtConfirmPass = findViewById(R.id.acp_edtConfirmPass);

        forgotPassword = findViewById(R.id.acp_txtForgotPass);

        setSupportActionBar(toolbar);


        txtCurrentPassTil.setTypeface(MyUtils.getRegularFont(context));
        txtNewPassTil.setTypeface(MyUtils.getRegularFont(context));
        txtConfirmPass.setTypeface(MyUtils.getRegularFont(context));

        edtCurrentPass.setTypeface(MyUtils.getRegularFont(context));
        edtNewPass.setTypeface(MyUtils.getRegularFont(context));
        edtConfirmPass.setTypeface(MyUtils.getRegularFont(context));


        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        listeners();
    }

    private void listeners() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        saveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MyUtils.isInternetConnected(ChangePasswordActivity.this)) {
                    validEditText();
                } else {
                    MyUtils.showLongToast(context, String.valueOf(R.string.internet_not_working));
                }

            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ForgotPassword.class);
                intent.putExtra(Constants.PHONE_NUMBER,PreferenceHandler.getPreferenceFromString(context, Constants.PHONE_NUMBER));
                intent.putExtra(Constants.COUNTRY_CODE,PreferenceHandler.getPreferenceFromString(context, Constants.COUNTRY_CODE));
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, ChangePasswordActivity.this);

            }
        });
    }

    private void validEditText() {

        String currentPassword = edtCurrentPass.getText().toString();
        String newPassword = edtNewPass.getText().toString();
        String confirmPassword = edtConfirmPass.getText().toString();

        if (!MyUtils.checkStringValue(currentPassword)) {
            MyUtils.showLongToast(context, "Current Password required");
        } else if (!MyUtils.checkStringValue(newPassword)) {
            MyUtils.showLongToast(context, "New Password required");
        } else if (!MyUtils.checkStringValue(confirmPassword)) {
            MyUtils.showLongToast(context, "Confirm New Password required");
        } else {
            if (newPassword.equals(confirmPassword)) {
                changePassword(currentPassword, newPassword);
            } else {
                MyUtils.showLongToast(context, "New Password and Confirm New Password incorrect");
            }

        }

    }


    private void changePassword(String currentPass, String newPassword) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.changePassword("change_password", userId, currentPass, newPassword);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                showAlertToUser(Constants.DIALOG_SUCCESS, "Update Password", MyUtils.checkStringValue(message) ? message : "User Password Updated Successfully");
                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", MyUtils.checkStringValue(message) ? message : "Unable to Update User Password");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Update Password", "Unable to Update User Password");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(ChangePasswordActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, ChangePasswordActivity.this);
    }


}
