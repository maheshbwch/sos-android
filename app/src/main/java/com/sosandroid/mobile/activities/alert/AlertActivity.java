package com.sosandroid.mobile.activities.alert;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.CountDownInterface;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyCountDownTimer;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AlertActivity extends AppCompatActivity {

    private Context context;
    private LinearLayout linearLayout;
    private TextView descriptionTxt, countDownTxt, sentTxt, buttonTxt;

    private MyCountDownTimer myCountDownTimer = null;

    private String alertType;

    private static final String EMERGENCY_ALERT_TEXT = "After the Countdown reach zero,Your contacts and operator will receive your Help Alert";
    private static final String EMERGENCY_ALERT_SENT = "We have sent the Help Alert to your contacts and operator.You will be contacted shortly";

    private static final String TEST_ALERT_TEXT = "We will send this Test Help Alert to your selected contacts after the countdown reach zero";
    private static final String TEST_ALERT_SENT = "We have send the Test Help Alert to all your selected contacts";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_alert);
        context = AlertActivity.this;

        init();
    }


    private void init() {
        linearLayout = findViewById(R.id.aa_linearLayout);
        descriptionTxt = findViewById(R.id.aa_descriptionTxt);
        countDownTxt = findViewById(R.id.aa_countDownTxt);
        sentTxt = findViewById(R.id.aa_sentTxt);
        buttonTxt = findViewById(R.id.aa_buttonTxt);

        Bundle bundle = getIntent().getExtras();
        alertType = bundle.getString(Constants.TYPE);

        if (MyUtils.checkStringValue(alertType) && alertType.equalsIgnoreCase(Constants.TEST_ALERT)) {
            linearLayout.setBackground(getResources().getDrawable(R.drawable.gradient_test_alert));
        } else {
            linearLayout.setBackground(getResources().getDrawable(R.drawable.gradient_alert));
        }

        listeners();
    }

    private void listeners() {

        startCountDown();

        buttonTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void startCountDown() {

        enableCountDownTxt();
        myCountDownTimer = new MyCountDownTimer(Constants.ALERT_TIME_OUT * 1000, 1000, new CountDownInterface() {
            @Override
            public void onTicking(long millisUntilFinished) {
                String text = String.format(Locale.getDefault(), "%02d", TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);
                countDownTxt.setText("" + text);
            }

            @Override
            public void onFinished() {
                showSentStatus();
            }
        });
        myCountDownTimer.start();
    }


    private void showSentStatus() {

        if (countDownTxt.getVisibility() == View.VISIBLE) {
            countDownTxt.setVisibility(View.GONE);
        }

        if (sentTxt.getVisibility() == View.GONE) {
            sentTxt.setVisibility(View.VISIBLE);
        }

        if (MyUtils.checkStringValue(alertType) && alertType.equalsIgnoreCase(Constants.TEST_ALERT)) {
            descriptionTxt.setText(TEST_ALERT_SENT);
        } else {
            descriptionTxt.setText(EMERGENCY_ALERT_SENT);
        }

        buttonTxt.setText("CLOSE");
    }

    private void enableCountDownTxt() {
        if (MyUtils.checkStringValue(alertType) && alertType.equalsIgnoreCase(Constants.TEST_ALERT)) {
            descriptionTxt.setText(TEST_ALERT_TEXT);
        } else {
            descriptionTxt.setText(EMERGENCY_ALERT_TEXT);
        }
        buttonTxt.setText("CANCEL ALERT");

        if (countDownTxt.getVisibility() == View.GONE) {
            countDownTxt.setVisibility(View.VISIBLE);
        }

        if (sentTxt.getVisibility() == View.VISIBLE) {
            sentTxt.setVisibility(View.GONE);
        }
    }


    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myCountDownTimer != null) {
            myCountDownTimer.onFinish();
        }
    }

}
