package com.sosandroid.mobile.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.account.ForgotPassword;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.Login;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private TextView titleTxt, txtForgotPass;
    private TextInputLayout passwordTil;
    private TextInputEditText passwordEdt;
    private LinearLayout bottomLinear;
    private ProgressDialog progressDialog = null;

    private String isFrom;
    private String countryCodeId;
    private String countryCode;
    private String phoneNumber;
    private String userName;
    private String fcmToken = "";
    private String deviceId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        context = PasswordActivity.this;

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        PreferenceHandler.storePreference(context, Constants.FCM_TOKEN, refreshedToken);

        toolbar = findViewById(R.id.ap_toolbar);
        progressBar = findViewById(R.id.ap_progressBar);
        titleTxt = findViewById(R.id.ap_titleTxt);
        passwordTil = findViewById(R.id.ap_passwordTil);
        passwordEdt = findViewById(R.id.ap_passwordEdt);
        bottomLinear = findViewById(R.id.ap_bottomLinear);

        txtForgotPass = findViewById(R.id.ap_txtForgotPass);
        txtForgotPass.setVisibility(View.GONE);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        isFrom = bundle.getString(Constants.IS_FROM);
        countryCodeId = bundle.getString(Constants.COUNTRY_CODE_ID);
        countryCode = bundle.getString(Constants.COUNTRY_CODE);
        phoneNumber = bundle.getString(Constants.PHONE_NUMBER);
        userName = bundle.getString(Constants.USER_NAME);

        fcmToken = PreferenceHandler.getPreferenceFromString(context, Constants.FCM_TOKEN);
        deviceId = MyUtils.getDeviceId(context);
        Log.e("token_login", "token: " + fcmToken);
        Log.e("device_id", "id: " + deviceId);

        if (MyUtils.checkStringValue(isFrom) && isFrom.equalsIgnoreCase(Constants.IS_FROM_SIGN_UP)) {
            progressBar.setVisibility(View.VISIBLE);
            titleTxt.setText("Create Your Password");
            txtForgotPass.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            titleTxt.setText(MyUtils.checkStringValue(userName) ? ("Welcome Back " + userName + "!") : "Welcome Back");
            txtForgotPass.setVisibility(View.VISIBLE);

        }

        listeners();
    }


    private void listeners() {
        passwordTil.setTypeface(MyUtils.getRegularFont(context));
        passwordEdt.setTypeface(MyUtils.getRegularFont(context));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        passwordEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkBottomLinearStatus(s.length() > 5);
            }
        });


        bottomLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomLinear.isFocusable()) {

                    String password = passwordEdt.getText().toString().trim();
                    if (MyUtils.checkStringValue(password) && password.length() > 5) {

                        if (MyUtils.checkStringValue(isFrom) && isFrom.equalsIgnoreCase(Constants.IS_FROM_SIGN_UP)) {

                            if (Constants.DEBUG_MODE) {
                                intentToOtpVerifyPage("");
                            } else {
                                showAlertToUser(Constants.DIALOG_GENERAL, "Verify Phone Number " + countryCode + phoneNumber, "OTP message will be sent to given Phone Number", true);
                            }


                        } else {
                            loginUser(password);
                        }

                    } else {
                        MyUtils.showLongToast(context, "Password Must be 6 Characters or more");
                    }
                }
            }
        });


        txtForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, ForgotPassword.class);
                intent.putExtra(Constants.PHONE_NUMBER, phoneNumber);
                intent.putExtra(Constants.COUNTRY_CODE, countryCode);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, PasswordActivity.this);

            }
        });

    }


    private void checkBottomLinearStatus(boolean state) {
        if (state && !bottomLinear.isFocusable()) {
            //Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            bottomLinear.setAlpha(1);
            bottomLinear.setFocusable(true);
        } else if (!state && bottomLinear.isFocusable()) {
            //Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            bottomLinear.setAlpha((float) 0.3);
            bottomLinear.setFocusable(false);
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage, final boolean state) {
        CustomDialog customDialog = new CustomDialog(PasswordActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (state) {
                    sendVerificationCode(countryCode + phoneNumber);
                }
            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    private void sendVerificationCode(String mobile) {
        progressDialog = MyUtils.showProgressLoader(context, "Sending Code...");
        PhoneAuthProvider provider = PhoneAuthProvider.getInstance();
        provider.verifyPhoneNumber(mobile, Constants.OTP_TIME_OUT, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, mCallbacks);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.e("verification", "code:" + code);
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(PasswordActivity.this, "code:" + code, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The phone number that you have entered is invalid", false);
            } else if (e instanceof FirebaseTooManyRequestsException) {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + countryCode + phoneNumber + " is attempted too many OTP verification. Please try after some time", false);
            } else {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + countryCode + phoneNumber + " is verification failed. Please try again", false);
            }
            Log.e("verification", "exp:" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            Log.e("verification", "id:" + verificationId);
            MyUtils.dismissProgressLoader(progressDialog);
            intentToOtpVerifyPage(verificationId);

        }
    };


    private void intentToOtpVerifyPage(String verificationId) {
        String password = passwordEdt.getText().toString().trim();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.COUNTRY_CODE_ID, countryCodeId);
        bundle.putString(Constants.COUNTRY_CODE, countryCode);
        bundle.putString(Constants.PHONE_NUMBER, phoneNumber);
        bundle.putString(Constants.PASSWORD, password);
        bundle.putString(Constants.VERIFICATION_ID, verificationId);
        bundle.putString(Constants.IS_FROM, Constants.IS_FROM_PASSWORD);

        Intent intent = new Intent(context, OtpVerifyActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REGISTRATION);
        MyUtils.openOverrideAnimation(true, PasswordActivity.this);
    }


    //================LOGIN USER=========================================================================
    private void loginUser(String password) {
        Log.e("countryCodeId", ":" + countryCodeId);
        Log.e("countryCode", ":" + countryCode);
        Log.e("phoneNumber", ":" + phoneNumber);
        Log.e("password", ":" + password);

        try {
            progressDialog = MyUtils.showProgressLoader(context, "Signing in..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<Login> call = apiService.signUser("signin_user", countryCodeId, phoneNumber, password, deviceId, fcmToken);
            call.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        Login loginModel = response.body();

                        if (loginModel != null) {
                            String status = loginModel.getStatus();
                            String message = loginModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                String userId = loginModel.getData().getUserId();
                                String userName = loginModel.getData().getUserName();
                                String age = loginModel.getData().getAge();
                                String race = loginModel.getData().getRace();
                                String email = loginModel.getData().getEmail();
                                String profileFile = loginModel.getData().getProfileFile();
                                String mobile = loginModel.getData().getMobile();
                                String countryId = loginModel.getData().getCountryId();
                                String countryCode = loginModel.getData().getCountryCode();
                                String homeAddress = loginModel.getData().getHomeAddress();
                                String schoolAddress = loginModel.getData().getSchoolAddress();
                                String officeAddress = loginModel.getData().getOfficeAddress();

                                PreferenceHandler.storePreference(context, Constants.USER_ID, userId);
                                PreferenceHandler.storePreference(context, Constants.USER_NAME, userName);
                                PreferenceHandler.storePreference(context, Constants.AGE, age);
                                PreferenceHandler.storePreference(context, Constants.RACE, race);
                                PreferenceHandler.storePreference(context, Constants.EMAIL, email);
                                PreferenceHandler.storePreference(context, Constants.PROFILE_IMG_URL, profileFile);
                                PreferenceHandler.storePreference(context, Constants.PHONE_NUMBER, mobile);
                                PreferenceHandler.storePreference(context, Constants.COUNTRY_CODE_ID, countryId);
                                PreferenceHandler.storePreference(context, Constants.COUNTRY_CODE, countryCode);
                                PreferenceHandler.storePreference(context, Constants.HOME_ADDRESS, homeAddress);
                                PreferenceHandler.storePreference(context, Constants.SCHOOL_ADDRESS, schoolAddress);
                                PreferenceHandler.storePreference(context, Constants.OFFICE_ADDRESS, officeAddress);

                                PreferenceHandler.storePreference(context, Constants.USER_LOGIN, Constants.USER_LOGIN);

                                intentToHomePage();


                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Sign In User", MyUtils.checkStringValue(message) ? message : "Unable to Sign In User", false);
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Sign In User", "Unable to Sign In User", false);
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Sign In User", "Unable to Sign In User", false);
                    }
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Sign In User", "Unable to Sign In User", false);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Sign In User", "Unable to Sign In User", false);
        }
    }


    private void intentToHomePage() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivityForResult(intent, Constants.REGISTRATION);
        MyUtils.openOverrideAnimation(true, PasswordActivity.this);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, PasswordActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constants.REGISTRATION_DECLINED:
                setResult(Constants.REGISTRATION_DECLINED);
                finish();
                break;
            default:
                break;
        }
    }


}
