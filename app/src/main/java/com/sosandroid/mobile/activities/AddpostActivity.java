package com.sosandroid.mobile.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.interfaces.UploadAlertListener;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PermissionAlert;
import com.sosandroid.mobile.utils.PermissionRequest;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.utils.RealPathUtil;
import com.sosandroid.mobile.utils.UploadAlert;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddpostActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextInputEditText edtWriteSomthing;
    private MaterialBetterSpinner categoriesSpinner;
    private LinearLayout linearAddPhoto, linearAddPost, linearAdPhotoButton;
    private Context context;
    private ImageView imgPhoto;
    private ProgressDialog progressDialog = null;

    private String selectedCategory = null;
    private String imageFilePath = null;
    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addpost);

        init();

        listeners();
    }

    private void init() {

        context = AddpostActivity.this;
        toolbar = findViewById(R.id.aap_toolbar);
        edtWriteSomthing = findViewById(R.id.aap_writeSomethingEdt);
        categoriesSpinner = findViewById(R.id.aap_categorySpinner);
        linearAddPhoto = findViewById(R.id.aap_addPhoto);
        imgPhoto = findViewById(R.id.aap_imgPhoto);
        linearAddPost = findViewById(R.id.aap_bottomLinear);
        linearAdPhotoButton = findViewById(R.id.aap_linearAddPhotoButton);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        edtWriteSomthing.setTypeface(MyUtils.getRegularFont(context));

    }

    private void listeners() {
        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        adaptSpinnerValues();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });
        linearAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showUploadAlert();
            }
        });

        linearAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyUtils.isInternetConnected(context)) {
                    if (!MyUtils.checkStringValue(edtWriteSomthing.getText().toString())) {
                        MyUtils.showLongToast(context, "Please Write Something");
                    } else if (!MyUtils.checkStringValue(selectedCategory)) {
                        MyUtils.showLongToast(context, "Please Choose Category");
                    } else {
                        addPost();
                    }
                } else {
                    MyUtils.showLongToast(context, String.valueOf(R.string.internet_not_working));
                }

            }
        });

    }

    private void adaptSpinnerValues() {
        final ArrayList<String> categoryList = new ArrayList<>();
        categoryList.add("Advice");
        categoryList.add("Quote");

        ArrayAdapter<String> opuSpinnerAdapter = new ArrayAdapter<String>(AddpostActivity.this, android.R.layout.simple_list_item_1, categoryList);
        categoriesSpinner.setAdapter(opuSpinnerAdapter);

        categoriesSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    selectedCategory = categoryList.get(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void showUploadAlert() {
        UploadAlert uploadAlert = new UploadAlert(AddpostActivity.this, new UploadAlertListener() {
            @Override
            public void onChooseFileClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_FILES_PERMISSION, AddpostActivity.this)) {
                    intentToImageSelection();
                }
            }

            @Override
            public void onCaptureCameraClicked() {
                if (PermissionRequest.askForActivityPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Constants.WRITE_STORAGE_CAMERA_PERMISSION, AddpostActivity.this)) {
                    intentToCameraApp();
                }
            }
        });
        uploadAlert.showAlertDialog();
    }


    private void intentToImageSelection() {
        imageFilePath = null;
        MyUtils.intentToImageSelection(AddpostActivity.this);
    }

    private void intentToCameraApp() {
        imageFilePath = null;
        imageFilePath = MyUtils.intentToCameraApp(AddpostActivity.this);
    }


    private void addPost() {

        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("post_type", RequestBody.create(MediaType.parse("text/plain"), "add_post"));
            map.put("user_id", RequestBody.create(MediaType.parse("text/plain"), userId));
            map.put("post_category", RequestBody.create(MediaType.parse("text/plain"), selectedCategory));
            map.put("post_content", RequestBody.create(MediaType.parse("text/plain"), edtWriteSomthing.getText().toString()));

            MultipartBody.Part body;
            if (MyUtils.checkStringValue(imageFilePath)) {
                File file = new File(imageFilePath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
                body = MultipartBody.Part.createFormData("post_image", file.getName(), requestFile);
            } else {
                body = MultipartBody.Part.createFormData("post_image", "");
            }

            Log.e("image_path", "file:" + imageFilePath);

            progressDialog = MyUtils.showProgressLoader(context, "Posting..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.addPost(map, body);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    try {
                        Log.e("response", "code" + response.code());
                        BaseModel postResponse = response.body();
                        if (postResponse != null) {
                            String status = postResponse.getStatus();
                            String message = postResponse.getMessage();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                MyUtils.showLongToast(context, (MyUtils.checkStringValue(message) ? message : "Posted Successfully"));
                                setResult(Constants.ADD_POST);
                                finish();
                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Save Post", (MyUtils.checkStringValue(message) ? message : "Unable to Post"));
                            }

                        } else {
                            showAlertToUser(Constants.DIALOG_FAILURE, "Save Post", "Unable to Post");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Save Post", "Unable to Post");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", "code" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Save Post", "Unable to Post");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(AddpostActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AddpostActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (requestCode == Constants.PICK_IMAGE_FROM_FILES) {

                        imageFilePath = RealPathUtil.getPath(context, data.getData());
                        Log.e("selectedFile", "path:" + imageFilePath);

                        loadImageSource(imageFilePath);

                    } else if (requestCode == Constants.CAPTURE_IMAGE_FROM_CAMERA) {

                        if (MyUtils.checkStringValue(imageFilePath)) {
                            Log.e("capturedImagePath", "path:" + imageFilePath);
                            loadImageSource(imageFilePath);
                        }
                    }
                    break;
                case Constants.REGISTRATION_DECLINED:
                    setResult(Constants.REGISTRATION_DECLINED);
                    finish();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }


    private void loadImageSource(String imageFilePath) {
        Glide.with(context).load(imageFilePath).placeholder(R.drawable.user_pic).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                imgPhoto.setVisibility(View.GONE);
                linearAdPhotoButton.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                imgPhoto.setVisibility(View.VISIBLE);
                linearAdPhotoButton.setVisibility(View.GONE);
                return false;
            }
        }).into(imgPhoto);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_STORAGE_FILES_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToImageSelection();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddpostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            case Constants.WRITE_STORAGE_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    intentToCameraApp();

                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddpostActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(AddpostActivity.this, "STORAGE");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        closePage();
    }
}
