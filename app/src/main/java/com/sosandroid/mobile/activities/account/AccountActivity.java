package com.sosandroid.mobile.activities.account;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.SplashScreen;
import com.sosandroid.mobile.interfaces.CustomAlertInterface;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomAlert;
import com.sosandroid.mobile.utils.MyService;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PermissionAlert;
import com.sosandroid.mobile.utils.PermissionRequest;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class AccountActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ImageView whatsAppIconImage;
    private ImageView userImage;
    private TextView userNameText,versionNameTxt;
    private LinearLayout myAddressLinear, goldPlanLinear, accountSettingLinear, logOutLinear, faqLinear, activityLinear;
    private Switch aSwitch;

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        context = AccountActivity.this;

        init();
    }

    private void init() {
        toolbar = findViewById(R.id.aa_toolbar);
        whatsAppIconImage = findViewById(R.id.aa_whatsAppIconImage);
        userImage = findViewById(R.id.aa_profileImageView);
        userNameText = findViewById(R.id.aa_userNameTxt);
        myAddressLinear = findViewById(R.id.aa_myAddressLinear);
        goldPlanLinear = findViewById(R.id.aa_goldPlanLinear);
        accountSettingLinear = findViewById(R.id.aa_accountSettingLinear);
        logOutLinear = findViewById(R.id.aa_logOutLinear);
        faqLinear = findViewById(R.id.aa_faqLinear);
        activityLinear = findViewById(R.id.aa_allActivityLinear);
        aSwitch = findViewById(R.id.aa_locationSwitch);
        versionNameTxt = findViewById(R.id.aa_versionNameTxt);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        listeners();

    }

    private void listeners() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        whatsAppIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.openWhatsApp(context, Constants.WHATSAPP_NUMBER, Constants.WHATSAPP_MSG);
            }
        });

        myAddressLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddressEditActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountActivity.this);

            }
        });

        goldPlanLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PremiumBenefitsActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountActivity.this);

            }
        });

        accountSettingLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AccountSettingActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountActivity.this);

            }
        });

        logOutLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogOutAlert();
            }
        });

        faqLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, FaqActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountActivity.this);
            }
        });

        activityLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyActivityList.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AccountActivity.this);
            }
        });

        if (isMyServiceRunning(MyService.class)){
            aSwitch.setChecked(true);
        }else {
            aSwitch.setChecked(false);
        }

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    if (PermissionRequest.askForActivityPermission(Manifest.permission.ACCESS_FINE_LOCATION, Constants.WRITE_FINE_LOCATION, AccountActivity.this)) {
                        startService();
                    }
                }else {
                    stopService(new Intent(context, MyService.class));
                }
            }
        });


        versionNameTxt.setText("V"+MyUtils.getVersionCodeAndName());

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void showLogOutAlert() {
        CustomAlert customAlert = new CustomAlert(context, "You are sure want to logout?", "LOGOUT", "CANCEL", new CustomAlertInterface() {
            @Override
            public void onPositiveButtonClicked() {
                PreferenceHandler.clearPreferences(context);
                Intent intent = new Intent(context, SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });
        customAlert.showDialog();
    }


    @Override
    protected void onResume() {
        super.onResume();

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        String userName = PreferenceHandler.getPreferenceFromString(context, Constants.USER_NAME);
        String profileUrl = PreferenceHandler.getPreferenceFromString(context, Constants.PROFILE_IMG_URL);
        Log.e("profileUrl", ":" + profileUrl);
        Log.e("userName", ":" + userName);
        if (MyUtils.checkStringValue(userName)) {
            userNameText.setText(userName);
        }

        if (MyUtils.checkStringValue(profileUrl)) {
            loadImageSource(profileUrl);
        }

    }


    private void loadImageSource(String profileUrl) {
        Glide.with(context).load(profileUrl).placeholder(R.drawable.user_pic).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(userImage);
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AccountActivity.this);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.WRITE_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startService();
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AccountActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");
                        PermissionAlert permissionAlert = new PermissionAlert(context, "LOCATION");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void startService(){
        Intent serviceIntent = new Intent(context,MyService.class);
        serviceIntent.putExtra(Constants.USER_ID, userId);
        startService(serviceIntent);
    }


}
