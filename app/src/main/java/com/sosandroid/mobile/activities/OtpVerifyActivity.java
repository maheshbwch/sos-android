package com.sosandroid.mobile.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.account.ForgotPassWordFinal;
import com.sosandroid.mobile.activities.registration.ProfileActivity;
import com.sosandroid.mobile.interfaces.CountDownInterface;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.Register;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyCountDownTimer;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.utils.custom_view.otp_view.OnOtpCompletionListener;
import com.sosandroid.mobile.utils.custom_view.otp_view.OtpView;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerifyActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private OtpView otpView;
    private TextView resendCodeTxt, countDownTxt, txtBtnNext;
    private LinearLayout bottomLinear;
    private ProgressDialog progressDialog = null;
    private ProgressBar progressBar = null;

    private FirebaseAuth mAuth;
    private MyCountDownTimer myCountDownTimer = null;

    private String countryCodeId = "";
    private String countryCode;
    private String phoneNumber;
    private String password = "";
    private String otpVerificationId;
    private String isFrom;
    private String deviceId;
    private String fcmToken;

    private boolean isOtpResend = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);
        context = OtpVerifyActivity.this;

        toolbar = findViewById(R.id.aov_toolbar);
        otpView = findViewById(R.id.aov_otp_view);
        countDownTxt = findViewById(R.id.aov_countDownTxt);
        resendCodeTxt = findViewById(R.id.aov_resendCodeTxt);
        bottomLinear = findViewById(R.id.aov_bottomLinear);
        txtBtnNext = findViewById(R.id.aov_btnNext);
        progressBar = findViewById(R.id.aov_progress);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();

        isFrom = bundle.getString(Constants.IS_FROM);

        countryCodeId = bundle.getString(Constants.COUNTRY_CODE_ID);
        countryCode = bundle.getString(Constants.COUNTRY_CODE);
        if (isFrom.equals(Constants.IS_FROM_PASSWORD)) {
            password = bundle.getString(Constants.PASSWORD);
            progressBar.setVisibility(View.VISIBLE);
            txtBtnNext.setText("NEXT");
        } else {
            progressBar.setVisibility(View.GONE);
            txtBtnNext.setText("VERIFY");
        }
        phoneNumber = bundle.getString(Constants.PHONE_NUMBER);
        otpVerificationId = bundle.getString(Constants.VERIFICATION_ID);

        deviceId = MyUtils.getDeviceId(context);
        fcmToken = PreferenceHandler.getPreferenceFromString(context, Constants.FCM_TOKEN);
        if (!MyUtils.checkStringValue(fcmToken)) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            PreferenceHandler.storePreference(context, Constants.FCM_TOKEN, refreshedToken);
            fcmToken = PreferenceHandler.getPreferenceFromString(context, Constants.FCM_TOKEN);
        }

        listeners();
    }


    private void listeners() {
        mAuth = FirebaseAuth.getInstance();

        startCountDown();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        otpView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkBottomLinearStatus(s.length() == 6);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                MyUtils.hideKeyboard(OtpVerifyActivity.this);
            }
        });


        bottomLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomLinear.isFocusable()) {
                    String OTP = otpView.getText().toString();

                    Log.e("OTP", ":" + OTP);
                    if (OTP.length() == 6) {
                        if (Constants.DEBUG_MODE) {
                            intentToAddProfilePage("5");
                        } else {
                            verifyOTPCode(OTP);
                        }
                    } else {
                        MyUtils.showLongToast(context, "Enter the OTP Number");
                    }
                }
            }
        });
    }

    private void checkBottomLinearStatus(boolean state) {
        if (state && !bottomLinear.isFocusable()) {
            //Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            bottomLinear.setAlpha(1);
            bottomLinear.setFocusable(true);
        } else if (!state && bottomLinear.isFocusable()) {
            //Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            bottomLinear.setAlpha((float) 0.3);
            bottomLinear.setFocusable(false);
        }
    }


    private void startCountDown() {
        myCountDownTimer = new MyCountDownTimer(Constants.OTP_TIME_OUT * 1000, 1000, new CountDownInterface() {
            @Override
            public void onTicking(long millisUntilFinished) {
                String text = String.format(Locale.getDefault(), "%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60, TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);
                countDownTxt.setText("" + text);
            }

            @Override
            public void onFinished() {
                countDownTxt.setText("00:00");
                if (!isOtpResend) {
                    resendCodeTxt.setAlpha(1);
                } else {
                    resendCodeTxt.setAlpha((float) 0.3);
                }
                resendCodeTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isOtpResend) {
                            sendVerificationCode(countryCode + phoneNumber);
                        }
                    }
                });


            }
        });
        myCountDownTimer.start();
    }

    //===========verify OTP====================================================


    private void verifyOTPCode(String code) {
        progressDialog = MyUtils.showProgressLoader(context, "Verifying Code...");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(otpVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(OtpVerifyActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                MyUtils.dismissProgressLoader(progressDialog);
                if (task.isSuccessful()) {
                    if (isFrom.equals(Constants.IS_FROM_PASSWORD)) {
                        registerUser();
                    } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_PHONE_NUMBER_EDIT)) {
                        setResult(Constants.PHONE_NUMBER_EDIT);
                        finish();
                    } else if (isFrom.equalsIgnoreCase(Constants.IS_FROM_FORGOT)) {
                        Intent intent = new Intent(context, ForgotPassWordFinal.class);
                        startActivity(intent);
                        MyUtils.openOverrideAnimation(true, OtpVerifyActivity.this);
                        finish();
                    }
                    Log.e("verification", "code:verified");

                } else {

                    //Toast.makeText(OtpVerifyActivity.this, "code:" + task.getException(), Toast.LENGTH_LONG).show();
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        Log.e("verification", "failed:" + task.getException().getMessage());
                        showAlertToUser(Constants.DIALOG_FAILURE, "Invalid OTP", "The OTP that you have entered is invalid", false);
                    }
                }
            }
        });
    }

    //==================Resend OTP================================================


    private void sendVerificationCode(String mobile) {
        progressDialog = MyUtils.showProgressLoader(context, "Sending Code...");
        PhoneAuthProvider provider = PhoneAuthProvider.getInstance();
        provider.verifyPhoneNumber(mobile, Constants.OTP_TIME_OUT, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, mCallbacks);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.e("verification", "code:" + code);
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(context, "code:" + code, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            if (e instanceof FirebaseTooManyRequestsException) {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + countryCode + phoneNumber + " is attempted too many OTP verification. Please try after some time", false);
            } else {
                showAlertToUser(Constants.DIALOG_FAILURE, "Verification", "The entered phone number : " + countryCode + phoneNumber + " is verification failed. Please try again", false);
            }

            Log.e("verification", "exp:" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);
            Toast.makeText(context, "" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            otpVerificationId = verificationId;
            Log.e("verification", "id:" + verificationId);
            MyUtils.dismissProgressLoader(progressDialog);

            resendCodeTxt.setAlpha((float) 0.3);
            isOtpResend = true;

            startCountDown();
        }
    };


    private void registerUser() {
        Log.e("countryCodeId", ":" + countryCodeId);
        Log.e("countryCode", ":" + countryCode);
        Log.e("phoneNumber", ":" + phoneNumber);
        Log.e("password", ":" + password);

        try {
            progressDialog = MyUtils.showProgressLoader(context, "Registering User..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<Register> call = apiService.registerUser("register_user", countryCodeId, countryCode, phoneNumber, password, deviceId, fcmToken);
            call.enqueue(new Callback<Register>() {
                @Override
                public void onResponse(Call<Register> call, Response<Register> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        Register register = response.body();

                        if (register != null) {
                            String status = register.getStatus();
                            String message = register.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                String userId = register.getData().getUserId();

                                PreferenceHandler.storePreference(context, Constants.USER_ID, userId);
                                PreferenceHandler.storePreference(context, Constants.PHONE_NUMBER, phoneNumber);
                                PreferenceHandler.storePreference(context, Constants.COUNTRY_CODE_ID, countryCodeId);
                                PreferenceHandler.storePreference(context, Constants.COUNTRY_CODE, countryCode);


                                intentToAddProfilePage(userId);

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Register User", MyUtils.checkStringValue(message) ? message : "Unable to Register User", true);
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Register User", "Unable to Register User", true);
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Register User", "Unable to Register User", true);
                    }
                }

                @Override
                public void onFailure(Call<Register> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Register User", "Unable to Register User", true);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Register User", "Unable to Register User", true);
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage, final boolean isClose) {
        CustomDialog customDialog = new CustomDialog(OtpVerifyActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (isClose) {
                    closePage();
                }
            }

            @Override
            public void onDismissedClicked() {
                if (isClose) {
                    closePage();
                }
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    private void intentToAddProfilePage(String userId) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.USER_ID, userId);

        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REGISTRATION);
        MyUtils.openOverrideAnimation(true, OtpVerifyActivity.this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myCountDownTimer != null) {
            myCountDownTimer.onFinish();
        }
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        setResult(Constants.REGISTRATION_DECLINED);
        finish();
        MyUtils.openOverrideAnimation(false, OtpVerifyActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constants.REGISTRATION_DECLINED:
                setResult(Constants.REGISTRATION_DECLINED);
                finish();
                break;
            default:
                break;
        }
    }


}
