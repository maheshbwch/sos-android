package com.sosandroid.mobile.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.dialogs.CountryListDialog;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.interfaces.OnSelectedListener;
import com.sosandroid.mobile.models.CountryCodes;
import com.sosandroid.mobile.models.ValidateUser;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneNumberActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout phoneNumberTil;
    private TextInputEditText phoneNumberEdt;
    private LinearLayout countryCodeLinear, bottomLinear;
    private ImageView countryFlagImage;
    private TextView countryCodeTxt;
    private ProgressDialog progressDialog = null;

    private ArrayList<CountryCodes.Datum> countryCodesList = new ArrayList<>();
    private CountryListDialog countryListDialog = null;

    //private String isFrom;
    private String countryISDCode = null;
    private String countryCodeId = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number);
        context = PhoneNumberActivity.this;

        toolbar = findViewById(R.id.apn_toolbar);
        phoneNumberTil = findViewById(R.id.apn_phoneNumberTil);
        phoneNumberEdt = findViewById(R.id.apn_phoneNumberEdt);
        countryCodeLinear = findViewById(R.id.aph_countryCodeLinear);
        countryFlagImage = findViewById(R.id.aph_countryFlagImage);
        countryCodeTxt = findViewById(R.id.aph_countryCodeTxt);
        bottomLinear = findViewById(R.id.apn_bottomLinear);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        /*Bundle bundle = getIntent().getExtras();
        isFrom = bundle.getString(Constants.IS_FROM);*/

        listeners();
    }


    private void listeners() {
        phoneNumberTil.setTypeface(MyUtils.getRegularFont(context));
        phoneNumberEdt.setTypeface(MyUtils.getRegularFont(context));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        disablePhoneNumberEditField();

        bottomLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomLinear.isFocusable()) {
                    String phoneNumber = phoneNumberEdt.getText().toString().trim();

                    if (MyUtils.checkStringValue(countryISDCode)) {

                        String number = phoneNumber.replace(countryISDCode, "");

                        Log.e("number", ":" + number);

                        String countryCode = countryISDCode.trim();

                        if (MyUtils.checkStringValue(number) && number.length() > 0) {
                            validateIfUserAlreadyRegistered(countryCodeId, countryCode, number);
                        } else {
                            MyUtils.showLongToast(context, "Enter the Phone Number");
                        }
                    } else {
                        MyUtils.showLongToast(context, "Select the Country ISD Code");
                    }
                }
            }
        });

        getCountryCodeList();
    }

    private void getCountryCodeList() {
        try {
            Log.e("appBaseUrl22", ":" + HttpRequest.BASE_URL);
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<CountryCodes> call = apiService.getCountriesCodesList();
            call.enqueue(new Callback<CountryCodes>() {
                @Override
                public void onResponse(Call<CountryCodes> call, Response<CountryCodes> response) {
                    if (response.isSuccessful()) {

                        CountryCodes countryCodes = response.body();

                        if (countryCodes != null) {

                            updateCountryCodeToDialogUI(countryCodes);

                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<CountryCodes> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void updateCountryCodeToDialogUI(CountryCodes countryCodes) {
        countryCodesList.addAll(countryCodes.getData());
        Log.e("countryCodesList", "size:" + countryCodesList.size());
        if (countryCodesList.size() > 0) {
            countryCodeLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (countryListDialog == null) {
                        countryListDialog = new CountryListDialog(countryCodesList, new OnSelectedListener() {
                            @Override
                            public void onSelected(CountryCodes.Datum countryCodes) {

                                String flagUrl = countryCodes.getUrlPath();
                                String countryName = countryCodes.getCountriesName();
                                String countryCode = countryCodes.getCountriesIsdCode();
                                countryCodeId = countryCodes.getCountriesId();

                                countryISDCode = "+" + countryCode;

                                countryCodeTxt.setText("" + countryName + "" + "(" + countryISDCode + ")");

                                countryISDCode = countryISDCode + " ";

                                setUpCountryCodeInToEditText();

                                loadImageSource(flagUrl);
                            }

                            @Override
                            public void onNothingSelected() {

                            }
                        });
                        countryListDialog.setTitle("Select Country Name");
                    }
                    countryListDialog.show(getSupportFragmentManager(), "dialog");
                }
            });
        }
    }

    private void setUpCountryCodeInToEditText() {
        phoneNumberTil.setAlpha(1);
        phoneNumberEdt.setFocusable(true);
        phoneNumberEdt.setFocusableInTouchMode(true);
        phoneNumberEdt.requestFocus();
        phoneNumberEdt.setText("");

        phoneNumberEdt.setText(countryISDCode);
        phoneNumberEdt.setCursorVisible(true);
        phoneNumberEdt.setSelection(countryISDCode.length());

        phoneNumberEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.e("textChange", "after:" + s.toString());
                if (s.length() == countryISDCode.length() - 1) {
                    phoneNumberEdt.setText(countryISDCode);
                    phoneNumberEdt.setSelection(countryISDCode.length());
                } else {
                    checkBottomLinearStatus(s.length() != countryISDCode.length());
                }
            }
        });
    }


    private void checkBottomLinearStatus(boolean state) {
        if (state && !bottomLinear.isFocusable()) {
            Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            bottomLinear.setAlpha(1);
            bottomLinear.setFocusable(true);
        } else if (!state && bottomLinear.isFocusable()) {
            Log.e("state", ":" + bottomLinear.isFocusableInTouchMode());
            bottomLinear.setAlpha((float) 0.3);
            bottomLinear.setFocusable(false);
        }
    }


    private void disablePhoneNumberEditField() {
        phoneNumberTil.setAlpha((float) 0.3);
        phoneNumberEdt.setFocusable(false);
        phoneNumberEdt.setFocusableInTouchMode(false);

        /*phoneNumberEdt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                MyUtils.showLongToast(context, "Select the Country ISD Code");
                return false;
            }
        });*/
    }


    private void loadImageSource(String imageUrl) {
        if (countryFlagImage.getVisibility() == View.GONE) {
            countryFlagImage.setVisibility(View.VISIBLE);
        }
        Glide.with(context).load(imageUrl).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                if (countryFlagImage.getVisibility() == View.VISIBLE) {
                    countryFlagImage.setVisibility(View.GONE);
                }
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(countryFlagImage);
    }


    private void validateIfUserAlreadyRegistered(final String countryCodeId, final String countryCode, final String phoneNumber) {
        try {
            Log.e("countryCodeId", "id:" + countryCodeId);
            progressDialog = MyUtils.showProgressLoader(context, "Validating Phone Number..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ValidateUser> call = apiService.validateIfUserAlreadyRegistered("existing_user_check", countryCodeId, phoneNumber);
            call.enqueue(new Callback<ValidateUser>() {
                @Override
                public void onResponse(Call<ValidateUser> call, Response<ValidateUser> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        ValidateUser validateUser = response.body();

                        if (validateUser != null) {
                            String status = validateUser.getStatus();

                            if (MyUtils.checkStringValue(status)) {
                                if (status.equalsIgnoreCase(Constants.SUCCESS)) {
                                    intentToPasswordPage(countryCodeId, countryCode, phoneNumber, "", Constants.IS_FROM_SIGN_UP);
                                } else if (status.equalsIgnoreCase(Constants.FAILURE)) {
                                    String userName = "" + validateUser.getData().getUserName();
                                    String userId = validateUser.getData().getUserId();
                                    PreferenceHandler.storePreference(context, Constants.USER_ID, userId);
                                    intentToPasswordPage(countryCodeId, countryCode, phoneNumber, userName, Constants.IS_FROM_LOGIN);
                                }
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
                    }
                }

                @Override
                public void onFailure(Call<ValidateUser> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Validate Phone Number", "Unable to Validate Your Number");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(PhoneNumberActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                //closePage();
            }

            @Override
            public void onDismissedClicked() {
                //closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    private void intentToPasswordPage(String countryCodeId, String countryCode, String phoneNumber, String userName, String isFrom) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.IS_FROM, isFrom);
        bundle.putString(Constants.COUNTRY_CODE_ID, countryCodeId);
        bundle.putString(Constants.COUNTRY_CODE, countryCode);
        bundle.putString(Constants.PHONE_NUMBER, phoneNumber);
        bundle.putString(Constants.USER_NAME, userName);

        Intent intent = new Intent(context, PasswordActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.REGISTRATION);
        MyUtils.openOverrideAnimation(true, PhoneNumberActivity.this);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, PhoneNumberActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constants.REGISTRATION_DECLINED:
                setResult(Constants.REGISTRATION_DECLINED);
                finish();
                break;
            default:
                break;
        }
    }
}
