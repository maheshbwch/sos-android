package com.sosandroid.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.account.AccountActivity;
import com.sosandroid.mobile.activities.alert.AlertMainActivity;
import com.sosandroid.mobile.adapters.HomePagerAdapter;
import com.sosandroid.mobile.interfaces.CustomAlertInterface;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomAlert;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;

public class HomeActivity extends AppCompatActivity {


    private Context context;
    private TextView accountTxt, titleTxt;
    private ImageView whatsAppIconImage;
    private ViewPager viewPager;
    private BottomNavigationView bottomNavigation;
    private LinearLayout alertLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = HomeActivity.this;

        init();
    }

    private void init() {
        accountTxt = findViewById(R.id.ah_accountTxt);
        titleTxt = findViewById(R.id.ah_titleTxt);
        whatsAppIconImage = findViewById(R.id.ah_whatsAppIconImage);
        viewPager = findViewById(R.id.ah_viewPager);
        bottomNavigation = findViewById(R.id.ah_bottomNavigation);
        alertLinear = findViewById(R.id.ah_alertLinear);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        PreferenceHandler.storePreference(context, Constants.FCM_TOKEN, refreshedToken);
        Log.e("fcm_token", "token: " + refreshedToken);

        listeners();
    }

    private void listeners() {

        viewPager.setAdapter(new HomePagerAdapter(getSupportFragmentManager(), Constants.IS_FROM_EMERGENCY_CONTACT));

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("selected", "page:" + position);
                setUpToolBarTitle(position);
                setNavigationSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.ahm_places_item:
                        setUpToolBarTitle(0);
                        setNavigationSelected(0);
                        showPagerFragment(0);
                        break;
                    case R.id.ahm_contacts_item:
                        setUpToolBarTitle(1);
                        setNavigationSelected(1);
                        showPagerFragment(1);
                        break;
                    case R.id.ahm_testing_item:
                        setUpToolBarTitle(2);
                        setNavigationSelected(2);
                        showPagerFragment(2);
                        break;
                    case R.id.ahm_send_item:
                        setUpToolBarTitle(3);
                        setNavigationSelected(3);
                        showPagerFragment(3);
                        break;
                    default:
                        break;
                }
                Log.e("menu", "item:" + menuItem.getItemId());
                return false;
            }
        });

        accountTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AccountActivity.class);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, HomeActivity.this);
            }
        });

        whatsAppIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.openWhatsApp(context, Constants.WHATSAPP_NUMBER, Constants.WHATSAPP_MSG);
            }
        });


        alertLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AlertMainActivity.class);
                startActivity(intent);
            }
        });

    }


    private void setNavigationSelected(int position) {
        bottomNavigation.getMenu().getItem(position).setChecked(true);
    }

    private void showPagerFragment(int position) {
        viewPager.setCurrentItem(position);
    }

    private void setUpToolBarTitle(int position) {
        switch (position) {
            case 0:
                titleTxt.setText("Places");
                break;
            case 1:
                titleTxt.setText("Contacts");
                break;
            case 2:
                titleTxt.setText("Testing");
                break;
            case 3:
                titleTxt.setText("My Post");
                break;
            default:
                break;

        }
    }

    private void showExitAlert() {
        CustomAlert customAlert = new CustomAlert(context, "Are you sure want to Exit App?", "EXIT", "CANCEL", new CustomAlertInterface() {
            @Override
            public void onPositiveButtonClicked() {
                closePage();
            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });
        customAlert.showDialog();
    }


    @Override
    public void onBackPressed() {
        showExitAlert();
    }

    private void closePage() {
        setResult(Constants.REGISTRATION_DECLINED);
        finish();
        MyUtils.openOverrideAnimation(false, HomeActivity.this);
    }


}
