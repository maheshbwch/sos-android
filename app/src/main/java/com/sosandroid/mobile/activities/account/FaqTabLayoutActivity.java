package com.sosandroid.mobile.activities.account;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.CategoryPagerAdapter;
import com.sosandroid.mobile.fragments.FaqFragment;
import com.sosandroid.mobile.models.FagTitleTypeModel;
import com.sosandroid.mobile.models.FaqModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaqTabLayoutActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ProgressDialog progressDialog = null;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ArrayList<FagTitleTypeModel> titleArrayList = new ArrayList<>();

    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_tab_layout);
        context = FaqTabLayoutActivity.this;
        toolbar = findViewById(R.id.fat_toolbar);

        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        listeners();
        getFaq();

    }

    private void listeners() {
        Bundle bundle = getIntent().getExtras();
        titleArrayList = (ArrayList<FagTitleTypeModel>) bundle.getSerializable("title_arraylist");
        type = bundle.getString("type");
    }

    private void getFaq() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<FaqModel> call = apiService.getFaq("faq");
            call.enqueue(new Callback<FaqModel>() {
                @Override
                public void onResponse(Call<FaqModel> call, Response<FaqModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        FaqModel faqResponse = response.body();
                        if (faqResponse != null) {
                            String status = faqResponse.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                updateUI(faqResponse);
                            }
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<FaqModel> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void updateUI(FaqModel faqModel) {
        if (titleArrayList.size() > 0) {
            if (titleArrayList.size() <= 3) {
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
            setupViewPager(viewPager, titleArrayList, faqModel);
            tabLayout.setupWithViewPager(viewPager);

            setUpTabSelected(type);
        }


    }


    private void setUpTabSelected(String type) {
        switch (type) {
            case Constants.COMMON_FAQ:
                viewPager.setCurrentItem(0);
                break;
            case Constants.LOCATION_FAQ:
                viewPager.setCurrentItem(1);
                break;
            case Constants.GENERAL_FAQ:
                viewPager.setCurrentItem(2);
                break;
            case Constants.FEATURES_FAQ:
                viewPager.setCurrentItem(3);
                break;
            case Constants.PRIVACY_POLICY:
                viewPager.setCurrentItem(4);
                break;
        }

    }


    private void setupViewPager(ViewPager viewPager, ArrayList<FagTitleTypeModel> mainCategoriesList, FaqModel faqModels) {
        try {
            CategoryPagerAdapter adapter = new CategoryPagerAdapter(getSupportFragmentManager());
            for (int i = 0; i < mainCategoriesList.size(); i++) {
                String subCatName = mainCategoriesList.get(i).getTitle();
                String tabType = mainCategoriesList.get(i).getType();
                adapter.addFragment(new FaqFragment(faqModels, tabType), subCatName);
            }
            viewPager.setAdapter(adapter);
        } catch (Exception e) {
            Log.e("exception", ":" + e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, FaqTabLayoutActivity.this);
    }
}
