package com.sosandroid.mobile.activities.contacts;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.dialogs.CountryListDialog;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.interfaces.OnSelectedListener;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.models.ContactModel;
import com.sosandroid.mobile.models.CountryCodes;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddContactManually extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout txtName, txtPhone;
    private TextInputEditText edtName, edtPhone;
    private TextView txtSave, txtCountryName;
    private ImageView countryFlagImage;

    private ProgressDialog progressDialog = null;
    private CountryListDialog countryListDialog = null;

    ArrayList<ContactModel> contactModelArrayList = new ArrayList<>();
    private ArrayList<CountryCodes.Datum> countryCodesList = new ArrayList<>();
    private LinearLayout linearSelectCountry;

    String userId = null;
    String isFrom = null;

    private String countryISDCode = null;
    private String countryCodeId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact_manually);
        context = AddContactManually.this;

        init();


    }

    private void init() {

        Intent intent = getIntent();
        isFrom = intent.getStringExtra(Constants.IS_FROM);


        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        txtName = findViewById(R.id.aam_userNameTil);
        txtPhone = findViewById(R.id.aam_phoneNumberTil);
        toolbar = findViewById(R.id.aam_toolbar);
        txtName.setTypeface(MyUtils.getRegularFont(context));
        txtPhone.setTypeface(MyUtils.getRegularFont(context));

        edtName = findViewById(R.id.aam_userNameEdt);
        edtPhone = findViewById(R.id.aam_phoneNumberEdt);

        txtSave = findViewById(R.id.aam_checkTxt);
        txtCountryName = findViewById(R.id.aam_countryCodeTxt);

        linearSelectCountry = findViewById(R.id.aam_countryCodeLinear);
        countryFlagImage = findViewById(R.id.aam_countryFlagImage);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        disablePhoneNumberEditField();

        txtSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (MyUtils.isInternetConnected(context)) {
                    String name = edtName.getText().toString();
                    String phoneNumber = edtPhone.getText().toString();

                    if (!MyUtils.checkStringValue(name)) {
                        MyUtils.showLongToast(context, "Name is Required");
                    } else if (!MyUtils.checkStringValue(phoneNumber)) {
                        MyUtils.showLongToast(context, "Phone Number is Required");
                    }else if(phoneNumber.length()<4){
                        MyUtils.showLongToast(context, "Phone Number not valid");
                    }
                    else {
                        contactModelArrayList.add(new ContactModel(name, phoneNumber, false));
                        Gson gson = new GsonBuilder().create();
                        JsonArray myCustomArray = gson.toJsonTree(contactModelArrayList).getAsJsonArray();
                        Log.e("data", "data:" + myCustomArray.toString());

                        saveContacts(myCustomArray.toString());
                    }


                }

            }
        });


        getCountryCodeList();
    }



    private void getCountryCodeList() {
        try {
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<CountryCodes> call = apiService.getCountriesCodesList();
            call.enqueue(new Callback<CountryCodes>() {
                @Override
                public void onResponse(Call<CountryCodes> call, Response<CountryCodes> response) {
                    if (response.isSuccessful()) {

                        CountryCodes countryCodes = response.body();

                        if (countryCodes != null) {

                            updateCountryCodeToDialogUI(countryCodes);

                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<CountryCodes> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void updateCountryCodeToDialogUI(CountryCodes countryCodes) {
        countryCodesList.addAll(countryCodes.getData());
        Log.e("countryCodesList", "size:" + countryCodesList.size());
        if (countryCodesList.size() > 0) {
            linearSelectCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (countryListDialog == null) {
                        countryListDialog = new CountryListDialog(countryCodesList, new OnSelectedListener() {
                            @Override
                            public void onSelected(CountryCodes.Datum countryCodes) {

                                String flagUrl = countryCodes.getUrlPath();
                                String countryName = countryCodes.getCountriesName();
                                String countryCode = countryCodes.getCountriesIsdCode();
                                countryCodeId = countryCodes.getCountriesId();

                                countryISDCode = "+" + countryCode;

                                txtCountryName.setText("" + countryName + "" + "(" + countryISDCode + ")");

                                countryISDCode = countryISDCode + "";

                                setUpCountryCodeInToEditText();

                                loadImageSource(flagUrl);
                            }

                            @Override
                            public void onNothingSelected() {

                            }
                        });
                        countryListDialog.setTitle("Select Country Name");
                    }
                    countryListDialog.show(getSupportFragmentManager(), "dialog");
                }
            });
        }
    }

    private void loadImageSource(String imageUrl) {
        if (countryFlagImage.getVisibility() == View.GONE) {
            countryFlagImage.setVisibility(View.VISIBLE);
        }
        Glide.with(context).load(imageUrl).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                if (countryFlagImage.getVisibility() == View.VISIBLE) {
                    countryFlagImage.setVisibility(View.GONE);
                }
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(countryFlagImage);
    }

    private void setUpCountryCodeInToEditText() {
        txtPhone.setAlpha(1);
        edtPhone.setFocusable(true);
        edtPhone.setFocusableInTouchMode(true);
        edtPhone.requestFocus();
        edtPhone.setText("");

        edtPhone.setText(countryISDCode);
        edtPhone.setCursorVisible(true);
        edtPhone.setSelection(countryISDCode.length());

        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.e("textChange", "after:" + s.toString());
                if (s.length() == countryISDCode.length() - 1) {
                    edtPhone.setText(countryISDCode);
                    edtPhone.setSelection(countryISDCode.length());
                } else {
                    //checkBottomLinearStatus(s.length() != countryISDCode.length());
                }
            }
        });
    }


    private void saveContacts(String contactsJson) {
        try {

            String type;
            if (isFrom.equals(Constants.IS_FROM_EMERGENCY_CONTACT)) {
                type = "emergency_contacts";
            } else {
                type = "test_contacts";
            }

            progressDialog = MyUtils.showProgressLoader(context, "Adding Contacts..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.saveContacts(type, userId, contactsJson);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {

                                //showAlertToUser(Constants.DIALOG_SUCCESS, "Add Contacts", MyUtils.checkStringValue(message) ? message : "Contacts Added Successfully");
                                MyUtils.showLongToast(context, "Contact Added Successfully");
                                setResult(Constants.ADD_CONTACT_MANUALLY);
                                finish();
                                MyUtils.openOverrideAnimation(false, AddContactManually.this);

                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", MyUtils.checkStringValue(message) ? message : "Unable to Add Contacts");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Add Contacts", "Unable to Add Contacts");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(AddContactManually.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AddContactManually.this);
    }

    private void disablePhoneNumberEditField() {
        txtPhone.setAlpha((float) 0.3);
        edtPhone.setFocusable(false);
        edtPhone.setFocusableInTouchMode(false);

        edtPhone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                MyUtils.showLongToast(context, "Select the Country ISD Code");
                return false;
            }
        });
    }
}
