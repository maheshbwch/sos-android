package com.sosandroid.mobile.activities.account;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.OtpVerifyActivity;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class ForgotPasswordDetail extends AppCompatActivity {


    private ProgressDialog progressDialog = null;
    private TextView txtPhone, txtReset;

    private String phoneNumber, countryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_detail);
        Intent intent = getIntent();

        txtPhone = findViewById(R.id.afp_txtPhone);
        txtReset = findViewById(R.id.afp_txtReset);

        phoneNumber = intent.getStringExtra(Constants.PHONE_NUMBER);
        countryCode = intent.getStringExtra(Constants.COUNTRY_CODE);

        if (MyUtils.checkStringValue(phoneNumber)) {
            txtPhone.setText(countryCode + phoneNumber);
        }

        txtReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAlertToUser(Constants.DIALOG_GENERAL, "Verify Phone Number " + countryCode + phoneNumber, "OTP message will be sent to given Phone Number", true);

            }
        });

    }


    private void showAlertToUser(String statusCode, String title, String statusMessage, final boolean state) {
        CustomDialog customDialog = new CustomDialog(ForgotPasswordDetail.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (state) {
                    sendVerificationCode(countryCode + phoneNumber);
                }
            }

            @Override
            public void onDismissedClicked() {

            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


    private void sendVerificationCode(String mobile) {
        progressDialog = MyUtils.showProgressLoader(ForgotPasswordDetail.this, "Sending Code...");
        PhoneAuthProvider provider = PhoneAuthProvider.getInstance();
        provider.verifyPhoneNumber(mobile, Constants.OTP_TIME_OUT, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, mCallbacks);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            Log.e("verification", "code:" + code);
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(ForgotPasswordDetail.this, "code:" + code, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Log.e("verification", "exp:" + e.getMessage());
            MyUtils.dismissProgressLoader(progressDialog);

            Toast.makeText(ForgotPasswordDetail.this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(verificationId, forceResendingToken);
            Log.e("verification", "id:" + verificationId);
            MyUtils.dismissProgressLoader(progressDialog);
            intentToOtpVerifyPage(verificationId);

        }
    };


    private void intentToOtpVerifyPage(String verificationId) {

        Bundle bundle = new Bundle();
        bundle.putString(Constants.COUNTRY_CODE, countryCode);
        bundle.putString(Constants.PHONE_NUMBER, phoneNumber);
        bundle.putString(Constants.VERIFICATION_ID, verificationId);
        bundle.putString(Constants.IS_FROM, Constants.IS_FROM_FORGOT);

        Intent intent = new Intent(ForgotPasswordDetail.this, OtpVerifyActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.FORGOT_PASSWORD);
        MyUtils.openOverrideAnimation(true, ForgotPasswordDetail.this);
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, ForgotPasswordDetail.this);
    }
}
