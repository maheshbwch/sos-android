package com.sosandroid.mobile.activities.alert;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.DialogInterface;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomDialog;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertMainActivity extends AppCompatActivity {

    private Context context;
    private LinearLayout alertPeopleNearbyLinear, alertPoliceLinear, alertHospitalLinear, alertFireStationLinear, closeLinear;
    private ProgressDialog progressDialog = null;

    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_alert_main);
        context = AlertMainActivity.this;

        init();
    }


    private void init() {

        closeLinear = findViewById(R.id.aamn_closeLinear);
        alertPeopleNearbyLinear = findViewById(R.id.aamn_alertPeopleNearbyLinear);
        alertPoliceLinear = findViewById(R.id.aamn_alertPoliceLinear);
        alertHospitalLinear = findViewById(R.id.aamn_alertHospitalLinear);
        alertFireStationLinear = findViewById(R.id.aamn_alertFireStationLinear);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        listeners();
    }

    private void listeners() {
        closeLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alertPeopleNearbyLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToAlertPage();
            }
        });

        alertPoliceLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* intentToAlertPage();*/
                Intent intent = new Intent(context, AlertSocialApp.class);
                intent.putExtra(Constants.IS_FROM, Constants.IS_FROM_POLICE);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AlertMainActivity.this);

            }
        });


        alertHospitalLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //intentToAlertPage();
                Intent intent = new Intent(context, AlertSocialApp.class);
                intent.putExtra(Constants.IS_FROM, Constants.IS_FROM_HOSPITAL);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AlertMainActivity.this);
            }
        });

        alertFireStationLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //intentToAlertPage();
                Intent intent = new Intent(context, AlertSocialApp.class);
                intent.putExtra(Constants.IS_FROM, Constants.IS_FROM_FIRE);
                startActivity(intent);
                MyUtils.openOverrideAnimation(true, AlertMainActivity.this);
            }
        });
    }


    private void intentToAlertPage() {

        if (MyUtils.isInternetConnected(context)) {
            alertNearBy();
        } else {
            MyUtils.showLongToast(context, String.valueOf(R.string.internet_not_working));
        }
    }

    private void alertNearBy() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.alertNearBy("near_by_alert", userId);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            String message = baseModel.getMessage();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                showAlertToUser(Constants.DIALOG_SUCCESS, "Alert Nearby", MyUtils.checkStringValue(message) ? message : "Alert Notify Successfully");
                            } else {
                                showAlertToUser(Constants.DIALOG_FAILURE, "Alert Nearby", MyUtils.checkStringValue(message) ? message : "Unable to notify alert");
                            }
                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Alert Nearby", "Unable to notify alert");
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Alert Nearby", "Unable to notify alert");
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Alert Nearby", "Unable to notify alert");
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Alert Nearby", "Unable to notify alert");
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(AlertMainActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }

    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AlertMainActivity.this);
    }

}
