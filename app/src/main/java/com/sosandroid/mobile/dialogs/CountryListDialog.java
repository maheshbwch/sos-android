package com.sosandroid.mobile.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.SearchView;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.CountryCodesAdapter;
import com.sosandroid.mobile.interfaces.OnSelectedListener;
import com.sosandroid.mobile.models.CountryCodes;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.ArrayList;

public class CountryListDialog extends DialogFragment {

    private ArrayList<CountryCodes.Datum> arrayList = new ArrayList<>();
    private OnSelectedListener onSelectedListener = null;
    private CountryCodesAdapter adapter = null;
    private String title = null;


    public CountryListDialog(ArrayList<CountryCodes.Datum> arrayList, OnSelectedListener onSelectedListener) {
        this.arrayList = arrayList;
        this.onSelectedListener = onSelectedListener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View rootView = inflater.inflate(R.layout.dialog_country_codes, null);

        setUpView(rootView);


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(rootView);

        alertDialog.setPositiveButton("CLOSE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog.setTitle(MyUtils.checkStringValue(title) ? title : "Select Item");

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    private void setUpView(View rootView) {
        SearchView searchView = (SearchView) rootView.findViewById(R.id.dcc_searchView);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.dcc_listItemsRecycler);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (adapter != null) {
                    adapter.getFilter().filter(s);
                }

                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return false;
            }
        });
        searchView.setFocusable(false);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        adapter = new CountryCodesAdapter(getActivity(),arrayList, new OnSelectedListener() {
            @Override
            public void onSelected(CountryCodes.Datum countryCodes) {

                onSelectedListener.onSelected(countryCodes);
                dismiss();

                //int originalPosition = arrayList.indexOf(countryCodes);
                //Log.e("dialog", "originalPosition:" + originalPosition);
                //updateStatusInSpinner(originalPosition, true);
            }

            @Override
            public void onNothingSelected() {
                //NO ACTION
            }
        });
        recyclerView.setAdapter(adapter);
    }

    /*public void updateStatusInSpinner(int position, boolean state) {
        if (adapter != null && arrayList.size() > 0) {
            Log.e("pos", "pos:" + position);
            adapter.updateStatus(position, state);
            adapter.notifyDataSetChanged();
        }
    }*/


    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }


}