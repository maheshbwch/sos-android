package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.models.GetActivityListModel;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.ArrayList;

public class MyActivityAdapter extends RecyclerView.Adapter<MyActivityAdapter.ViewHolder> {

    private ArrayList<GetActivityListModel.Notification> arrayList;
    private Context context;

    public MyActivityAdapter(Context context, ArrayList<GetActivityListModel.Notification> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_get_activity, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String activityType = arrayList.get(position).getActivityType();
        String message = arrayList.get(position).getActivityMessage();
        String date = arrayList.get(position).getActivityUpdatedAt();

        if (MyUtils.checkStringValue(message)) {
            holder.txtContent.setText(message);
        }

        if (MyUtils.checkStringValue(date)) {
            holder.txtDate.setText(date);
        }

        if (MyUtils.checkStringValue(activityType) && activityType.equalsIgnoreCase("2")) {
            holder.typeImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.near));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView typeImageView;
        public TextView txtContent, txtDate;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtContent = itemView.findViewById(R.id.ap_txtContent);
            this.txtDate = itemView.findViewById(R.id.ip_txtDate);
            this.typeImageView = itemView.findViewById(R.id.ip_typeImage);
        }
    }


}
