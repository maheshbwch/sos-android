package com.sosandroid.mobile.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.models.PremiumBenefitsList;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.ArrayList;


public class BenefitsAdapter extends RecyclerView.Adapter<BenefitsAdapter.ViewHolder> {

        private ArrayList<PremiumBenefitsList.PremiumBenefit> arrayList;
    private Context context;
    private String type;

    public BenefitsAdapter(Context context, ArrayList<PremiumBenefitsList.PremiumBenefit> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        Log.e("type", ":" + type);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_premium_benefits, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String planName = arrayList.get(position).getFeature();
        boolean isGold = arrayList.get(position).getGoldPlan();
        boolean isFree = arrayList.get(position).getFree();

        if (!isGold){
            holder.goldIconTxt.setText(context.getResources().getString(R.string.fa_wrong));
            holder.goldIconTxt.setTextColor(context.getResources().getColor(R.color.red));
        }else {
            holder.goldIconTxt.setText(context.getResources().getString(R.string.fa_check));
            holder.goldIconTxt.setTextColor(context.getResources().getColor(R.color.green));
        }


        if (!isFree){
            holder.freeIconTxt.setText(context.getResources().getString(R.string.fa_wrong));
            holder.freeIconTxt.setTextColor(context.getResources().getColor(R.color.red));
        }else {
            holder.freeIconTxt.setText(context.getResources().getString(R.string.fa_check));
            holder.freeIconTxt.setTextColor(context.getResources().getColor(R.color.green));
        }


        holder.planTxt.setText(MyUtils.checkStringValue(planName) ? planName.trim() : "-");

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView freeIconTxt,goldIconTxt,planTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.freeIconTxt = itemView.findViewById(R.id.ipb_freePlanTxt);
            this.goldIconTxt = itemView.findViewById(R.id.ipb_goldPlanTxt);
            this.planTxt = itemView.findViewById(R.id.ipb_planTxt);
        }
    }


}
