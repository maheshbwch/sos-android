package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.OnContactSelected;

import java.util.ArrayList;


public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.ViewHolder> {

    private ArrayList<String> arrayList;
    private Context context;
    private OnContactSelected onContactSelected;
    private boolean isSelected;

    public PlacesAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_places, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


    }

    @Override
    public int getItemCount() {
        return 3; //TODO REMOVE
        //return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView locationNameTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.locationNameTxt = itemView.findViewById(R.id.ipl_locationNameTxt);
        }
    }


}