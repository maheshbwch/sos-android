package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.OnItemClicked;
import com.sosandroid.mobile.models.Contacts;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.ArrayList;

public class AlertSocailAppAdapter extends RecyclerView.Adapter<AlertSocailAppAdapter.ViewHolder> {

    private ArrayList<Contacts.Datum> arrayList;
    private Context context;
    private OnItemClicked onItemClicked;
    private String type;

    public AlertSocailAppAdapter(Context context, ArrayList<Contacts.Datum> arrayList, OnItemClicked onItemClicked) {
        this.context = context;
        this.arrayList = arrayList;
        this.onItemClicked = onItemClicked;
        Log.e("type", ":" + type);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.alert_social_app, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String contactName = arrayList.get(position).getContactName();
        holder.contactNameTxt.setText(MyUtils.checkStringValue(contactName) ? contactName.trim() : "-");

        String whatsappStatus = arrayList.get(position).getWhatsappNotification();
        String telegramStatus = arrayList.get(position).getTelegramNotification();
        String lineStatus = arrayList.get(position).getLineNotification();
        String viberStatus = arrayList.get(position).getViberNotification();
        String callStatus = arrayList.get(position).getCall();

        if (whatsappStatus.equalsIgnoreCase("1")) {
            holder.imgView.setBackgroundResource(R.drawable.whatsapp_live);
        } else if (telegramStatus.equalsIgnoreCase("1")) {
            holder.imgView.setBackgroundResource(R.drawable.telegram);
        } else if (lineStatus.equalsIgnoreCase("1")) {
            holder.imgView.setBackgroundResource(R.drawable.line);
        } else if (viberStatus.equalsIgnoreCase("1")) {
            holder.imgView.setBackgroundResource(R.drawable.viber);
        } else if (callStatus.equalsIgnoreCase("1")) {
            holder.imgView.setBackgroundResource(R.drawable.call);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView contactNameTxt;
        public ImageView imgView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.contactNameTxt = itemView.findViewById(R.id.ci_contactNameTxt);
            this.imgView = itemView.findViewById(R.id.img_whatsApp);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClicked.onItemClicked(getAdapterPosition());
                }
            });


        }
    }


}
