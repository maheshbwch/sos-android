package com.sosandroid.mobile.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.models.PostModel;
import com.sosandroid.mobile.utils.MyUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private ArrayList<PostModel.PostDetail> arrayList;
    private Context context;

    public PostAdapter(Context context, ArrayList<PostModel.PostDetail> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_posts, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String postContent = arrayList.get(position).getPostContent();
        String postImage = arrayList.get(position).getPostImage();
        String postDate = arrayList.get(position).getPostedAt();
        String postCategory = arrayList.get(position).getPostCategory();

        if (MyUtils.checkStringValue(postImage)) {
            Glide.with(context).load(postImage).placeholder(R.drawable.user_pic).into(holder.imgView);
            holder.imgView.setVisibility(View.VISIBLE);
        } else {
            holder.imgView.setVisibility(View.GONE);
        }

        if (MyUtils.checkStringValue(postContent)) {
            holder.txtContent.setText(postContent);
        }

        if (MyUtils.checkStringValue(postDate)) {
            holder.txtDate.setText(postDate);
        }

        if (MyUtils.checkStringValue(postCategory)) {
            holder.txtCategory.setText("Category: " + postCategory);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtContent, txtDate, txtCategory;
        private ImageView imgView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtContent = itemView.findViewById(R.id.ap_txtContent);
            this.imgView = itemView.findViewById(R.id.ip_imgView);
            this.txtDate = itemView.findViewById(R.id.ip_txtDate);
            this.txtCategory = itemView.findViewById(R.id.ip_txtCategory);
        }
    }


}