package com.sosandroid.mobile.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.OnItemClicked;
import com.sosandroid.mobile.models.Contacts;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.ToggleImageButton;

import java.util.ArrayList;


public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    private ArrayList<Contacts.Datum> arrayList;
    private Context context;
    private OnItemClicked onItemClicked;
    private String type;

    public ContactListAdapter(Context context, ArrayList<Contacts.Datum> arrayList, OnItemClicked onItemClicked) {
        this.context = context;
        this.arrayList = arrayList;
        this.onItemClicked = onItemClicked;
        Log.e("type", ":" + type);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.contact_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String contactName = arrayList.get(position).getContactName();
        holder.contactNameTxt.setText(MyUtils.checkStringValue(contactName) ? contactName.trim() : "-");

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView contactNameTxt;
        public ToggleImageButton toggleImage;
        public TextView optionsTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.contactNameTxt = itemView.findViewById(R.id.ci_contactNameTxt);
            this.toggleImage = itemView.findViewById(R.id.ci_toggleImage);
            this.optionsTxt = itemView.findViewById(R.id.ci_optionsTxt);


            toggleImage.setVisibility(View.GONE);
            optionsTxt.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClicked.onItemClicked(getAdapterPosition());
                }
            });

        }
    }


}

