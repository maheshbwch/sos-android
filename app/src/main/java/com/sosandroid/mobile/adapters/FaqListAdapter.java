package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.account.FaqTabLayoutActivity;
import com.sosandroid.mobile.models.FagTitleTypeModel;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.ArrayList;

public class FaqListAdapter extends RecyclerView.Adapter<FaqListAdapter.ViewHolder> {

    private ArrayList<FagTitleTypeModel> arrayList;
    private Context context;

    public FaqListAdapter(Context context, ArrayList<FagTitleTypeModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.faq_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String title = arrayList.get(position).getTitle();
        holder.txtTtitle.setTypeface(MyUtils.getRegularFont(context));
        holder.txtTtitle.setText(MyUtils.checkStringValue(title) ? title.trim() : "-");

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTtitle;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtTtitle = itemView.findViewById(R.id.faq_titleTxt);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("title_arraylist", arrayList);
                    bundle.putSerializable("type", arrayList.get(getAdapterPosition()).getType());
                    Intent intent = new Intent(context, FaqTabLayoutActivity.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                }
            });

        }
    }
}
