package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.models.FaqModel;

import java.util.ArrayList;

public class FeaturtesFaqAdapter extends RecyclerView.Adapter<FeaturtesFaqAdapter.ViewHolder> {

    private ArrayList<FaqModel.Faqs.Feature> arrayList;
    private Context context;

    public FeaturtesFaqAdapter(Context context, ArrayList<FaqModel.Faqs.Feature> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.faq_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.txtTitle.setText(arrayList.get(position).getFeatureContent());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;


        public ViewHolder(View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.faq_titleTxt);


        }
    }

}
