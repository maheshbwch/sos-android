package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.OnContactSelected;
import com.sosandroid.mobile.models.ContactModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.ToggleImageButton;

import java.util.ArrayList;


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private ArrayList<ContactModel> arrayList;
    private Context context;
    private OnContactSelected onContactSelected;
    private String type;

    public ContactsAdapter(Context context, ArrayList<ContactModel> arrayList, OnContactSelected onContactSelected, String type) {
        this.context = context;
        this.arrayList = arrayList;
        this.onContactSelected = onContactSelected;
        this.type = type;

        Log.e("type", ":" + type);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.contact_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String contactName = arrayList.get(position).getName();
        boolean isSelected = arrayList.get(position).isSelected();

        holder.toggleImage.setChecked(isSelected);
        holder.contactNameTxt.setText(MyUtils.checkStringValue(contactName) ? contactName.trim() : "-");

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView contactNameTxt;
        public ToggleImageButton toggleImage;
        public TextView optionsTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.contactNameTxt = itemView.findViewById(R.id.ci_contactNameTxt);
            this.toggleImage = itemView.findViewById(R.id.ci_toggleImage);
            this.optionsTxt = itemView.findViewById(R.id.ci_optionsTxt);


            if (type.equalsIgnoreCase(Constants.CONTACTS_SELECTION)) {
                toggleImage.setVisibility(View.VISIBLE);
                toggleImage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        ContactModel contactModel = arrayList.get(getAdapterPosition());
                        if (isChecked) {
                            onContactSelected.onSelected(contactModel);
                        } else {
                            onContactSelected.onDeSelected(contactModel);
                        }

                    }
                });
            } else {
                toggleImage.setVisibility(View.GONE);
                optionsTxt.setVisibility(View.GONE);
            }


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ContactModel contactModel = arrayList.get(getAdapterPosition());

                    if (type.equalsIgnoreCase(Constants.CONTACTS_SELECTION)) {
                        if (toggleImage.isChecked()) {
                            toggleImage.setEnabled(false);
                            toggleImage.setChecked(false);
                            onContactSelected.onDeSelected(contactModel);
                        } else {
                            toggleImage.setEnabled(true);
                            toggleImage.setChecked(true);
                            onContactSelected.onSelected(contactModel);
                        }
                    }


                }
            });

        }
    }


}