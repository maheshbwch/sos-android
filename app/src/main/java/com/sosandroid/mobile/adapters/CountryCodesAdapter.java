package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.OnSelectedListener;
import com.sosandroid.mobile.models.CountryCodes;
import com.sosandroid.mobile.utils.MyUtils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CountryCodesAdapter extends RecyclerView.Adapter<CountryCodesAdapter.ViewHolder> implements Filterable {

    private ArrayList<CountryCodes.Datum> arrayList;
    private ArrayList<CountryCodes.Datum> filteredList;
    private Context context;
    private OnSelectedListener onSelectedListener;

    public CountryCodesAdapter(Context context, ArrayList<CountryCodes.Datum> arrayList, OnSelectedListener onSelectedListener) {
        this.context = context;
        this.arrayList = arrayList;
        this.filteredList = arrayList;
        this.onSelectedListener = onSelectedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_country_code, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String flagUrl = filteredList.get(position).getUrlPath();
        String name = filteredList.get(position).getCountriesName();
        holder.countryNameTxt.setText(MyUtils.checkStringValue(name) ? name.trim() : "-");
        Glide.with(context).load(flagUrl).into(holder.countryFlagImage);

    }


    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    filteredList = arrayList;
                } else {

                    ArrayList<CountryCodes.Datum> fList = new ArrayList<>();

                    for (CountryCodes.Datum countryCodes : arrayList) {

                        if (countryCodes.getCountriesName().toLowerCase().contains(charString) || countryCodes.getCountriesName().toLowerCase().contains(charString) ||
                                countryCodes.getCountriesName().toLowerCase().contains(charString)) {

                            fList.add(countryCodes);
                        }
                    }

                    filteredList = fList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<CountryCodes.Datum>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView countryFlagImage;
        public TextView countryNameTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.countryFlagImage = itemView.findViewById(R.id.icc_countryFlagImage);
            this.countryNameTxt = itemView.findViewById(R.id.icc_countryNameTxt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    CountryCodes.Datum countryCodes = filteredList.get(getAdapterPosition());
                    onSelectedListener.onSelected(countryCodes);
                }
            });

        }
    }
}