package com.sosandroid.mobile.adapters;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.sosandroid.mobile.fragments.ContactsFragment;
import com.sosandroid.mobile.fragments.PlacesFragment;
import com.sosandroid.mobile.fragments.PostFragment;
import com.sosandroid.mobile.fragments.TestingFragment;

public class HomePagerAdapter extends FragmentPagerAdapter {

    private String isFrom;

    public HomePagerAdapter(FragmentManager fm,String isFrom) {
        super(fm);
        this.isFrom = isFrom;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new PlacesFragment(); //ChildFragment1 at position 0
            case 1:
                return new ContactsFragment(isFrom); //ChildFragment2 at position 1
            case 2:
                return new TestingFragment(); //ChildFragment3 at position 2
            case 3:
                return new PostFragment(); //ChildFragment4 at position 3
        }
        return null; //does not happen
    }

    @Override
    public int getCount() {
        return 4; //four fragments
    }
}