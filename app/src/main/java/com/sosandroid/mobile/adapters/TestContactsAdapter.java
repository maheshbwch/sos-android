package com.sosandroid.mobile.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.sosandroid.mobile.fragments.ContactsFragment;

public class TestContactsAdapter extends FragmentPagerAdapter {

    private String isFrom;

    public TestContactsAdapter(FragmentManager fm,String isFrom) {
        super(fm);
        this.isFrom = isFrom;
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new ContactsFragment(isFrom); //ChildFragment1 at position 0
        }
        return null; //does not happen
    }

    @Override
    public int getCount() {
        return 1;
    }
}
