package com.sosandroid.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.interfaces.OnContactSelected;
import com.sosandroid.mobile.models.FaqModel;

import java.util.ArrayList;

public class CommonFaqAdapter extends RecyclerView.Adapter<CommonFaqAdapter.ViewHolder> {

    private ArrayList<FaqModel.Faqs.CommonFaq> arrayList;
    private Context context;
    private OnContactSelected onContactSelected;
    private String type;

    public CommonFaqAdapter(Context context, ArrayList<FaqModel.Faqs.CommonFaq> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.commonfaq_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.txtQuestions.setText(arrayList.get(position).getCommonFaqQuestion());
        holder.txtAnswers.setText(arrayList.get(position).getCommonFaqAnswer());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtQuestions, txtAnswers;


        public ViewHolder(View itemView) {
            super(itemView);
            this.txtQuestions = itemView.findViewById(R.id.cfq_titleTxtQues);
            this.txtAnswers = itemView.findViewById(R.id.cfq_titleTxtAns);


        }
    }

}
