package com.sosandroid.mobile.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.sosandroid.mobile.R;

import java.util.ArrayList;

public class IntroPagerAdapter extends PagerAdapter {

    Activity activity;
    private ArrayList<Integer> arrayList;

    public IntroPagerAdapter(Activity activity, ArrayList<Integer> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater  layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_intro_pager, container, false);

        ImageView sliderImageView = view.findViewById(R.id.iip_sliderImageView);

        sliderImageView.setImageDrawable(activity.getResources().getDrawable(arrayList.get(position)));


        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}

