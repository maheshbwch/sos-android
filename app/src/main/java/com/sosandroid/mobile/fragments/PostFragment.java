package com.sosandroid.mobile.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.AddpostActivity;
import com.sosandroid.mobile.adapters.PostAdapter;
import com.sosandroid.mobile.models.PostModel;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostFragment extends Fragment {

    private View rootView = null;
    private RecyclerView postRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton addPost;
    private TextView txtTotalPost, errorTxt;
    private ProgressBar progressBar, bottomProgressBar;

    private ArrayList<PostModel.PostDetail> postDetailsArrayList = new ArrayList<>();
    private PostAdapter postAdapter = null;
    private boolean isLoading = true;
    private int pageNumber = 0;
    private String userId = null;

    public PostFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_posts, container, false);
        }

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        postRecyclerView = view.findViewById(R.id.fpo_postRecyclerView);
        addPost = view.findViewById(R.id.fpo_addPost);
        txtTotalPost = view.findViewById(R.id.fpo_postCountTxt);

        progressBar = view.findViewById(R.id.fp_progressBar);
        bottomProgressBar = view.findViewById(R.id.fp_bottomProgressBar);

        errorTxt = view.findViewById(R.id.fp_errorTxt);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        postRecyclerView.setLayoutManager(linearLayoutManager);
        postRecyclerView.setHasFixedSize(true);

        userId = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_ID);

        getPosts(0, true);

        listeners();

    }

    private void listeners() {

        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), AddpostActivity.class);
                startActivityForResult(intent, Constants.ADD_POST);
                MyUtils.openOverrideAnimation(true, getActivity());
            }
        });

    }

    private void getPosts(int pageNumber, final boolean isLoadingForFirstTime) {
        try {
            if (isLoadingForFirstTime) {
                postDetailsArrayList.clear();
                progressBar.setVisibility(View.VISIBLE);
            } else {
                bottomProgressBar.setVisibility(View.VISIBLE);
            }

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<PostModel> call = apiService.getPosts("post_list", String.valueOf(pageNumber), userId);
            call.enqueue(new Callback<PostModel>() {
                @Override
                public void onResponse(Call<PostModel> call, Response<PostModel> response) {
                    hideProgressBar(isLoadingForFirstTime);
                    if (response.isSuccessful()) {
                        PostModel postModel = response.body();

                        if (postModel != null) {
                            ArrayList<PostModel.PostDetail> arrayList = new ArrayList<>();
                            arrayList.addAll(postModel.getPostDetails());

                            if (arrayList.size() > 0) {
                                postDetailsArrayList.addAll(arrayList);
                                int totalPost = postModel.getTotalPost();
                                adaptPosts(postDetailsArrayList, totalPost,isLoadingForFirstTime);
                            } else {
                                showError(isLoadingForFirstTime);
                            }
                        }

                    } else {
                        Log.e("error", "response un successful");
                        showError(isLoadingForFirstTime);
                    }
                }

                @Override
                public void onFailure(Call<PostModel> call, Throwable t) {
                    showError(isLoadingForFirstTime);
                    hideProgressBar(isLoadingForFirstTime);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            hideProgressBar(isLoadingForFirstTime);
            if (!isLoadingForFirstTime) {
                isLoading = isLoadingForFirstTime;
            }
            Log.e("exp", ":" + e.getMessage());
            showError(isLoadingForFirstTime);
        }
    }

    private void hideProgressBar(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (progressBar.getVisibility() == View.VISIBLE) {
                progressBar.setVisibility(View.GONE);
            }
        } else {
            hideBottomProgressBar();
        }
    }

    private void hideBottomProgressBar() {
        if (bottomProgressBar.getVisibility() == View.VISIBLE) {
            bottomProgressBar.setVisibility(View.GONE);
        }
    }


    private void adaptPosts(ArrayList<PostModel.PostDetail> postModelArrayList, int totalPost, boolean isLoadingForFirstTime) {

        if (postModelArrayList.size() > 0) {
            resetViews(isLoadingForFirstTime);
            if (postAdapter != null) {
                postAdapter.notifyDataSetChanged();
                isLoading = true;
            } else {
                postAdapter = new PostAdapter(getActivity(), postModelArrayList);
                postRecyclerView.setAdapter(postAdapter);
                txtTotalPost.setText("Posts(" + totalPost + ")");
            }

            postRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (isLoading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                isLoading = false;
                                Log.e("recyclerView", "bottom_reached");
                                pageNumber = pageNumber + 1;

                                getPosts(pageNumber, false);

                                Log.e("page_number", "number:" + pageNumber);
                            }
                        }
                    }
                }
            });
        } else {
            showError(isLoadingForFirstTime);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constants.ADD_POST) {
            postAdapter = null;
            getPosts(0, true);
        }

    }

    private void showError(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (postRecyclerView.getVisibility() == View.VISIBLE) {
                postRecyclerView.setVisibility(View.GONE);
            }

            if (errorTxt.getVisibility() == View.GONE) {
                errorTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    private void resetViews(boolean isLoadingForFirstTime) {
        if (isLoadingForFirstTime) {
            if (postRecyclerView.getVisibility() == View.GONE) {
                postRecyclerView.setVisibility(View.VISIBLE);
            }

            if (errorTxt.getVisibility() == View.VISIBLE) {
                errorTxt.setVisibility(View.GONE);
            }
        }
    }

}
