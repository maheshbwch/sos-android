package com.sosandroid.mobile.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.TestHelpContactsActivity;
import com.sosandroid.mobile.activities.alert.AlertSocialApp;
import com.sosandroid.mobile.models.ContactsCounts;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestingFragment extends Fragment {


    private LinearLayout addTestContactLinear, testAlertLinear;
    private String userId = null;
    private TextView txtTotalCount;
    private ProgressDialog progressDialog = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_testing, container, false);

        addTestContactLinear = rootView.findViewById(R.id.ft_addTestContactLinear);
        testAlertLinear = rootView.findViewById(R.id.ft_testAlertLinear);
        txtTotalCount = rootView.findViewById(R.id.ft_totalCount);
        userId = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_ID);

        listeners();

        return rootView;
    }

    private void listeners() {
        addTestContactLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), TestHelpContactsActivity.class);
                startActivityForResult(intent, Constants.TEST_CONTACT_COUNT);
                MyUtils.openOverrideAnimation(true, getActivity());

            }
        });


        testAlertLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToAlertPage();
            }
        });

        getTotalCount();
    }


    private void getTotalCount() {
        try {
            progressDialog = MyUtils.showProgressLoader(getActivity(), "Loading..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<ContactsCounts> call = apiService.getTotalCount("total_contact", userId);
            call.enqueue(new Callback<ContactsCounts>() {
                @Override
                public void onResponse(Call<ContactsCounts> call, Response<ContactsCounts> response) {
                    MyUtils.dismissProgressLoader(progressDialog);

                    if (response.isSuccessful()) {

                        ContactsCounts contactsTotal = response.body();

                        if (contactsTotal != null) {
                            String totalCount = String.valueOf(contactsTotal.getData().getTotalCount());
                            if (MyUtils.checkStringValue(totalCount)) {
                                txtTotalCount.setText(totalCount);
                            }
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<ContactsCounts> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Constants.TEST_CONTACT_COUNT) {
            getTotalCount();
        }
    }


    private void intentToAlertPage() {
       /* Bundle bundle = new Bundle();
        bundle.putString(Constants.TYPE, Constants.TEST_ALERT);
        Intent intent = new Intent(getActivity(), AlertActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);*/
        Intent intent = new Intent(getActivity(), AlertSocialApp.class);
        intent.putExtra(Constants.IS_FROM,Constants.IS_FROM_TEST);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());

    }


}
