package com.sosandroid.mobile.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.CommonFaqAdapter;
import com.sosandroid.mobile.adapters.FeaturtesFaqAdapter;
import com.sosandroid.mobile.adapters.GeneralFaqAdapter;
import com.sosandroid.mobile.adapters.LocationFaqAdapter;
import com.sosandroid.mobile.adapters.PrivacyPolicyFaqAdapter;
import com.sosandroid.mobile.models.FaqModel;
import com.sosandroid.mobile.utils.Constants;

import java.util.ArrayList;

public class FaqFragment extends Fragment {

    private FaqModel faqModels;
    private String type;
    private RecyclerView recyclerView;
    private ArrayList<FaqModel.Faqs.CommonFaq> commonFaqArrayList = new ArrayList<>();
    private ArrayList<FaqModel.Faqs.Location> locationArrayList = new ArrayList<>();
    private ArrayList<FaqModel.Faqs.General> generalArrayList = new ArrayList<>();
    private ArrayList<FaqModel.Faqs.Feature> featureArrayList = new ArrayList<>();
    private ArrayList<FaqModel.Faqs.PrivacyPolicy> privacyPolicyArrayList = new ArrayList<>();

    public FaqFragment(FaqModel faqModels, String type) {
        this.faqModels = faqModels;
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_faq, container, false);

        recyclerView = rootView.findViewById(R.id.faq_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        commonFaqArrayList = faqModels.getFaqs().getCommonFaqs();
        locationArrayList = faqModels.getFaqs().getLocation();
        generalArrayList = faqModels.getFaqs().getGeneral();
        featureArrayList = faqModels.getFaqs().getFeatures();
        privacyPolicyArrayList = faqModels.getFaqs().getPrivacyPolicy();

        switch (type) {
            case Constants.COMMON_FAQ:
                if (commonFaqArrayList.size() > 0) {
                    CommonFaqAdapter getFaqAdapter = new CommonFaqAdapter(getActivity(), commonFaqArrayList);
                    recyclerView.setAdapter(getFaqAdapter);
                }
                break;

            case Constants.LOCATION_FAQ:
                if (locationArrayList.size() > 0) {
                    LocationFaqAdapter locationFaqAdapter = new LocationFaqAdapter(getActivity(), locationArrayList);
                    recyclerView.setAdapter(locationFaqAdapter);
                }

                break;

            case Constants.GENERAL_FAQ:
                if (generalArrayList.size() > 0) {
                    GeneralFaqAdapter generalFaqAdapter = new GeneralFaqAdapter(getActivity(), generalArrayList);
                    recyclerView.setAdapter(generalFaqAdapter);
                }

                break;

            case Constants.FEATURES_FAQ:
                if (featureArrayList.size() > 0) {
                    FeaturtesFaqAdapter generalFaqAdapter = new FeaturtesFaqAdapter(getActivity(), featureArrayList);
                    recyclerView.setAdapter(generalFaqAdapter);
                }

                break;

            case Constants.PRIVACY_POLICY:
                if (privacyPolicyArrayList.size() > 0) {
                    PrivacyPolicyFaqAdapter privacyPolicyFaqAdapter = new PrivacyPolicyFaqAdapter(getActivity(), privacyPolicyArrayList);
                    recyclerView.setAdapter(privacyPolicyFaqAdapter);
                }

                break;

        }


        return rootView;
    }
}
