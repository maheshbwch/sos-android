package com.sosandroid.mobile.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.adapters.SortByAdapter;
import com.sosandroid.mobile.utils.MyUtils;

import java.util.ArrayList;

public class PlacesFragment extends Fragment {

    private LinearLayout sortByLinear,searchLinear;
    private TextView  sortByTxt, searchTxt;
    private ImageView imgPolice,imgAmbulance,imgHospital,imgFireStation;
    //private RecyclerView recyclerView;

    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayList<String> sortByArrayList = new ArrayList<>();
    private PopupWindow sortByPopByWindow;


    String searchBy = null;

    //https://www.google.com/maps
    //WebView webView;
    //https://www.google.com/maps/search/hospitals+near+me


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_places, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        //webView = view.findViewById(R.id.fp_webview);

       // searchIconTxt = view.findViewById(R.id.fpl_searchIconTxt);
        sortByLinear = view.findViewById(R.id.fpl_sortByLinear);
        sortByTxt = view.findViewById(R.id.fpl_sortByTxt);
        searchLinear = view.findViewById(R.id.fpl_searchLinear);
        searchTxt = view.findViewById(R.id.fpl_searchTxt);

        imgPolice = view.findViewById(R.id.fp_imgPolice);
        imgAmbulance = view.findViewById(R.id.fp_imgAmbulance);
        imgHospital = view.findViewById(R.id.fp_imgHospital);
        imgFireStation = view.findViewById(R.id.fp_imgFireStation);

        //recyclerView = view.findViewById(R.id.fpl_recyclerView);

        listeners();
    }

    private void listeners() {
        /*webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://www.google.com/maps/search/hospitals");*/



        /*sortByLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSortByPopupWindow(sortByArrayList);
            }
        });*/


        sortByArrayList.clear();
        sortByArrayList.add("Police Station");
        sortByArrayList.add("Ambulance");
        sortByArrayList.add("Hospital");
        sortByArrayList.add("Fire Station");
        searchLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSortByPopupWindow(sortByArrayList);
            }
        });


       /* searchIconTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchText = searchEdt.getText().toString().trim();
                if (MyUtils.checkStringValue(searchText)) {
                    intentToGoogleMapApp(searchText);
                } else {
                    MyUtils.showLongToast(getActivity(), "Enter the keyword or location to search");
                }
            }
        });
*/

        searchTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MyUtils.checkStringValue(searchBy)) {
                    intentToGoogleMapApp(searchBy);
                } else {
                    MyUtils.showLongToast(getActivity(), "Select the NearBy Type of Emergency");
                }

            }
        });

        imgPolice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                intentToGoogleMapApp("Police Station");
            }
        });

        imgAmbulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToGoogleMapApp("Ambulance");
            }
        });

        imgHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToGoogleMapApp("Hospital");
            }
        });

        imgFireStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToGoogleMapApp("Fire Station");
            }
        });


        /*recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        PlacesAdapter placesAdapter = new PlacesAdapter(getActivity(), arrayList);
        recyclerView.setAdapter(placesAdapter);*/
    }


    private void intentToGoogleMapApp(String query) {
        try {
            String googleMapPackageName = "com.google.android.apps.maps";
            boolean isAppInstalled = MyUtils.appInstalledOrNot(getActivity(), googleMapPackageName);
            if (isAppInstalled) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + query);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage(googleMapPackageName);
                startActivity(mapIntent);
            } else {
                MyUtils.showLongToast(getActivity(), "Google Map app is not installed in your app");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showSortByPopupWindow(final ArrayList<String> filterList) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.item_sort_type_layout, null);
        sortByPopByWindow = new PopupWindow(view, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, false);
        ListView listView = view.findViewById(R.id.ist_sortByListView);
        SortByAdapter adapter = new SortByAdapter(getActivity(), filterList);
        listView.setAdapter(adapter);
        sortByPopByWindow.setOutsideTouchable(true);
        sortByPopByWindow.showAsDropDown(searchLinear);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                sortByPopByWindow.dismiss();
                sortByTxt.setText(filterList.get(i));
                searchBy = filterList.get(i);
                intentToGoogleMapApp(searchBy);
            }
        });
    }


}
