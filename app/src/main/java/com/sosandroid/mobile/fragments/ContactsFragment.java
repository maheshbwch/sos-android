package com.sosandroid.mobile.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.sosandroid.mobile.R;
import com.sosandroid.mobile.activities.contacts.AddContactManually;
import com.sosandroid.mobile.activities.contacts.ContactsOptions;
import com.sosandroid.mobile.activities.registration.ContactsActivity;
import com.sosandroid.mobile.adapters.ContactListAdapter;
import com.sosandroid.mobile.adapters.ContactsBannerAdapter;
import com.sosandroid.mobile.interfaces.CustomAlertInterface;
import com.sosandroid.mobile.interfaces.OnItemClicked;
import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.models.ContactModel;
import com.sosandroid.mobile.models.Contacts;
import com.sosandroid.mobile.utils.Constants;
import com.sosandroid.mobile.utils.CustomAlert;
import com.sosandroid.mobile.utils.MyUtils;
import com.sosandroid.mobile.utils.PreferenceHandler;
import com.sosandroid.mobile.webservice.ApiInterface;
import com.sosandroid.mobile.webservice.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsFragment extends Fragment {

    private Context context;
    private ViewPager viewPager;
    private LinearLayout pagerDotsLinear;
    private RecyclerView emergencyContactsRecycler;
    private TextView addContactTxt, contactTitle, errorTxt;
    private ProgressDialog progressDialog = null;

    ArrayList<Contacts.Datum> savedContactsList = new ArrayList<>();
    ArrayList<ContactModel> contactModelArrayList = new ArrayList<>();
    private ContactListAdapter contactsAdapter = null;

    private String userId = null;
    private String isFrom = null;

    public ContactsFragment(String isFrom) {
        this.isFrom = isFrom;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);
        context = getActivity();
        init(rootView);

        return rootView;
    }

    private void init(View view) {
        viewPager = view.findViewById(R.id.fct_view_pager);
        pagerDotsLinear = view.findViewById(R.id.fct_pager_dots);
        emergencyContactsRecycler = view.findViewById(R.id.fct_emergencyContactsRecycler);
        addContactTxt = view.findViewById(R.id.fct_addContactTxt);
        contactTitle = view.findViewById(R.id.fct_titleNameTxt);
        errorTxt = view.findViewById(R.id.fc_errorTxt);

        if (isFrom.equals(Constants.IS_FROM_EMERGENCY_CONTACT)) {
            contactTitle.setText("Emergency Contacts List");
        } else {
            contactTitle.setText("Test Help Contact List");
        }


        listener();
    }

    private void listener() {

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
        Log.e("userID", ":" + userId);

        emergencyContactsRecycler.setHasFixedSize(true);
        emergencyContactsRecycler.setLayoutManager(new LinearLayoutManager(context));

        final ArrayList<Integer> sliderArrayList = new ArrayList<>();
        sliderArrayList.add(R.drawable.contacts_banner1);
        sliderArrayList.add(R.drawable.contacts_banner2);

        ContactsBannerAdapter sizeAdapter = new ContactsBannerAdapter(getActivity(), sliderArrayList);
        viewPager.setAdapter(sizeAdapter);

        addBottomDots(0, sliderArrayList.size());
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position, sliderArrayList.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        addContactTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseContactAlert();
            }
        });


        getContactsList();


    }

    private void chooseContactAlert() {
        CustomAlert customAlert = new CustomAlert(context, "How you will add this new contact?", "Add Manually", "Select From Contact", new CustomAlertInterface() {
            @Override
            public void onPositiveButtonClicked() {
                Intent intent = new Intent(context, AddContactManually.class);
                intent.putExtra(Constants.IS_FROM, isFrom);
                startActivityForResult(intent, Constants.ADD_CONTACT_MANUALLY);
                MyUtils.openOverrideAnimation(true, getActivity());
            }

            @Override
            public void onNegativeButtonClicked() {
                intentToContactsPage();
            }
        });
        customAlert.showDialog();
    }

    private void addBottomDots(int currentPage, int size) {
        if (size > 0) {
            TextView[] dots = new TextView[size];

            pagerDotsLinear.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(context);
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(60);
                dots[i].setTextColor(Color.parseColor("#D4D8DB"));
                pagerDotsLinear.addView(dots[i]);
            }

            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
            dots[currentPage].setTextSize(60);
        }

    }


    private void getContactsList() {
        try {
            String type;

            if (isFrom.equals(Constants.IS_FROM_EMERGENCY_CONTACT)) {
                type = "emergency_contacts";
            } else {
                type = "test_contacts";
            }

            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<Contacts> call = apiService.getContactsList(type, userId);
            call.enqueue(new Callback<Contacts>() {
                @Override
                public void onResponse(Call<Contacts> call, Response<Contacts> response) {

                    if (response.isSuccessful()) {

                        Contacts contacts = response.body();

                        if (contacts != null) {
                            String status = contacts.getStatus();

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                updateUI(contacts);
                            } else if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.FAILURE)) {
                                showError();
                            }
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<Contacts> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    showError();
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
            showError();
        }
    }


    private void updateUI(Contacts contacts) {

        savedContactsList.clear();
        savedContactsList.addAll(contacts.getData());

        contactModelArrayList.clear();
        for (Contacts.Datum contactModel : savedContactsList) {
            contactModelArrayList.add(new ContactModel(contactModel.getContactName(), contactModel.getContactNumber(), false));
        }

        adaptContactListToRecycler();
    }


    private void adaptContactListToRecycler() {
        if (savedContactsList.size() > 0) {
            resetViews();
            ContactListAdapter contactsAdapter = new ContactListAdapter(context, savedContactsList, new OnItemClicked() {
                @Override
                public void onItemClicked(int position) {

                    Intent intent = new Intent(context, ContactsOptions.class);
                    intent.putExtra(Constants.CONTACT_ID, savedContactsList.get(position).getContactId());
                    Log.e("number", ":" + savedContactsList.get(position).getContactNumber());
                    startActivity(intent);

                }
            });
            emergencyContactsRecycler.setAdapter(contactsAdapter);


           /* if (contactsAdapter != null) {
                contactsAdapter.notifyDataSetChanged();
            } else {
                contactsAdapter = new ContactListAdapter(context, savedContactsList, new OnItemClicked() {
                    @Override
                    public void onItemClicked(int position) {

                        Intent intent = new Intent(context, ContactsOptions.class);
                        startActivity(intent);

                    }
                });
                emergencyContactsRecycler.setAdapter(contactsAdapter);
            }*/
        } else {
            showError();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("requestCode", "code:" + requestCode);
        Log.e("resultCode", "code:" + resultCode);


        if (requestCode == Constants.GET_CONTACTS) {
            if (resultCode == Activity.RESULT_OK) {

                Bundle b = data.getExtras();

                ArrayList<ContactModel> contactArrayList = (ArrayList<ContactModel>) b.getSerializable(Constants.selectedContacts);
                if (contactArrayList != null) {
                    Gson gson = new GsonBuilder().create();
                    JsonArray myCustomArray = gson.toJsonTree(contactArrayList).getAsJsonArray();
                    Log.e("data", "data:" + myCustomArray.toString());

                    saveContacts(myCustomArray.toString());
                }


            } else if (resultCode == Activity.RESULT_CANCELED) {
                //TODO
            }
        }

        if (resultCode == Constants.ADD_CONTACT_MANUALLY) {

            getContactsList();
        }
    }


    private void saveContacts(String contactsJson) {
        try {

            String type;
            if (isFrom.equals(Constants.IS_FROM_EMERGENCY_CONTACT)) {
                type = "emergency_contacts";
            } else {
                type = "test_contacts";
            }

            progressDialog = MyUtils.showProgressLoader(context, "Adding Contacts..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<BaseModel> call = apiService.saveContacts(type, userId, contactsJson);
            call.enqueue(new Callback<BaseModel>() {
                @Override
                public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {

                        BaseModel baseModel = response.body();

                        if (baseModel != null) {
                            String status = baseModel.getStatus();
                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.SUCCESS)) {
                                getContactsList();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseModel> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void intentToContactsPage() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.selectedContacts, contactModelArrayList);
        Intent intent = new Intent(context, ContactsActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, Constants.GET_CONTACTS);
        MyUtils.openOverrideAnimation(true, getActivity());
    }

    private void closePage() {
        getActivity().finish();
        MyUtils.openOverrideAnimation(false, getActivity());
    }


    private void showError() {
        if (emergencyContactsRecycler.getVisibility() == View.VISIBLE) {
            emergencyContactsRecycler.setVisibility(View.GONE);
        }

        if (errorTxt.getVisibility() == View.GONE) {
            errorTxt.setVisibility(View.VISIBLE);
        }
    }

    private void resetViews() {
        if (emergencyContactsRecycler.getVisibility() == View.GONE) {
            emergencyContactsRecycler.setVisibility(View.VISIBLE);
        }

        if (errorTxt.getVisibility() == View.VISIBLE) {
            errorTxt.setVisibility(View.GONE);
        }
    }


}
