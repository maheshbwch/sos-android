package com.sosandroid.mobile.webservice;

public class ApiConstants {

    public static final String BASE_URL = "https://bestweb.my/sos/";
    public static final String API_BASE_URL = BASE_URL + "api/";

    public static final String COUNTRIES_CODES_LIST = API_BASE_URL + "Country_details/country_list?type=country_list";
    public static final String CHECK_USER_ALREADY_REGISTERED = API_BASE_URL + "User_details/existing_user_check";
    public static final String REGISTER_USER = API_BASE_URL + "User_details/register_user";
    public static final String SIGN_IN_USER = API_BASE_URL + "User_details/signin_user";
    public static final String UPDATE_PROFILE_INFO = API_BASE_URL + "Update_details/profile";
    public static final String ADD_CONTACTS = API_BASE_URL + "User_details/save_contacts";
    public static final String CONTACTS_LIST = API_BASE_URL + "User_details/contact_list";
    public static final String UPDATE_USER_NAME = API_BASE_URL + "Update_details/user_name";
    public static final String UPDATE_USER_PROFILE = API_BASE_URL + "Update_details/profile";
    public static final String UPDATE_USER_ADDRESS = API_BASE_URL + "Update_details/address";
    public static final String CHANGE_PASSWORD = API_BASE_URL + "User_details/change_password";
    public static final String PREMIUM_BENEFITS = API_BASE_URL + "premium_benefits/get_plans?get_type=get_plans";
    public static final String ADD_POST = API_BASE_URL + "posts/add_post";
    public static final String GET_POST = API_BASE_URL + "posts/post_list";
    public static final String GET_TOTAL_COUNT = API_BASE_URL + "User_details/contact_list";
    public static final String GET_CONTACT_DETAILS = API_BASE_URL + "User_details/read_contact_notification_details";
    public static final String UPDATE_CLICK_STATUS = API_BASE_URL + "User_details/edit_contact_options";
    public static final String GET_FAQ = API_BASE_URL + "faq/get_faq";
    public static final String FORGOT_PASSWORD = API_BASE_URL + "User_details/forgot_password";
    public static final String NEAR_BY_ALERT = API_BASE_URL + "alert/near_by_alert";
    public static final String GET_ACTIVITY = API_BASE_URL + "alert/get_activity";
    public static final String UPDATE_LOCATION = API_BASE_URL + "User_details/update_user_location";

}
