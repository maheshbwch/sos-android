package com.sosandroid.mobile.webservice;

import com.sosandroid.mobile.models.BaseModel;
import com.sosandroid.mobile.models.ContactDetails;
import com.sosandroid.mobile.models.Contacts;
import com.sosandroid.mobile.models.ContactsCounts;
import com.sosandroid.mobile.models.CountryCodes;
import com.sosandroid.mobile.models.FaqModel;
import com.sosandroid.mobile.models.GetActivityListModel;
import com.sosandroid.mobile.models.Login;
import com.sosandroid.mobile.models.PostModel;
import com.sosandroid.mobile.models.PremiumBenefitsList;
import com.sosandroid.mobile.models.Profile;
import com.sosandroid.mobile.models.Register;
import com.sosandroid.mobile.models.ValidateUser;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET(ApiConstants.COUNTRIES_CODES_LIST)
    Call<CountryCodes> getCountriesCodesList();

    @FormUrlEncoded
    @POST(ApiConstants.CHECK_USER_ALREADY_REGISTERED)
    Call<ValidateUser> validateIfUserAlreadyRegistered(@Field("type") String type, @Field("country_id") String countryId, @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST(ApiConstants.REGISTER_USER)
    Call<Register> registerUser(@Field("type") String type, @Field("country_id") String countryId, @Field("country_code") String country_code, @Field("mobile") String mobile, @Field("password") String password, @Field("device_id") String deviceId, @Field("fcm_token") String fcmToken);

    @FormUrlEncoded
    @POST(ApiConstants.SIGN_IN_USER)
    Call<Login> signUser(@Field("type") String type, @Field("country_id") String countryId, @Field("mobile") String mobile, @Field("password") String password,@Field("device_id") String deviceId,@Field("fcm_token") String fcmToken);

    @Multipart
    @POST(ApiConstants.UPDATE_PROFILE_INFO)
    Call<Profile> updateProfileInfo(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST(ApiConstants.ADD_CONTACTS)
    Call<BaseModel> saveContacts(@Field("type") String type, @Field("user_id") String userId, @Field("contacts") String contacts);


    @FormUrlEncoded
    @POST(ApiConstants.CONTACTS_LIST)
    Call<Contacts> getContactsList(@Field("type") String type, @Field("user_id") String userId);


    @FormUrlEncoded
    @POST(ApiConstants.UPDATE_USER_NAME)
    Call<BaseModel> updateUserName(@Field("type") String type, @Field("user_id") String userId, @Field("user_name") String userName, @Field("race") String race);

    @Multipart
    @POST(ApiConstants.UPDATE_USER_PROFILE)
    Call<Profile> updateProfileProfile(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);


    @FormUrlEncoded
    @POST(ApiConstants.UPDATE_USER_PROFILE)
    Call<BaseModel> updateEmail(@Field("type") String type, @Field("user_id") String userId, @Field("email") String email);

    @FormUrlEncoded
    @POST(ApiConstants.UPDATE_USER_PROFILE)
    Call<BaseModel> updatePhoneNumber(@Field("type") String type, @Field("user_id") String userId, @Field("mobile") String userName, @Field("country_id") String countryId);

    @FormUrlEncoded
    @POST(ApiConstants.UPDATE_USER_ADDRESS)
    Call<BaseModel> updateAddress(@Field("type") String type, @Field("user_id") String userId, @Field("home_address") String homeAddress, @Field("office_address") String officeAddress, @Field("school_address") String schoolAddress);

    @FormUrlEncoded
    @POST(ApiConstants.CHANGE_PASSWORD)
    Call<BaseModel> changePassword(@Field("post_type") String type, @Field("user_id") String userId, @Field("old_password") String oldPassword, @Field("new_password") String newPassword);


    @GET(ApiConstants.PREMIUM_BENEFITS)
    Call<PremiumBenefitsList> getPremiumBenefitsList();

    @Multipart
    @POST(ApiConstants.ADD_POST)
    Call<BaseModel> addPost(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part file);

    @GET(ApiConstants.GET_POST)
    Call<PostModel> getPosts(@Query("get_type")String type, @Query("pagination") String pagination,@Query("user_id") String userId);

    @FormUrlEncoded
    @POST(ApiConstants.GET_TOTAL_COUNT)
    Call<ContactsCounts> getTotalCount(@Field("type") String type, @Field("user_id") String userId);

    @FormUrlEncoded
    @POST(ApiConstants.GET_CONTACT_DETAILS)
    Call<ContactDetails> getContactDetails(@Field("type") String type, @Field("user_id") String userId, @Field("contact_id") String contactId);

    @FormUrlEncoded
    @POST(ApiConstants.UPDATE_CLICK_STATUS)
    Call<BaseModel> updateStatus(@Field("type") String type, @Field("user_id") String userId, @Field("contact_id") String contactId, @Field("whatsapp") String whatsApp,
                                 @Field("telegram") String telegram, @Field("line") String line, @Field("viber") String viber, @Field("call") String call);

    @GET(ApiConstants.GET_FAQ)
    Call<FaqModel> getFaq(@Query("get_type")String type);

    @FormUrlEncoded
    @POST(ApiConstants.FORGOT_PASSWORD)
    Call<BaseModel> forgotPassword(@Field("post_type") String type, @Field("user_id") String userId,@Field("new_password") String newPassword);

    @FormUrlEncoded
    @POST(ApiConstants.NEAR_BY_ALERT)
    Call<BaseModel> alertNearBy(@Field("post_type") String type, @Field("user_id") String userId);

    @GET(ApiConstants.GET_ACTIVITY)
    Call<GetActivityListModel> getActivityList(@Query("get_type")String type, @Query("user_id") String userId, @Query("pagination") String pagination);

    @FormUrlEncoded
    @POST(ApiConstants.UPDATE_LOCATION)
    Call<BaseModel> updateLocation(@Field("post_type") String type, @Field("user_id") String userId, @Field("latitude") String latitude, @Field("longitude") String longitude);


}
